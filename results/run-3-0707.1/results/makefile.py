#!/usr/bin/python

import sys
import getopt
import os.path

def addlist_todict(d, key,value):
    if(key in d):
        d[key].append(value)
    else:
        d[key] = [value]

def make_pars(fgroup,mode):
    ret = ""
    for feat in fgroup:
        if feat in ["CG", "PG", "OG", "WG"]:
            ret = ret + ' '.join(["%s-%d,%s" % (feat, x, mode) for x in [1,2,3]])+' '
            
        elif feat in ["ER", "FW"]:
            ret = ret + "%s-%s " % (feat, mode)
         
        elif feat in ["SL", "WL"]:
            ret = ret + "%s " % feat
        else:
            assert False
    return ret
    
debug = 0
opts, args = getopt.getopt(sys.argv[1:], 'd:')
for opt,val in opts:
    if(opt=='-d'):
        debug = int(val)
        
    
modes = ["bin", "log", "raw", "rel"]
Cs  = ["0.001"]
degrees = [1,2]
#ftypes = ["SL", "WL", "CG", "PG", "WG", "OG", "ER", "FW"]
fgroups = [["SL", "WL", "CG", "PG", "WG", "OG", "ER", "FW"], #ALL
           ["SL", "WL", "PG", "ER", "FW"], #NONCONTENT + ERR
           ["SL", "WL", "PG", "FW"], #NONCONTENT - ERR
           ["CG", "PG", "WG", "OG", "ER", "FW"], #ALL - SL - WL
           ["CG", "PG", "WG", "OG"], #NGRAMS
           ["CG", "PG", "OG"], #NGRAMS - WORDGRAMS 
           ["PG", "OG"], #NGRAMS - WORDGRAMS - CHARGRAMS
           ["SL", "WL"], #AVG LENGTHS
           ["ER", "FW"], #ERR+FW
           ["SL"],
           ["WL"],
           ["CG"],
           ["PG"],
           ["WG"],
           ["OG"],
           ["ER"],
           ["FW"]
]

if(debug):
    modes = modes[0:debug]
    Cs = Cs[0:debug]
    degrees = degrees[0:debug]
    #ftypes = ftypes[0:debug]
    fgroups = [fgroups[-debug]]    

leaf_dirs = ["classify_out", "examples", "learn_output", "models", "predictions"]

targets = []

prefix = \
         """
         SHELL = /bin/bash
         ROOT = both-types
         SCRIPTS = ../scripts
         MAKEFEATS = $(SCRIPTS)/makefeats.py
         COMPUTEGAIN = $(SCRIPTS)/computeGain.py
         LEARN = ../svm-light/svm_learn
         CLASSIFY = ../svm-light/svm_classify
         TEST = devtest
         .PHONY: train gain learn test classify
         """

targets.append(["all",["classify"],[]])
targets.append(["$(ROOT)/dirs.complete", [],
                ["mkdir -p $(ROOT)/%s/%s/%s" % ('_'.join(group), mode, leaf)
                 for group in fgroups
                 for mode in modes
                 for leaf in leaf_dirs]+["touch $@"]])

train_targets = []
for group in fgroups:
    for mode in modes:
        name = "$(ROOT)/%s/%s/examples/train.complete" % ('_'.join(group), mode)
        train_targets.append(name)
        command = "$(MAKEFEATS) train $(ROOT)/%s/%s/examples/train " \
                  % ('_'.join(group), mode) + make_pars(group, mode)

        touch = "touch $@"
        targets.append([name, ["$(ROOT)/dirs.complete"], [command, touch]])
        #targets.append([name, [], [command, touch]])

targets.append(["train", train_targets, []])

gain_targets = []
for group in fgroups:
    for mode in modes:
        name =  "$(ROOT)/%s/%s/featnames.selected" % ('_'.join(group), mode)
        gain_targets.append(name)
        gain_file = "$(ROOT)/%s/%s/examples/train.gain" % ('_'.join(group), mode)
        commands = ["$(COMPUTEGAIN) $(ROOT)/%s/%s/examples/train %s"\
                    % ('_'.join(group), mode, gain_file),
                    "cat %s | tail -n +2 | join --nocheck-order -j1 -t$$'\\t' -  $(ROOT)/%s/featnames | cut -f1,2,4,5- | awk '$$2 > 0.002'  | sort -gr -k2 > $@.tmp && mv -f $@.tmp $@" % (gain_file,'_'.join(group))
        ]
        #targets.append([name, ["train"], commands])
        targets.append([name, ["$(ROOT)/%s/%s/examples/train.complete" % ('_'.join(group), mode)], commands])

targets.append(["gain", gain_targets, []])

models = {}
learn_targets = []
#linear
for c in Cs:
    for group in fgroups:
        for mode in modes:
            name = "$(ROOT)/%s/%s/learn_output/t-0_c-%s" % ('_'.join(group), mode, c)
            learn_targets.append(name)
            model = "$(ROOT)/%s/%s/models/t-0_c-%s" % ('_'.join(group), mode, c)
            addlist_todict(models, ('_'.join(group), mode), model)
            command = "$(LEARN) -j 2.8 -c %s $(ROOT)/%s/%s/examples/train %s > $@.tmp && mv -f $@.tmp $@" % (c, '_'.join(group), mode, model)
            #targets.append([name, ["train"], [command]])
            targets.append([name, ["$(ROOT)/%s/%s/examples/train.complete" % ('_'.join(group), mode)], [command]])
            
#polynomial
for d in degrees:
    for c in Cs:
        for group in fgroups:
            for mode in modes:
                name = "$(ROOT)/%s/%s/learn_output/t-1_c-%s_d-%d" \
                       % ('_'.join(group), mode, c, d)
                learn_targets.append(name)
                model = "$(ROOT)/%s/%s/models/t-1_c-%s_d-%d" % ('_'.join(group), mode, c, d)
                addlist_todict(models, ('_'.join(group), mode), model)
                command = "$(LEARN) -j 2.8 -c %s -t 1 -d %d $(ROOT)/%s/%s/examples/train %s > $@.tmp && mv -f $@.tmp $@" \
                      % (c, d, '_'.join(group), mode, model)
                #targets.append([name, ["train"], [command]])
                targets.append([name, ["$(ROOT)/%s/%s/examples/train.complete" % ('_'.join(group), mode)], [command]])

targets.append(["learn", learn_targets, []])

test_targets = []
test_examples = {}

for group in fgroups:
    for mode in modes:
        name = "$(ROOT)/%s/%s/examples/$(TEST).complete" % ('_'.join(group), mode)
        test_targets.append(name)
        test_example = "$(ROOT)/%s/%s/examples/$(TEST)" % ('_'.join(group), mode)
        test_examples[('_'.join(group), mode)] = test_example
        command =  "$(MAKEFEATS) $(TEST) %s $(ROOT)/%s/%s/featnames.selected " \
                      % (test_example, '_'.join(group), mode) + make_pars(group, mode)       

        touch = "touch $@"
        #targets.append([name, ["gain"], [command, touch]])
        targets.append([name, ["$(ROOT)/%s/%s/featnames.selected" % ('_'.join(group), mode)], [command, touch]])
targets.append(["test", test_targets, []])

classify_targets = []

for group in fgroups:
    for mode in modes:
        for model in models[('_'.join(group), mode)]:
            example = test_examples[('_'.join(group), mode)]
            base_example = example.split('/')[-1]
            base_model = model.split('/')[-1]
            name = "$(ROOT)/%s/%s/classify_out/%s_%s" % ('_'.join(group), mode, base_example, base_model)
            classify_targets.append(name)
            command = "$(CLASSIFY) %s %s $(ROOT)/%s/%s/predictions/%s_%s > $@.tmp && mv -f $@.tmp $@" \
                      % (example, model, '_'.join(group), mode, base_example, base_model)
            #targets.append([name, ["learn", "test"], [command]])
            targets.append([name, learn_targets+test_targets, [command]])
        
targets.append(["classify", classify_targets, []])

data_targets = []
for group in fgroups:
    for mode in modes:
        name = "$(ROOT)/%s/%s/data.tsv" % ('_'.join(group), mode)
        tmp = "$(ROOT)/%s/%s/data.tmp" % ('_'.join(group), mode)
        command_tmp = "for f in $(ROOT)/%s/%s/classify_out/* ; do cat $$f | sed 's/[\\/, ]/\\n/g' | grep -e \"[0-9][0-9]\\?\\.[0-9][0-9]\\?%%\" | tr '\\n' $$'\\t'; echo $$f ; done | grep -v \"[[:space:]]0.00%%\" | tr ' ' $$'\\t' > %s" % ('_'.join(group), mode, tmp)
        command_tsv = "./table.py %s > $@.tmp && mv -f $@.tmp $@" % (tmp)
        targets.append([name, [], [command_tmp, command_tsv]])
        data_targets.append(name)
targets.append(["data", data_targets, []])

clean = ["rm -f $(ROOT)/dirs.complete",
         "rm -f $(ROOT)/*/featnames",
         "rm -f $(ROOT)/*/*/featnames.selected",
         "rm -f $(ROOT)/*/*/data*",
         "rm -f $(ROOT)/*/*/classify_out/*",
         "rm -f $(ROOT)/*/*/models/*",
         "rm -f $(ROOT)/*/*/examples/*",
         "rm -f $(ROOT)/*/*/learn_output/*",
         "rm -f $(ROOT)/*/*/predictions/*"]

targets.append(["clean", [], clean])

sys.stdout.write(prefix)
for target in targets:
    name, deps, commands = target
    sys.stdout.write("\n%s: %s\n" % (name, ' '.join(deps)))
    for c in commands:
        sys.stdout.write("\t%s\n" % (c,))
    
    
