#!/usr/bin/python

# Transforms data from svm-classify (data.tmp files) to tsv file
# with Accuracy, F-score, Continuous (binary), Kernel, C, Degree columns.

import sys

f = open(sys.argv[1],'r')
lines = [l.split() for l in f.readlines()]
f.close()

meta = list()
nums = list()

for l in lines:
    nums.append(l[0:3])
    meta.append(l[3:])

measures = list()
for n in nums: # get rid of percent signs and make floats
    acc = float(n[0][0:len(n[0])-1])
    prec = float(n[1][0:len(n[1])-1])
    rec = float(n[2][0:len(n[2])-1])
    f_measure = round(((2*prec*rec)/(prec+rec)),2)
    measures.append([acc,f_measure])

params = list()
parlength = 4 # "Cont.", "Kernel", "C", "Degree"
for m in meta:
    dat = m[0].split('/')[-1].split('_')
    dat = [dat[0]] + [e.split('-')[1] for e in dat[1:]]
    # jen diskretizovana data
    if(dat[1] <> 'c'):
        app = [0]+dat[1:]
    else:
        app = dat[1:]
    if(len(app)<parlength):
        app = app + ([""]*(parlength-len(app)))
    params.append(app)

print '\t'.join(["Acc.", "F-score", "Cont.", "Kernel", "C", "Deg."])    
for dat in [measures[i]+params[i] for i in range(0,len(lines))]:
    print '\t'.join((str(d) for d in dat))
    
    
