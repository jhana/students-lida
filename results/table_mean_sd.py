#!/usr/bin/python

# Input as stdin : concatenated data.tsv files (get rid of headers first,
# `grep -v '[a-z]'` suffices).
# Output to stdout : Table with the following columns:
# Kernel (0-lin, 1-poly), C, Degree (- when linear kernel, otherwise 1,2 or 3),
# Accuracy mean, Accuracy standard deviation,
# F-score mean, F-score standard deviation.

import sys, numpy

datadict = dict()
#parse datafile 
for line in sys.stdin:
    nums = [float(e) for e in line.split()[0:2]]
    pars = '-'.join(line.split()[2:])

    if(pars in datadict):
        datadict[pars] += [nums]
    else:
        datadict[pars] = [nums]

#Compute mean and sd of accuracy and fscore

print "Kernel\tC\tDegree\tA mean\tA sd\tF mean\tF sd\tFreq"
for key in datadict:
    freq = len(datadict[key])
    accs = [e[0] for e in datadict[key]]
    fs = [e[1] for e in datadict[key]]
    
    accmean = round(numpy.mean(accs),2)
    accsd = round(numpy.std(accs),2)
    fmean = round(numpy.mean(fs),2)
    fsd = round(numpy.std(fs),2)
    if(len(key.split('-'))==4):
        print '\t'.join(key.split('-')[1:]),'\t',accmean,'\t',accsd,'\t',fmean,'\t',fsd,'\t',freq
    else: #linear kernel
        print '\t'.join(key.split('-')[1:]),'\t','-','\t',accmean,'\t',accsd,'\t',fmean,'\t',fsd,'\t',freq

    
