#!/usr/bin/python

"""
DESCRIPTION:
This script creates a valid Makefile, which runs a series of classification
experiments. This includes creating a directory structure for storing output and
results, extracting features from training data, training models by different
values of their parameters, learning these models, classification and performance
evaluation.
"""

"""
USAGE:
Settings that can be modified are:
-- Feature values (for example, it is possible to consider only binary values of
   features)
-- Values of the C parameter (values can be modified, only one value can be
   considered...)
-- Degrees of the polynomial kernel (1, 2 or 3)
-- and mainly Feature groups: There are 8 feature types and these can be variously
   combined. 
   The feature types are described in bigger detail in Chapter 4 of the thesis
   (text/thesis.pdf). In addition, n-gram features can be specified as uni-, bi-
   or trigrams.
   For example, "CG" means n-grams of characters, n=1,2,3 and "CG1" means unigrams
   of characters.

After modifying settings according to your choice, run from
results/run-x/results/:

makefile.py > your-makefile-name
make -f your-makefile-name (consider the -j [jobs] option for experiments which
                           include several feature groups)
"""

import sys
import getopt
import os.path

def addlist_todict(d, key,value):
    if(key in d):
        d[key].append(value)
    else:
        d[key] = [value]

###Locations of scripts, data to test on (`test` by default, can also be `devtest`).
prefix = \
         """
         SHELL = /bin/bash
         ROOT = feats
         SCRIPTS = ../scripts
         MAKEFEATS = $(SCRIPTS)/makefeats.py
         COMPUTEGAIN = $(SCRIPTS)/computeGain.py
         LEARN = ../svm-light/svm_learn
         CLASSIFY = ../svm-light/svm_classify
         TEST = test
         .PHONY: train gain learn test classify data
         """        
debug = 0
opts, args = getopt.getopt(sys.argv[1:], 'd:')
for opt,val in opts:
    if(opt=='-d'):
        debug = int(val)
        
###Feature values
modes = ["bin", "log", "raw", "rel"]
#modes = ["bin"]

###Different values of C parameter to be tested
Cs  = ["0.001", "0.01", "0.1", "1.0", "10.0", "100.0", "1000.0"]
#Cs = ["0.1"]

###Degrees of polynomial kernel
degrees = [1,2,3]
#degrees = [1]

###Different groups of features.
###You can choose out of these or try out your own combinations.
fgroups = [["SL", "WL", "CG", "PG", "WG", "OG", "ER", "FW"], #ALL
           #["SL", "WL", "PG", "ER", "FW"], #NONCONTENT + ERR
           #["SL", "WL", "PG", "FW"], #NONCONTENT - ERR
           #["CG", "PG", "WG", "OG", "ER", "FW"], #ALL - SL - WL
           #["CG", "PG", "WG", "OG"], #NGRAMS
           #["CG", "PG", "OG"], #NGRAMS - WORDGRAMS
           #["CG","PG"],
           ["CG","OG"],
           #["CG","ER"],
           #["CG","FW"],
           #["PG", "OG"], #NGRAMS - WORDGRAMS - CHARGRAMS
           #["SL", "WL"], #AVG LENGTHS
           #["ER", "FW"], #ERR+FW
           ["SL"],
           ["WL"],
           #["CG"], #1,2,3GRAMS
           #["PG"], #1,2,3GRAMS
           #["WG"], #1,2,3GRAMS
           ["OG"], #1,2,3GRAMS
           ["ER"],
           ["FW"],
           #["CG1"], #CHARACTER 1GRAMS
           ["CG2"], #CHARACTER 2GRAMS
           #["CG3"], #CHARACTER 3GRAMS
           #["PG1"], #POS 1GRAMS
           ["PG2"], #POS 2GRAMS
           #["PG3"], #POS 3GRAMS
           #["OG1"], #OPEN CLASS POS 1GRAMS
           #["OG2"], #OPEN CLASS POS 2GRAMS
           #["OG3"], #OPEN CLASS POS 3GRAMS
           #["WG1"], #WORD 1GRAMS
           #["WG2"], #WORD 2GRAMS
           #["WG3"], #WORD 3GRAMS
]

if(debug):
    modes = modes[0:debug]
    Cs = Cs[0:debug]
    degrees = degrees[0:debug]
    fgroups = [fgroups[-debug]]    

def make_pars(fgroup,mode):
    ret = ""
    for feat in fgroup:
        if feat in ["CG", "PG", "OG", "WG"]:
            ret = ret + ' '.join(["%s-%d,%s" % (feat, x, mode)
                                  for x in [1,2,3]])+' '
        elif feat in ["CG1", "PG1", "OG1", "WG1"]:
            ret = ret + ' '.join(["%s-%d,%s" % (feat[:-1], x, mode)
                                  for x in [1]])+' '
        elif feat in ["CG2", "PG2", "OG2", "WG2"]:
            ret = ret + ' '.join(["%s-%d,%s" % (feat[:-1], x, mode)
                                  for x in [2]])+' '
        elif feat in ["CG3", "PG3", "OG3", "WG3"]:
            ret = ret + ' '.join(["%s-%d,%s" % (feat[:-1], x, mode)
                                  for x in [3]])+' '
        elif feat in ["ER", "FW"]:
            ret = ret + "%s-%s " % (feat, mode)
        elif feat in ["SL", "WL"]:
            ret = ret + "%s " % feat
        else:
            assert False
    return ret
###Names of directories where different outputs are stored.
leaf_dirs = ["classify_out", "examples", "learn_output", "models", "predictions"]

###Make targets
targets = []

targets.append(["all",["classify","data"],[]])
targets.append(["$(ROOT)/dirs.complete", [],
                ["mkdir -p $(ROOT)/%s/%s/%s" % ('_'.join(group), mode, leaf)
                 for group in fgroups
                 for mode in modes
                 for leaf in leaf_dirs]+["touch $@"]])

train_targets = []
for group in fgroups:
    for mode in modes:
        name = "$(ROOT)/%s/%s/examples/train.complete" % ('_'.join(group), mode)
        train_targets.append(name)
        command = "$(MAKEFEATS) train $(ROOT)/%s/%s/examples/train " \
                  % ('_'.join(group), mode) + make_pars(group, mode)

        touch = "touch $@"
        targets.append([name, ["$(ROOT)/dirs.complete"], [command, touch]])
        #targets.append([name, [], [command, touch]])

targets.append(["train", train_targets, []])

gain_targets = []
for group in fgroups:
    for mode in modes:
        name =  "$(ROOT)/%s/%s/featnames.selected" % ('_'.join(group), mode)
        gain_targets.append(name)
        gain_file = "$(ROOT)/%s/%s/examples/train.gain" % ('_'.join(group), mode)
        commands = ["$(COMPUTEGAIN) $(ROOT)/%s/%s/examples/train %s"\
                    % ('_'.join(group), mode, gain_file),
                    "cat %s | tail -n +2 | join --nocheck-order -j1 -t$$'\\t' -  $(ROOT)/%s/featnames | cut -f1,2,4,5- | awk '$$2 > 0.000'  | sort -gr -k2 > $@.tmp && mv -f $@.tmp $@" % (gain_file,'_'.join(group))
        ]
        #targets.append([name, ["train"], commands])
        targets.append([name, ["$(ROOT)/%s/%s/examples/train.complete" % ('_'.join(group), mode)], commands])
        
targets.append(["gain", gain_targets, []])

models = {}
learn_targets = []
###Linear kernel function
for c in Cs:
    for group in fgroups:
        for mode in modes:
            name = "$(ROOT)/%s/%s/learn_output/t-0_c-%s" % ('_'.join(group), mode, c)
            learn_targets.append(name)
            model = "$(ROOT)/%s/%s/models/t-0_c-%s" % ('_'.join(group), mode, c)
            addlist_todict(models, ('_'.join(group), mode), model)
            command = "$(LEARN) -j 2.8 -c %s $(ROOT)/%s/%s/examples/train %s > $@.tmp && mv -f $@.tmp $@" % (c, '_'.join(group), mode, model)
            #targets.append([name, ["train"], [command]])
            targets.append([name, ["$(ROOT)/%s/%s/examples/train.complete" % ('_'.join(group), mode)], [command]])
            
###Polynomial kernel function
for d in degrees:
    for c in Cs:
        for group in fgroups:
            for mode in modes:
                name = "$(ROOT)/%s/%s/learn_output/t-1_c-%s_d-%d" \
                       % ('_'.join(group), mode, c, d)
                learn_targets.append(name)
                model = "$(ROOT)/%s/%s/models/t-1_c-%s_d-%d" % ('_'.join(group), mode, c, d)
                addlist_todict(models, ('_'.join(group), mode), model)
                command = "$(LEARN) -j 2.8 -c %s -t 1 -d %d $(ROOT)/%s/%s/examples/train %s > $@.tmp && mv -f $@.tmp $@" \
                      % (c, d, '_'.join(group), mode, model)
                #targets.append([name, ["train"], [command]])
                targets.append([name, ["$(ROOT)/%s/%s/examples/train.complete" % ('_'.join(group), mode)], [command]])

targets.append(["learn", learn_targets, []])

test_targets = []
test_examples = {}

for group in fgroups:
    for mode in modes:
        name = "$(ROOT)/%s/%s/examples/$(TEST).complete" % ('_'.join(group), mode)
        test_targets.append(name)
        test_example = "$(ROOT)/%s/%s/examples/$(TEST)" % ('_'.join(group), mode)
        test_examples[('_'.join(group), mode)] = test_example
        command =  "$(MAKEFEATS) $(TEST) %s $(ROOT)/%s/%s/featnames.selected " \
                      % (test_example, '_'.join(group), mode) + make_pars(group, mode)       

        touch = "touch $@"
        #targets.append([name, ["gain"], [command, touch]])
        targets.append([name, ["$(ROOT)/%s/%s/featnames.selected" % ('_'.join(group), mode)], [command, touch]])
targets.append(["test", test_targets, []])

classify_targets = []

for group in fgroups:
    for mode in modes:
        for model in models[('_'.join(group), mode)]:
            example = test_examples[('_'.join(group), mode)]
            base_example = example.split('/')[-1]
            base_model = model.split('/')[-1]
            name = "$(ROOT)/%s/%s/classify_out/%s_%s" % ('_'.join(group), mode, base_example, base_model)
            classify_targets.append(name)
            command = "$(CLASSIFY) %s %s $(ROOT)/%s/%s/predictions/%s_%s > $@.tmp && mv -f $@.tmp $@" \
                      % (example, model, '_'.join(group), mode, base_example, base_model)
            #targets.append([name, ["learn", "test"], [command]])
            targets.append([name, learn_targets+test_targets, [command]])
        
targets.append(["classify", classify_targets, []])

data_targets = []
for group in fgroups:
    for mode in modes:
        name = "$(ROOT)/%s/%s/data.tsv" % ('_'.join(group), mode)
        tmp = "$(ROOT)/%s/%s/data.tmp" % ('_'.join(group), mode)
        command_tmp = "for f in $(ROOT)/%s/%s/classify_out/* ; do cat $$f | sed 's/[\\/, ]/\\n/g' | grep -e \"[0-9][0-9]\\?\\.[0-9][0-9]\\?%%\" | tr '\\n' $$'\\t'; echo $$f ; done | grep -v \"[[:space:]]0.00%%\" | tr ' ' $$'\\t' > %s" % ('_'.join(group), mode, tmp)
        command_tsv = "./table.py %s > $@.tmp && mv -f $@.tmp $@" % (tmp)
        targets.append([name, ["classify"], [command_tmp, command_tsv]])
        data_targets.append(name)
targets.append(["data", data_targets, []])

clean = ["rm -f $(ROOT)/*/featnames",
         "rm -f $(ROOT)/*/*/featnames.selected",
         "rm -f $(ROOT)/*/*/data*",
         "rm -f $(ROOT)/*/*/classify_out/*",
         "rm -f $(ROOT)/*/*/models/*",
         "rm -f $(ROOT)/*/*/examples/*",
         "rm -f $(ROOT)/*/*/learn_output/*",
         "rm -f $(ROOT)/*/*/predictions/*"]

targets.append(["clean", [], clean])

### Makefile output
sys.stdout.write(prefix)
for target in targets:
    name, deps, commands = target
    sys.stdout.write("\n%s: %s\n" % (name, ' '.join(deps)))
    for c in commands:
        sys.stdout.write("\t%s\n" % (c,))
    
    
    


