#!/usr/bin/python
#Open-Class Part of Speech n-grams
import os,sys,codecs, math
from collections import Counter
from Feature_Generator import *
from itertools import tee, islice, izip
from pprint import pprint
from ngram import my_ngrams
from log_normalize import log_normalize, relative, transform

def get_tagged(docname):
    """Receives filename of text file (for example AA_AH_001.txt) and returns 
    the filename of the tagged text"""
    split = os.path.splitext(os.path.basename(docname))
    return split[0]+'.OCtag'+split[1]

class Feat_OCPOSgram(Feature_Generator):
    def __init__(self,n,mode):
        Feature_Generator.__init__(self)
        self.ftype = "OCPOS %d-gram"%(n,)
        self.n = n #n-gram size
        self.mode = mode
        self.ngcounts = None
        self.ngindices = None

    def set_documents(self,path,doc_names,doc_list):
        self.path = path
        self.docnames = [get_tagged(d) for d in doc_names]
        doc_list = list()
        for d in self.docnames:
            d_open = codecs.open(path+'/octags/'+d,'r','utf-8')
            doc_list.append(d_open.read())
        self.docs = doc_list
        
    def make_featureset(self):
        assert self.docs <> None
        ret = list()
        posgrams = dict()
        i = self.startindex
        
        alldocs = ''
        for doc in self.docs:
            alldocs = alldocs + doc
        # Creates sorted list of all Open Class POS n-grams in self.docs
        self.ngcounts = self.make_POSgrams(alldocs)
        # Only ngrams occuring at least twice in data
        ngset = set((ngram for ngram in self.ngcounts
                     if self.ngcounts[ngram] > 1 ))
        self.ngindices = dict(zip(ngset,range(0,len(ngset))))
        
        for doc in self.docs:
            ngrams = self.make_POSgrams(doc) #Create dict of ngrams for doc
            ngrams_filtered = {ngram:ngrams[ngram] for ngram in ngrams
                               if ngram in ngset}
            featureset = self.map_to_indices(self.ngindices,ngrams_filtered)
            ret.append(transform(featureset, self.mode, len(doc)))
            #ret.append(featureset)
        return(ret)

    def eval_featureset(self):
        ret = list()
        ngrams = dict()

        for doc in self.docs:
            ngrams = self.make_POSgrams(doc)
            # only ngrams previously seen in flattened training data
            ngrams_filtered = {ngram:ngrams[ngram] for ngram in ngrams
                               if ngram in self.inv_model}
            featureset = self.map_to_model(ngrams_filtered)
            ret.append(transform(featureset, self.mode, len(doc)))
            #ret.append(featureset)
        return(ret)

    def map_to_model(self,ngram_counts):
        ret = dict()
        for ngram in ngram_counts:
            ret[self.inv_model[ngram]] = ngram_counts[ngram]
        return(ret)
    
    def pass_index(self):
        #return(len(self.ngcounts)+self.startindex)
        return(len(self.ngindices)+self.startindex)
        
    def map_to_indices(self,indices,result_dict):
        global g_feat_names
        ret = dict()
        for ngram in result_dict:
            id = indices[ngram]+self.startindex
            count = result_dict[ngram]
            ret[id] = count
            g_feat_names[id] = (self.ftype,"\t".join(ngram))
        return(ret)
        
    def make_POSgrams(self,text):
        """Returns {ngram:count} Counter"""
        wgrams_counts = Counter()
        sentences = text.split('\n')
        for s in sentences:
            wgrams = my_ngrams(s.upper().split(), self.n)
            wgrams_counts.update([w for w in wgrams if not 'X' in w])
        return(dict(wgrams_counts))

#-------------------------------------------------#

if __name__ == "__main__":
    text = ''
    for line in sys.stdin:
        text = text + line
    pos = Feat_OCPOSgram(2)
    posgrams = pos.train(text)
    for p in posgrams:
        print posgrams[p],'\t',p
    
