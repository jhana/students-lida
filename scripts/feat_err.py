#!/usr/bin/python
#Error tags
import os,sys,codecs
from collections import Counter
from Feature_Generator import *
from log_normalize import log_normalize
from itertools import tee, islice, izip
from log_normalize import *
from pprint import pprint

def get_err(docname):
    """Receives filename of text file (for example AA_AH_001.txt) and returns 
    the filename of the file with corresponding errors"""
    split = os.path.splitext(os.path.basename(docname))
    return split[0]+'.err'+split[1]

class Feat_Err(Feature_Generator):
    def __init__(self,mode):
        Feature_Generator.__init__(self)
        self.ftype = "Error"
        self.mode = mode
        self.errcounts = None
        self.errindices = None

    def set_documents(self,path,doc_names,doc_list):
        self.path = path
        self.docnames = [get_err(d) for d in doc_names]
        doc_list = list()
        for d in self.docnames:
            d_open = codecs.open(path+'/errs/'+d,'r','utf-8')
            doc_list.append(d_open.read())
            d_open.close()
        self.docs = doc_list

    def make_featureset(self):
        assert self.docs <> None
        ret = list()
        errs = dict()
        i = self.startindex

        alldocs = ''
        for doc in self.docs:
            alldocs = alldocs + doc
        self.errcounts = self.make_errcounts(alldocs)
        # Only errors occuring at least twice in data
        errset = set((err for err in self.errcounts if self.errcounts[err] > 1))
        #print "Errors with count > 1 : ",errset
        self.errindices = dict(zip(errset,range(0,len(errset))))

        for doc in self.docs:
            errs = self.make_errcounts(doc) # Create dict of err:freq
            errs_filtered = {e:errs[e] for e in errs if e in errset}
            featureset = self.map_to_indices(self.errindices,errs_filtered)
            ret.append(transform(featureset, self.mode, len(doc)))
            #ret.append(featureset)
        return(ret)

    def eval_featureset(self):
        ret = list()
        errs = dict()
        
        for doc in self.docs:
            errs = self.make_errcounts(doc)
            # only previously seen errors
            errs_filtered = {e:errs[e] for e in errs if (e,) in self.inv_model}
            featureset = self.map_to_model(errs_filtered)
            ret.append(transform(featureset, self.mode, len(doc)))
            #ret.append(featureset)
        return(ret)
            
    def map_to_model(self,err_counts):
        ret = dict()
        for err in err_counts:
            ret[self.inv_model[(err,)]] = err_counts[err]
        return(ret)
        
    def pass_index(self):
        return(len(self.errcounts)+self.startindex)
    
    def map_to_indices(self,indices,result_dict):
        global g_feat_names
        ret = dict()
        for err in result_dict:
            id = indices[err]+self.startindex
            count = result_dict[err]
            ret[id] = count
            g_feat_names[id] = (self.ftype,err)
        return(ret)
    
    def make_errcounts(self,text):
        """Returns {errtype:count} Counter"""
        err_counts = Counter()
        sentences = text.split('\n')
        for s in sentences:
            # More than one error can occur in a word, e.g. 'Y1|Quant0'
            errs = s.replace('|',' ').split()
            err_counts.update(errs)
        return(err_counts)


