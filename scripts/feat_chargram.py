#!/usr/bin/python

import unicodedata, sys, math, collections, mypath, fileinput, math
from ngram import cleantext, _byvalue, Counter
from log_normalize import log_normalize, relative, transform
from pprint import pprint
from Feature_Generator import *

class Feat_Chargram(Feature_Generator):
    def __init__(self,n,mode):
        Feature_Generator.__init__(self)
        self.ftype = "char %d-gram"%(n,)
        self.n = n #n-gram size
        self.mode = mode
        self.ngcounts = None
        self.ngindices = None
       
    def make_featureset(self):
        assert self.docs <> None #zaruci, ze jsme zavolali set_documents
        ret = list()
        ngrams = dict()
        i = self.startindex
        
        # Creates set of all ngrams in self.docs
        alldocs=''
        for doc in self.docs:
            alldocs = alldocs+doc
        self.ngcounts = self.make_chargrams(alldocs)
        # only ngrams occurring at least twice in training data
        ngset = set((ngram for ngram in self.ngcounts
                     if self.ngcounts[ngram] > 1))
        #ngset = set((ngram for ngram in self.ngcounts))
        self.ngindices = dict(zip(ngset,range(0,len(ngset))))
        
        for doc in self.docs:
            # create dict of ngrams for doc with raw freqs
            ngrams = self.make_chargrams(doc)
            ngrams_filtered =  {ngram:ngrams[ngram] for ngram in ngrams
                                if ngram in ngset}            
            featureset = self.map_to_indices(self.ngindices,ngrams_filtered)
            # transform feature values depending on mode (binary, normalized...)
            ret.append(transform(featureset, self.mode, len(doc)))
            #ret.append(featureset)
        return(ret)

    def eval_featureset(self):
        ret = list()
        ngrams = dict()

        for doc in self.docs:
            ngrams = self.make_chargrams(doc)    
            # Only ngrams previously seen in flattened training data
            ngrams_filtered = {ngram:ngrams[ngram] for ngram in ngrams
                               if ngram in self.inv_model}
            featureset = self.map_to_model(ngrams_filtered)
            ret.append(transform(featureset, self.mode, len(doc)))
            #ret.append(featureset)
        return(ret)

    def map_to_model(self,ngram_counts):
        ret = dict()
        for ngram in ngram_counts:
            ret[self.inv_model[ngram]] = ngram_counts[ngram]
        return(ret)
        
    def pass_index(self):
        return(len(self.ngindices)+self.startindex)

    def map_to_indices(self,indices,result_dict):
        global g_feat_names
        ret = dict()
        for ngram in result_dict:
            id = indices[ngram]+self.startindex
            count = result_dict[ngram]
            ret[id] = count
            g_feat_names[id] = (self.ftype,"\t".join(ngram))
        return(ret)
            
    def make_chargrams(self,text):
        """Returns {ngram:raw count} dict"""
        wgrams_counts = Counter()
        sentences = text.split('\n')
        for s in sentences:
            words = [(" "*(self.n-1)+word+" "*(self.n-1)).lower()
                     for word in s.split()
                     if word.isalnum()]
            for w in words:
                wgrams = my_ngrams(w,self.n)
                wgrams_counts.update(wgrams)
        return(dict(wgrams_counts))
    
def my_ngrams(inlist,n):
    len_inlist = len(inlist)
    outlist = [tuple(inlist[i:i+n]) for i in xrange(len_inlist-n+1)]
    return outlist    
#-------------------------------------------------#

if __name__ == "__main__":    
    text = ''
    for line in sys.stdin:
        text = text + line
    ch = Feat_Chargram(2)
    chgrams = ch.train(text)

   
