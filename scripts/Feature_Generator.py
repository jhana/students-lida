#!/usr/bin/python

import os
import codecs

# os.listdir(path) returns a list containing the names of the entries in
# the directory given by path. The list is in arbitrary order.
def docs_to_strings(path):
    """Returns a tuple (names of documents, list of documents)"""
    doc_names = [file for file in os.listdir(path) if os.path.isfile(os.path.join(path, file))]
    doc_names.sort()
    doc_list = list()
    for d in doc_names:
        d_open = codecs.open(path+'/'+d,'r','utf-8')
        doc_list.append(d_open.read()) #An encoded unicode object is returned
    return(doc_names,doc_list)        

class Feature_Generator:
    def __init__(self):
        self.docs = list() #list of strings=documents
        self.startindex = None #index, od ktereho se cisluje
        self.ftype = None
        self.model = None ## Feats from load_features
        self.inv_model = None
        
    def set_documents(self,path,doc_names,doc_list):
        self.path = path
        self.docnames = doc_names
        self.docs = doc_list
    
    def set_startindex(self,index):
        self.startindex = index  
    
    def pass_index(self):#Last used index+1 - for the next FeatureGenerator
        assert False # Implemented in children
    
    def make_featureset(self):
        # Implemented in children;
        # vraci seznam dict(), indexy stejne jako u seznamu dokumentu
        assert False

    def eval_featureset(self):
        # When applying features to testing data
        # Implemented in children
        assert False

    def load_features(self,filename):
        f = codecs.open(filename,mode="r",encoding="utf-8")
        feats = dict()
        for line in f:
            data = line.rstrip('\n').split('\t')
            feat_num = data[0]
            #feat_type = data[1]
            feat_type = data[2]
            if(feat_type == self.ftype):
                #feat_params = data[2:]
                feat_params = data[3:]
                #feats[feat_num] = feat_params
                feats[feat_num] = tuple(feat_params)
        f.close()
        return(feats)
        

def get_feat_names():
    global g_feat_names
    return(g_feat_names)

def print_feat_names(filename):
    global g_feat_names
    f=codecs.open(filename,mode="w",encoding="utf-8")
    keys = g_feat_names.keys()
    keys.sort()
    for k in keys:
        val = g_feat_names[k]
        ftype,fname = val
        f.write('\t'.join([str(k),ftype,fname])+'\n')
    f.close()


    
# Dictionary indexed by feat numbers, with (feattype, featname) values
g_feat_names = dict()


        
