#!/usr/bin/python
#Word length
import unicodedata, sys, collections, mypath, os, fileinput
from ngram import cleantext, _byvalue, Counter, wordlist

from Feature_Generator import *

class Feat_Wlength(Feature_Generator):
    def __init__(self):
        Feature_Generator.__init__(self)
        self.ftype = "wlength"
    
    def make_featureset(self):
        global g_feat_names
        ret = list()
        for doc in self.docs:
            n = self.wlength(self.make_wlist(doc))
            ret.append({self.startindex:n})
        g_feat_names[self.startindex] = (self.ftype,"wlength")
        return(ret)
    
    def eval_featureset(self):
        ret = list()
        for doc in self.docs:
            n = self.wlength(self.make_wlist(doc))
            ret.append({self.inv_model[(u'wlength',)]:n})
        return(ret)

    def pass_index(self):
        return(self.startindex+1)      
            
    def make_wlist(self,text):
        #wlist=wordlist(text)
        wlist=list()
        for word in text.split(' '):
            if word.isalnum():
                wlist = wlist +[word]

        return(wlist)
        
    def wlength(self,wlist):
        if(len(wlist)==0):
            return(None)
        all = 0
        for word in wlist:
            all = all + len(word)
        return(float(all)/len(wlist))#avg word length    
