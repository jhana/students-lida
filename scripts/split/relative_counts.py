#!/usr/bin/python

"""
Input file (passed as argument) should have a " sort | uniq -c | sort -nr " format, that is absolute line counts in first column and and line in second column.

Outputs a three column format to standard output, first column are relative counts (percentage), second and third are same as input.
"""

import sys

dict_in = {}
list_out = []

count_sum = 0

for x in open(sys.argv[1]):
    count, line = x.split()
    dict_in[line] = count
    count_sum = count_sum + int(count)

for line, count in dict_in.iteritems():
    list_out.append([format(float(count)/count_sum, '.2f'), count, line])

for x in sorted(list_out, reverse=True):
    print '\t'.join(x)
    


    


