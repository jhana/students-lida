#!/usr/bin/python

# Arguments : [1,2,3,4] : s_id.counts, Ratio of TEST, TRAIN, DEV_TEST (sums to 1.0)
"""
Example how to run script: " ./split.py s_id.counts 0.3 0.4 0.3 " divides 
s_id.counts (file containg two columns: author ID, number of texts written by 
this author) into three files, test.s_id, train.s_id and dev_test.s_id. Each of 
these files (created in working directory) contains a unique list of author IDs (s_id attribute).
--------------------------------------------------------------------------------
To get a split of t_ids, run:

" scripts/split/split_docs.sh test.s_id train.s_id dev_test.s_id"

from the my_data directory.
"""

import sys, time, re

# total number of docs to split
TOTAL = 3715 

s_id_counts = {}
for line in open(sys.argv[1]):
    (count,id) = line.split()
    s_id_counts[id] = int(count)

TEST = int(float(sys.argv[2])*TOTAL)
TRAIN = int(float(sys.argv[3])*TOTAL)
DEV_TEST = int(float(sys.argv[4])*TOTAL+1)

train_count = 0
train_s_id = list()
test_count = 0
test_s_id = list()

while(train_count <= TRAIN):
    pop = s_id_counts.popitem()
    train_s_id.append(pop[0])
    train_count = train_count + pop[1]

while(test_count <= TEST):
    pop = s_id_counts.popitem()
    test_s_id.append(pop[0])
    test_count = test_count + pop[1]

dev_test_count = sum(s_id_counts.values())
dev_test_s_id = s_id_counts.keys()

# print s_ids to files
test_file = open('test.s_id','w')
for item in test_s_id:
    test_file.write("%s\n" % item)
test_file.close()    
    
train_file = open('train.s_id','w')
for item in train_s_id:
    train_file.write("%s\n" % item)
train_file.close()    

dev_test_file = open('dev_test.s_id','w')
for item in dev_test_s_id:
    dev_test_file.write("%s\n" % item)
dev_test_file.close()

# create info_file
info_file = open('split_info','w')
info_file.writelines(["Total number of files: %d\n" % TOTAL, 
    "Train/test/dev_test ratio is %.3f/%.3f/%.3f\n" % (train_count/float(TOTAL), test_count/float(TOTAL), dev_test_count/float(TOTAL))])
info_file.writelines([  "Number of train files is %d\n" % train_count,
                        "Number of test files is %d\n" % test_count,
                        "Number of dev_test files is %d\n" % dev_test_count])
info_file.close()





