#!/bin/sh

# Splits t_id into three files (test,train, dev_test) using a previous s_id split.
# Run from students-lida/data/my_data.

# arguments : three files created by split_authors.py,
# should be : "test.s_id train.s_id dev_test.s_id"
#------------------------------------------------------------------------------#

# sort files
sort statistics/split/$1 > statistics/split/$1.sorted
sort statistics/split/$2 > statistics/split/$2.sorted
sort statistics/split/$3 > statistics/split/$3.sorted

# get t_ids: join on second field of 1st file
join -1 2 statistics/t_id2s_id.sorted statistics/split/$1.sorted \
    | cut -f2 -d' ' > statistics/split/test.t_id

join -1 2 statistics/t_id2s_id.sorted statistics/split/$2.sorted \
    | cut -f2 -d' ' > statistics/split/train.t_id

join -1 2 statistics/t_id2s_id.sorted statistics/split/$3.sorted \
    | cut -f2 -d' ' > statistics/split/dev_test.t_id
