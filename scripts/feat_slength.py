#!/usr/bin/python
#Sentence length
import unicodedata, sys, collections, mypath, re, fileinput
from Feature_Generator import *

class Feat_Slength(Feature_Generator):
    def __init__(self):
        Feature_Generator.__init__(self)
        self.ftype = "slength"
        
    def make_featureset(self):
        global g_feat_names
        ret = list()
        for doc in self.docs:
            n = self.slength(self.make_slist(doc)) 
            ret.append({self.startindex:n})
        g_feat_names[self.startindex] = (self.ftype,"slength")
        return(ret)

    def eval_featureset(self):
        ret = list()
        for doc in self.docs:
            n = self.slength(self.make_slist(doc))
            ret.append({self.inv_model[(u'slength',)]:n})
        return(ret)
    
    def pass_index(self):
        return(self.startindex+1)
    
    def make_slist(self,text):
        # one line = one sentence (see how the txt files are made in
        # scripts/extract-from-xml/xml_get_text.py)
        return([s for s in text.splitlines() if s <> ''])    
  
    def slength(self,slist): #slength as number of words in sentence
        if(len(slist)==0):
            return(None)
        all = 0
        for sentence in slist:
            for word in sentence.split(' '):
                if word.isalnum():
                    all = all + 1
        return(float(all)/len(slist)) #avg sentence length
