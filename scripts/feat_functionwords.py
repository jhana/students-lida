#!/usr/bin/python
#Function words
from Feature_Generator import *
from log_normalize import log_normalize, relative, transform
from collections import Counter

def get_flist(docname):
    """Receives filename of text file with one function word on each line
    and saves this as a list"""
    flist = list()
    f = codecs.open(docname,'r','utf-8')
    for line in f:
        flist.append(line.strip().lower())
    f.close()
    return flist

class Feat_FunctWords(Feature_Generator):
    def __init__(self,mode):
        Feature_Generator.__init__(self)
        self.ftype = "function w"
        self.mode = mode
        self.flist = get_flist('../data/function_words/tags.closedclass.300')
        self.fwordcounts = None
        self.fwordindices = None

    def make_featureset(self):
        assert self.docs <> None
        ret = list()
        fwords_dict = dict()
        i = self.startindex

        alldocs = ''
        for doc in self.docs:
            alldocs = alldocs + doc
            self.fwordcounts = self.make_counts(alldocs)
            # Only function words occurring at least twice in data
            fwords_set = set((w for w in self.fwordcounts if self.fwordcounts[w] > 1))
            self.fwordindices = dict(zip(fwords_set,range(0,len(fwords_set))))
            
            for doc in self.docs:
                fwords = self.make_counts(doc)
                fwords_filtered = {w:fwords[w] for w in fwords if w in fwords_set}
                featureset = self.map_to_indices(self.fwordindices,fwords_filtered)
                ret.append(transform(featureset, self.mode, len(doc)))
                #ret.append(featureset)
            return(ret)

    def eval_featureset(self):
        ret = list()
        fwords = dict()
        for doc in self.docs:
            fwords = self.make_counts(doc)
            # only previously seen function words
            fwords_filtered = {w:fwords[w] for w in fwords
                               if (w,) in self.inv_model}
            featureset = self.map_to_model(fwords_filtered)
            ret.append(transform(featureset, self.mode, len(doc)))
            #ret.append(featureset)
        return(ret)
    
    def map_to_model(self,funct_counts):
        ret = dict()
        for w in funct_counts:
            ret[self.inv_model[(w,)]] = funct_counts[w]
        return(ret)

    def pass_index(self):
        return(len(self.fwordcounts)+self.startindex)

    def map_to_indices(self, indices, result_dict):
        global g_feat_names
        ret = dict()
        for w in result_dict:
            id = indices[w]+self.startindex
            count = result_dict[w]
            ret[id] = count
            g_feat_names[id] = (self.ftype,w)
        return(ret)

    def make_counts(self,text):
        """Returns {function word:raw count} dict"""
        fwords_counts = Counter()
        sentences = text.split('\n')
        for s in sentences:
            fwords = [word.lower() for word in s.split()
                      if (word.isalpha() & isfunctword(self.flist, word))]
            fwords_counts.update(fwords)
        return(dict(fwords_counts))

def isfunctword(functwords, word):
    return(word in functwords)
                
        
        
