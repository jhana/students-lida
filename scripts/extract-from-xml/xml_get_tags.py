#!/usr/bin/python

# Input as 1st argument: path/to/corpus/2014-czesl-sgt-en-all-v2
# 2nd argument: filename of list of t_ids to extract
# Output: one text file for every essay containing POS tags of texts.

import sys
import codecs
import xml.etree.ElementTree as ET

t_id = open(sys.argv[2],'r')
my_tids = map(str.strip,t_id.readlines())
t_id.close()

tree = ET.parse(sys.argv[1])
root = tree.getroot()
# Length of POS tag
n = 2

for div in root:
    id = div.attrib['t_id']
    if id in my_tids:
        f = codecs.open(id+'.tag.txt','w','utf-8')
        for s in div:
            f.write(' '.join([word.attrib['tag1'][0:n] for word in s]))
            f.write('\n')
        f.close()
