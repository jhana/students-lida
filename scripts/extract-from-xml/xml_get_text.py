#!/usr/bin/python

# Input as 1st argument: path/to/corpus/2014-czesl-sgt-en-all-v2
# Output: one text file for every essay containing raw text data.

import sys
import codecs
import xml.etree.ElementTree as ET

"""Extracts text"""

tree = ET.parse(sys.argv[1])
root = tree.getroot()

for div in root:
    id = div.attrib['t_id']
    f = codecs.open(id+'.txt','w','utf-8')
    for s in div:
        f.write(' '.join([word.text for word in s]))
        f.write('\n')
    f.close()
