#!/usr/bin/python

# Input parameter: path to file from PDT2.5 (https://ufal.mff.cuni.cz/pdt2.5/)
# (file stored in XML-based format Prague Markup Language,
# see https://ufal.mff.cuni.cz/jazz/PML/index_en.html).

# Output: TSV file: tag \t word form

import sys
import codecs
import xml.etree.ElementTree as ET

tree = ET.parse(sys.argv[1])
root = tree.getroot()
f = codecs.open(sys.argv[1]+'.tags','w','utf-8')

for s in root.findall('{http://ufal.mff.cuni.cz/pdt/pml/}s'):
    for m in s:
        form = m.find('{http://ufal.mff.cuni.cz/pdt/pml/}form').text
        tag = m.find('{http://ufal.mff.cuni.cz/pdt/pml/}tag').text
        f.write(tag + '\t' + form + '\n')
f.close()
