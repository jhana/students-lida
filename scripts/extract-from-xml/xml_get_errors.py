#!/usr/bin/python

# Input as 1st argument: path/to/corpus/2014-czesl-sgt-en-all-v2
# 2nd argument: filename of list of t_ids to extract
# Output: one text file for every essay containing error tags.

import sys
import codecs
import xml.etree.ElementTree as ET

"""Extracts error tags"""

t_id = open(sys.argv[2],'r')
my_tids = map(str.strip,t_id.readlines())
#print my_tids
t_id.close()

tree = ET.parse(sys.argv[1])
root = tree.getroot()

for div in root:
    id = div.attrib['t_id']
    if id in my_tids:
        f = codecs.open(id+'.err.txt','w','utf-8')
        for s in div:
            f.write(' '.join([word.attrib['err'] for word in s]))
            f.write('\n')
        f.close()
