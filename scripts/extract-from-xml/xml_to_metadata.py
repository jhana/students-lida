#!/usr/bin/python

# Creates tab-separated data from attributes of div element (text metadata)
# in CzeSL-SGT. 1st argument is name of corpus in xml format. 
# Output to sys.stdout.

import sys
import codecs
import xml.etree.ElementTree as ET

#obtained from http://utkl.ff.cuni.cz/~rosen/public/meta_attr_vals.html
attr_en = "t_id t_date t_medium t_limit_minutes t_aid t_exam t_limit_words t_title t_topic_type t_activity t_topic_assigned t_genre_assigned t_genre_predominant t_words_count t_words_range s_id s_sex s_age s_age_cat s_L1 s_L1_group s_other_langs s_cz_CEF s_cz_in_family s_years_in_CzR s_study_cz s_study_cz_months s_study_cz_hrs_week s_textbook s_bilingual".split()

tree = ET.parse(sys.argv[1])
root = tree.getroot()

out = codecs.getwriter('utf8')(sys.stdout)

sys.stdout.write('\t'.join(attr_en)+'\n')

for div in root:
    out.write('\t'.join([div.attrib[a] for a in attr_en]))
    out.write('\n')   
        
    
