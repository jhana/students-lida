#!/usr/bin/python

####TODO###FIX###

"""
Input:
1st argument: name of svm-light file with multi-value feature vectors as columns,
with the first (0th) column representing the label (class). 
2nd argument: name of output file
Output:
TSV file: ftrNo, information gain, conditional entropy, 1st line contains column
names.
"""

import sys
import numpy as np
from pprint import pprint
from scipy.stats import entropy, itemfreq
#from scipy.sparse import lil_matrix

from getdata import getdata
from discretize import discretize 

#N = 25 # number of bins for discretization
SEP = '\t'
CLASS_IDX = 0 #first column of matrix 

def dist(vector): #returns probability distribution of vector
    """
    @param vector: feature vector 
    @return: matrix [[X1,p(X1)], [X2,p(X2)],...]
    """
    frequencies = itemfreq(np.array(vector))
    
    return np.column_stack((frequencies[:,0],
                            frequencies[:,1]/float(sum(frequencies[:,1]))
                            ))

def computegain(infile,outfile):
    
    out = open(outfile, 'w')    
    np.seterr(all="raise") #script fails when dividing by 0
    
    #print "discretizing..." ##should already get discretized file
    #data,bins = discretize(infile, N)

    data = getdata(infile)
    classes = data[:, CLASS_IDX]
    dist_classes = dist(classes)
    classEntropy = entropy(dist(classes)[:,1])

    results = []
    #print data.shape[1]
    for ftrNo in xrange(1,data.shape[1]): #data.shape[1] - column indices
        ftrVec = data[:, ftrNo]
        #print ftrVec
        ftrVecDist = dist(ftrVec)
        condEntropy = 0
        
        # condEntropy = H(class|feat) = sum (p(x) * H(class|feat=x))
        for [x,p] in ftrVecDist:
            condEntropy += p * entropy(dist(classes[ftrVec == x])[:,1])

        gain = classEntropy - condEntropy
        results.append((ftrNo, round(gain,5), condEntropy))
    out.write("Ftr No\tInf Gain\tCond Entropy\n") 
    out.write('\n'.join((SEP.join(map(str, row)) for row in results))) # TSV
    out.close()
    
if __name__ == '__main__':
    computegain(sys.argv[1],sys.argv[2])
    ### to plot features according to information gain:
    ### cat computegain.output | cut -f2 | tail -n +2 | sort -gr > gain.plot
    ### gnuplot
    ### plot "gain.plot"
