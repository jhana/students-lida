#!/usr/bin/python

"""
Transforms data from continuous features to categorical values.
Argument 1: svm-light data file
Argument 2: number of bins
Output: "discretized" data - former floats are now indices of buckets.
"""
import sys
import numpy as np
import scipy.stats as sp
from getdata import getdata
from scipy.sparse import lil_matrix

# "Return the indices of the bins to which each value in input array belongs."
# http://docs.scipy.org/doc/numpy/reference/generated/numpy.digitize.html

#100/n is the number of bins
def discretize(fileName, n):
    data = getdata(fileName) # returns numpy ndarray
    discrete = np.zeros(data.shape,dtype=np.float)

    i=0
    for row in discrete:
        row[0] = data[i][0].copy()
        i = i+1
        
    bins = list()
    bins.append([]) #0th element

    for i in range (1, data.shape[1]):
        #c = data[:,i]
        c = data[:,i].copy().flatten()
        #print c
        #bins.append(sp.scoreatpercentile(c,range(0,100,n)))
        bins.append(np.percentile(c,range(0,100,n)))
       
        if(len(set(c)) > (100/n)): #feature is 'continuous enough'
            discrete[:,i] = np.digitize(c, bins[i])
        else:
            discrete[:,i] = c
    return (discrete, bins)


if __name__ == '__main__':
    import cProfile, pstats, StringIO
    discrete, bins = discretize(sys.argv[1], int(sys.argv[2]))
    #for d in discrete:
    #    print d

   


   
