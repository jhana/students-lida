#!/usr/bin/python

#1st argument : `train`,`devtest` or `test`
#2nd argument : directory where to save examples
#3rd argument : when devtest , list of selected features
#(3rd) 4rd+ args    : feature types, examples:
## [CG,OG,PG,WG]-N,MODE , where CG are character,OG open class POS, PG POS and W word ngrams. N is an integer (typically < 10) and MODE is one of bin,raw,rel,log.
## [ER,FW]-MODE for error types or function words
## [SL,WL] for average sentence or word length

from pprint import pprint
from discretize import discretize
from Feature_Generator import *
from feat_slength import *
from feat_wlength import *
from feat_chargram import *
from feat_wordgram import *
from feat_POSgram import *
from feat_OCPOSgram import *
from feat_err import *
from feat_functionwords import *

# Classes
pos=['S']
neg=['IE','nIE']

# Data
train = '../data/my_data/train'
devtest = '../data/my_data/dev-test'
test = '../data/my_data/test'

# Load t_id -> s_L1_group mapping
f = open('../data/my_data/statistics/t_id2s_L1_group','r')
tid_sL1group = dict(map(str.split,f.readlines()))
f.close()

# for n-grams and errors:
### bin : binary values (ngram or err is/isnt in doc)
### raw : raw frequencies (ngram or err is in doc x times)
### rel : relative frequencies (raw/len(doc))
### log : normalized frequencies (like (Aharodnik et al, 2013))

#Load generators from arguments.
generators = []
i= int()

if(sys.argv[1]=='train'):
    i=3
elif((sys.argv[1]=='devtest') or (sys.argv[1]=='test')):
    i=4
else:
    print "First arg should be `train` or `devtest`"

for arg in sys.argv[i:]:
    argsplit = arg.split('-')
    if(len(argsplit)==2):
        feat = argsplit[0]
        pars = argsplit[1]
    elif(len(argsplit)==1):
        feat = argsplit[0]
    else:
        print "Wrong argument ",arg

    if(feat in ['CG','OG','PG','WG']): #n-gram type
        n = int(pars.split(',')[0])
        m= pars.split(',')[1]
        if(feat == 'CG'):
            generators.append(Feat_Chargram(n,mode=m))
        elif(feat == 'OG'):
            generators.append(Feat_OCPOSgram(n,mode=m))
        elif(feat == 'PG'):
            generators.append(Feat_POSgram(n,mode=m))
        elif(feat == 'WG'):
            generators.append(Feat_Wordgram(n,mode=m))
    elif(feat in ['ER','FW']): #errs or funct words
        m = pars
        if(feat == 'ER'):
            generators.append(Feat_Err(mode=m))
        elif(feat == 'FW'):
            generators.append(Feat_FunctWords(mode=m))
    elif(feat == 'SL'):
        generators.append(Feat_Slength())
    elif(feat == 'WL'):
        generators.append(Feat_Wlength())
    else:
        print "Wrong parameter "+feat    

"""generators = [#Feat_Err(mode='bin'),
              Feat_FunctWords(mode="log"),
              #Feat_Chargram(1, mode='bin'),
              #Feat_Chargram(2, mode='bin'),
              #Feat_Chargram(3, mode='bin'),
              #Feat_POSgram(1, mode='bin'),
              #Feat_POSgram(2, mode='bin'),
              #Feat_POSgram(3, mode='bin'),
              #Feat_OCPOSgram(1, mode='raw'),
              #Feat_OCPOSgram(2, mode='raw'),
              #Feat_OCPOSgram(3, mode='raw')
              #Feat_Wordgram(1, mode='bin'),
              #Feat_Wordgram(2, mode='bin'),
              #Feat_Slength(),
              #Feat_Wlength()
]"""
featuresets = list()
i=1 #SVMlight numbering
j=0

if(sys.argv[1] == 'train'):
    docs = docs_to_strings(train)
    doc_names = docs[0]
    doc_list = docs[1]
    for g in generators:
        print "Now processing generator ",j
        g.set_documents(train,doc_names,doc_list)
        g.set_startindex(i)
        FS = g.make_featureset()
        featuresets.append(FS)
        i = g.pass_index()
        j=j+1
    print_feat_names(os.path.dirname(sys.argv[2])+"/../../featnames")
    
elif(sys.argv[1] in ['devtest', 'test']):
    docs = docs_to_strings(devtest)
    doc_names = docs[0]
    doc_list = docs[1]
    for g in generators:
        print "Now processing generator ",j
        g.set_documents(devtest,doc_names,doc_list)
        print sys.argv[3]
        g.model = g.load_features(sys.argv[3])
        #print g.model
        if g.model:
            g.inv_model = dict(((value,index)
                                for (index,value) in g.model.items()))
            FS = g.eval_featureset()
            featuresets.append(FS)
        else:
            print "Generator ",j," empty"
        j=j+1

doc_inx = 0 #document index
outfile = open(sys.argv[2]+'_c','w')
#print "---writing results to file---"
for i in range(len(doc_list)): #indices
    out=''
    svmlist=list()
    x=list()
    for feat in featuresets:        
        x=x+feat[i].items()
    for (k,v) in sorted(x, key=lambda x: int(x[0])):
        out=out+'{0}:{1} '.format(k,v)
    docname = os.path.splitext(doc_names[doc_inx])[0]
    if(tid_sL1group[docname] in pos):
        outfile.write('{0} {1}'.format('+1',out)+'\n')
    elif(tid_sL1group[docname] in neg):
        outfile.write('{0} {1}'.format('-1',out)+'\n')
    doc_inx = doc_inx + 1
outfile.close()

#print "---discretizing---"
discrete_feats = discretize(sys.argv[2]+'_c',5)
discrete_out = open(sys.argv[2],'w')
#print "---writing discrete to file---"
for doc in discrete_feats[0]:
    discrete_out.write("%+d " % int(doc[0]))
    for i in range(1,len(doc)):
        ##from http://svmlight.joachims.org/: features with value 0 can be skipped
        if(doc[i] <> 0):
            discrete_out.write('{0}:{1} '.format(i,doc[i]))
    discrete_out.write('\n')
discrete_out.close()        
