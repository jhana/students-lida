#!/usr/bin/python
#Word n-grams
import unicodedata, re, sys, collections
import mypath
from log_normalize import log_normalize, relative, transform
from itertools import tee, islice, izip
from ngram import cleantext, _byvalue, Counter, wordlist,my_ngrams
from pprint import pprint
from Feature_Generator import *

class Feat_Wordgram(Feature_Generator):
    def __init__(self,n,mode):
        Feature_Generator.__init__(self)
        self.ftype="word %d-gram"%(n,)
        self.n = n #n-gram size
        self.mode = mode
        self.wgcounts = None
        self.wgindices = None

    def make_featureset(self):
        assert self.docs <> None
        ret = list()
        wgrams = dict()
        i = self.startindex

        alldocs=''
        for doc in self.docs:
            alldocs = alldocs+doc
        # Creates set of all ngrams in self.docs
        self.wgcounts = self.make_wordgrams(alldocs)
        wgset = set((wgram for wgram in self.wgcounts
                     if self.wgcounts[wgram] > 1))
        self.wgindices = dict(zip(wgset,range(0,len(wgset))))

        for doc in self.docs:
            ngrams = self.make_wordgrams(doc) # Create dict of ngrams for doc
            ngrams_filtered = {ngram:ngrams[ngram] for ngram in ngrams
                               if ngram in wgset}
            featureset = self.map_to_indices(self.wgindices,ngrams_filtered)
            ret.append(transform(featureset,self.mode,len(doc)))
            #ret.append(featureset)
        return(ret)

    def eval_featureset(self):
        ret = list()
        ngrams = dict()

        for doc in self.docs:
            ngrams = self.make_wordgrams(doc)
             # only ngrams previously seen in flattened training data
            ngrams_filtered = {ngram:ngrams[ngram] for ngram in ngrams
                               if ngram in self.inv_model}
            featureset = self.map_to_model(ngrams_filtered)
            ret.append(transform(featureset,self.mode,len(doc)))
            #ret.append(featureset)
        return(ret)

    def map_to_model(self,ngram_counts):
        ret = dict()
        for ngram in ngram_counts:
            ret[self.inv_model[ngram]] = ngram_counts[ngram]
        return(ret)
    
    def pass_index(self):
        return(len(self.wgindices)+self.startindex)

    def map_to_indices(self,indices,result_dict):
        global g_feat_names
        ret = dict()
        for ngram in result_dict:
            id = indices[ngram]+self.startindex
            count = result_dict[ngram]
            ret[id] = count
            g_feat_names[id] = (self.ftype,"\t".join(ngram))
        return(ret)
       
    def make_wordgrams(self,text):
        """Returns {ngram:count} Counter"""
        wgrams_counts = Counter()
        sentences = text.split('\n')
        for s in sentences:
            wgrams = my_ngrams([x for x in s.lower().split() if x.isalnum()],
                               self.n)
            wgrams_counts.update(wgrams)
        return(wgrams_counts)

#-------------------------------------------------#

if __name__ == "__main__": 
    docs = docs_to_strings(sys.argv[1])
    doc_names = docs[0]
    doc_list = docs[1]
    
    WG = Feat_Wordgram(2)
    WG.set_documents(sys.argv[1],doc_names,doc_list)
    WG.set_startindex(1)
    WG.make_featureset()
    

    
    


            
