#!/usr/bin/python

import sys
import numpy as np
from scipy.sparse import lil_matrix

def getdata(fileName):
    """
    Converts svm-light data file to numpy array
    with rows for documents and columns for features.
    Column 0 represents document class.
    """
    #pr = cProfile.Profile()
    #pr.enable()
    
    dataFile = open(fileName)
    data = [f.split() for f in dataFile]
    dataFile.close()

    labels = [int(row[0]) for row in data]
    number_of_rows = len(labels)
    feats = [row[1:] for row in data]

    cols=[]
    for f in feats:
        cols.extend([int(x.split(':')[0]) for x in f])

    #get maximum feature index
    number_of_cols = max(cols)
    sparse=np.zeros(shape=(number_of_rows, number_of_cols+1))

    #build sparse
    for i in range(0,len(feats)):
        #print feats[i]
        for v in feats[i]:
            f_index = int(v.split(':')[0])
            f_value = float(v.split(':')[1])
            sparse[i][f_index] = f_value
    
    i=0
    for row in sparse:
        row[0] = labels[i]
        i = i+1

    """pr.disable()
    s = StringIO.StringIO()
    sortby = 'cumulative'
    ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
    ps.print_stats()
    print s.getvalue()"""
    return sparse
    
if __name__ == '__main__':
    import cProfile, pstats, StringIO
    gdata = getdata(sys.argv[1])
    #for i in range(0,len(gdata)):
    #print gdata[i]
