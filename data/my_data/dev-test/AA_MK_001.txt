Život na kolej vs. život doma
To je pro mě otázka , kterou je těžká odpovědět , protože jsem žila na koleji jenom 4 týdny během letní školy v Olomouci .
Tak nemám moc zkušeností , ale co jsem tam přožila , musím řict , že život na koleji má dvě strány .
Zaprvé , a podle mě je to to nejlepší výhoda života na koleji , je tam svoboda .
Tam není ani mamínka ani tatinek kteří ti říkají co máš dělat a co nemáš dělat .
Zadruhé , pokoj na koleji je ta nejlevnější mošnost abys žil bez rodiču .
Zatřetí , na koleji nejsi nikdy sám .
Vždy a všude jsou lide , kteři s tebou chtejí mluvit nebo něco dělat .
Je to skvělé , když jsi poprvé ve městě a ješte neznáš nikoho .
Za pět minut máš nové kamarády a když potřebuješ někoho , třeba aby sis promluvil nebo pro něco jiného , koleji je spravné místo .
Ale , a teď přijdeme do nevýhody života na koleji , když nejsi tak otevřený a nepotřebuješ 24 hodin za den kontakt s lidmi , život na koleji může být opravdu náročný .
Může to být toky hodně náročné , když maš spolubýdlící se kterou nevyjdeš dobře .
Já stím nemám zkušenost .
Ma Japonka v Olomouci byla tak klídná , že jsem si občaš nevšimla že je v pokoji .
Ale mám kamarády kteří měli méné šteští než já .
Jejich spolubydlící opravdu nedělají ohled a potom život není příjemný .
Jedna další nevýhoda na koleji je , že musiš dělat všechno sám .
Tam není maminka , ktera vaří , pere a uklízi .
Myslím , že když jsi mladý a otevřený život na koleji může být skvělá zkušenost .
Ale po roce nebo dvou můžu chápat že to stačí .
