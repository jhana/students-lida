Moje pracovní zkušenosti Doufám , že psát o pracovních zkušeností neznamená nuceně , že musím se představit z nejlepší strany .
Není to tak , že nemám žadnou pozitivní zkušenost , ale napišu o tomu radši počestněji a zaroveň zabavněji než v normalním uchazení nebo něčím podobného .
Vzhledem k tomu musím to všechno začínat s doznáním , že jsem nikdy nekonal žadnou " praktickou " práci .
Myslím , že jsem sbíral první ( placené ) zkušenosti s soukromými hodinami pro jednoho o pět let mladší žáku .
Vždycky jsem ale cítil jako podvodnící vůči němu , když jsem pouze dělal jeho domácí ukoly s ním a zkoušel jich vysvětlit .
Mezitím realizuju , že jsou dokonce i hodně učitelů , kteři nedělají moc vic než to .
To bylo sledované několika pokusy vydělávat peníze s internetovými stránkam - činnost , v kterém jsem měl aspoň trochu tvůrcí volnost a věděl , že na konci bude něco nového .
Vstoupit do univerzity neznámenal v první řadě , že kladl bych nové požadavky o práci , ale spíše , že jsem dostal směnou studijní prostoru docela nové předpoklady a šanci , abych se odvážil něčeho nového .
Takže se přihodil , že jsem našel oznámení , v kteře se hledal pro jeden dokumentární film asistentní střihač s " filmovými zkušenostmi . "
Nu , věděl jsem jak moje nedokonalá kamera fungovala a uměl zpracovat video jedným počitačovým programem , ale to bylo jiné .
Nevěděl jsem nic o tomu programu a vůbec nikdy jsem nedělal nic profesionalního .
Tak to určitě byla usměšná situace , když jsem seděl vedle mé spoluuchazečky a jsem se nervozně koukal v poslední minutě na některé informace o zacházení toho programu .
Ať to bylo jakkoli dostal jsem tu práci zřejmě protože ta druhá uchazečka měl čas jenom dvakrát tydně mezitím já jsem měl pouze několik kursů v univerzitě a mimoto spoustu času .
Na konci ani ta vlastní práce nebyla tak užasná a jsem trávil většinu mého času ( ať už den nebo noc ) vedle univerzity v tom pokoji a digitalizoval jsem jednu kazetu po druhou až když již nemohl snášet melodie toho symfonie , která byla jako hlavní tema filmu natáčen minimalně šesti kamerami .
Nicméně ta práce mi pomáhala nejvíce v tom , že jsem se v mé předchozí zaměstnání věnoval sam vlastním projektu vuniverzitě .
Planoval jsem tam celou řadu věcí , aby můj ústav mohl digitalizovat staré VHS pro audio-visualní archiv s materiály od cajdáků do celovečerních sci-fi filmů .
Ta digitalizace samo o sobě jako dřiv není nejzajímavější činnost , ale všechno okolo ní a ten vedlejší efekt , že takový byro je docela praktický pokud ještě nebydlím v stejném městě jako studuju , tak daleko povážuju za optimalní podmínky .
Poněvadž jsem po několika docela administrativní tutorátů , což většinou znamenalo xeroxování , začinal řidit vlastní " kurz četby " o různých kulturních teoriích , myslím , že rád nebudu opouštět to akademické prostředí tak brzo .
