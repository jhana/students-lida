Město , kde se dobře žije
Podle mně domov je místo , v kterém cítí jako doma .
Znamená to , že neexistuje jenom jedno město , v kterém se dobře žije .
Znám už dvě místa , kde mně se baví .
Praha a Helsinky jsou různá místa , ale mají nečo taky stejné .
Oba jsou hlavná mista malých států v Evropě .
Praha leží v střední Evropě a Helsinky v Severovychodní Evropě .
Praha má Vltavu a Helsinky mají Baltické moři .
Praha je historickější a Helsinky modernější .
Oba však mají srci plná života se svým způsobem .
Teď žiju v Praze víc než půl roku .
Poprvé jsem se dívala na Prahu cizími očima , ale přizpůzobuju postupně .
Občas Praha je nejlepší místo na světe .
Červené tramvaje jezdí po starých ulicich .
Všude jsou útulné kavárny .
Taky má výhody : v léte jsou hodně turistů a je příliš horko pro mně , protože mám rádeji přímořské podnebí .
Když odjedu z Prahy budu se styskat po ní .
Jsem jistá , že chtěla bych se vratit jednoho dně .
Člověk může odjet z Prahy , ale Praha zůztane uvnitř člověka .
