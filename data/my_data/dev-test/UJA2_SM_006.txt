Má kamarádka se jmenuje Eva .
Ona patří k mým nejlepším kamarádkám .
Znám ji už 5 let .
Pracuje se mnou v kancelaři jako pravnička .
Je vysoká a štihlá .
Má dlouhé , tmavé , kudrnaté vlasy a modré oči .
Lidé jí mají rádi , protože má zlaté srdce : vždycky se snaží všem pomoct a myslí na ostatní .
Je moc chytrá a pracovitá , ale nenosí nos nahoru .
Je upřímná a vždycky řekní co si mysli .
Je také moc trpělivá .
Ráda cestuje , a proto často jezdime na výlet spolu .
Také ráda sportuje a vaří .
Jidla , která připravuje , mi moc chutnají .
Bohužel , jsem ji neviděla už dlouho , ale , doufam že přijede ke mně na navštěvu v Prahu v letě .
