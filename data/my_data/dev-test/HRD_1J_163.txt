Proč mám ráda Českou republiku
V následující práci bych se ráda zamyslela nad svým vztahem k České republice .
V zemí žijí již jedenáct let a narodila jsem se v Středoasijském Kyrgyzstánu .
Česká republika se nachází ve středu Evropu a má bohatou historií .
Přímo v srdci Evropy se odehrávala řada významných událostí , bitev a české země navštívilo mnoho význačných osobnosti , kteří po sobě zanechali cenné prameny a památky .
Česká republika se může pyšnit stověžatou Prahou , jedním z nejkrásnějších měst v Evropě .
Toto unikátní hlavní město láká ročně tisíce turistů z celého světa .
Obyvatelé České republiky však mají stále v paměti hořkou komunistickou minulost .
Soudobé dějiny se odráží v mentalitě Čechů a především v jejich vztahu k cizincům z východní Evropy .
Proces vyrovnávání s komunistickou minulosti započal v devadesátých letech a stále pokračuje .
To je možné například sledovat na české zahraniční politice , která se snaží distancovat od Ruska a přiklání se k prozápadní podpoře .
Západní kultura se střetává s větším zájmem obyvatelstva .
Existuje řada argumentů , nad kterými by se dalo uvažovat .
Česká republika se pro mě stala druhým domovem , ale necítím vlastenectví k žádné zemi .
Obdivují českou kulturu a historií , ráda sledují proměny země na časové ose , od počátků po současnost .
Přesto nemůžu souhlasit s izolacionismem vůči východní Evropě a postsovětským republikám .
