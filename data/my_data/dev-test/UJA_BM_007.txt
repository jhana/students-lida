Příslov
Když jsem byla mladší , milovala jsem jedného kluka .
Pořád jsem myslela o něm , a čekala jsem nějakou šanci , abychom hovořili spolu .
Jeden den ten kluk přišel ke mně , a pozval mi na večeře .
My jsme měli velmi šťastnou chvíli , myslela jsem , že budeme chodit spolu .
Ale za týden ten kluk mělo novou přítelkyně .
Takže to znamená , že " Jedna vlaštovka , jaro nedělá . "
Potom jsme se neviděli , a zapoměla jsem na něj velmi rychle .
" Sejde z očí , sejde z mysli . "
