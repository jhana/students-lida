Konzum
Dovolte mi drazí čtenáři povědět vám něco málo o tom , co je to konzum - jak já jej chápu - a říci vám , zda jsem konzumním člověkem nebo ne a v případě , že ano , tak jestli vám doporučuji být ve stejné kategorii konzumentů , jako jsem já či ne .
Nedávno jsem na hodině základů společenských věd ( jsem totiž studentem třetího ročníku gymnázia ) v souvislosti s probíraným učivem - EKONOMIKY dostal od vyučujícího papír malého formátu , který byl potištěn článkem na téma konzum .
Autorem textu byl uznávaný sociolog Jan Keller .
Ten ve své stati tvrdí , že konzumem můžeme rozumět orientaci na materiální statky , která vytlačuje - coby druhořadé - hodnoty nemateriální atd .
Poté , co jsem si přečetl tuto definici konzumu v úvodu , ihned jsem musel naprosto jednoznačně souhlasit .
Jinak si konzum ani neumím vysvětlit .
Aspoň ne teď , v tuto chvíli , v době , kdy dospívám .
Nevylučuji , že po určité době se můj názor v chápání tohoto pojmu může změnit .
Samozřejmě jsem si vědom , že jednou k tomu dojde pochod s tím , který už byl a již není .
Bude to jistě velmi zajímavé srovnání .
Dopředu chci prozradit , že se těším , až tato situace nastane .
Co se týče odpovědi na to , zda jsem konzumní ( lpím na materiálních hodnotách ) nebo ne , tak vám jednoduše sdělím , že mě můžete považovat za konzumenXXX v každém případě .
Víte , narodil jsem se na počátku demokracie - kapitalismu a žádné jiné prostředí než peníze , sláva , moc , užívání si soukromého vlastnictví " za své " neznám , a tudíž mi vše přijde normální .
Nevidím nic špatného na konzumním stylu života .
Pochopitelně , važení čtenáři , zde máte nyní prostor pro polemizování s mými tvrzeními .
Namítejte cokoliv !
Jistě jste možná zkušenější ( zažili jste jiné doby ) a můžete směle tvrdit , že dnešní život je horší , než jaký jste měli kdysi ( právě za té vaší jiné éry ) , nebo jste stejně staří jako já , a přesto vám nevyhovuje konzum a radši byste upřednostňovali méně materiální způsob žití .
( Třeba žít více v souladu s přírodou , což souvisí s konzumem a to tak , že někteří lidé tvrdí , že právě on způsobuje její drancování , chcete - li můžem říkat vykořisťování , jež má za následek její nevratné poškození . ( tedy většinou ) )
Ale to bych já , jakožto obhájce konzumu , neměl říkat .
Vždyť takto nahrávám těm , kteří se mnou nesouhlasí .
Mám - li mít závěrečnou řeč , pak vám , pozorní čtenáři , oznamuji :
" Žijte konzumně !
Každý z vás se narodil jednou a nikdy více nebudete mít příležitost úžívat si .
