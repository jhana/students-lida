Narozeniny obvykle slavím v malé skupině rodiny a kamarádů .
Připravuju večírek v svým bytě a volám svým bratrům a kamarádům , abych ji pozvala na oslavu .
Jídlo připravuju sama , v čem mi můj manžel pomáhá .
Je to obvykle nějaké netypické malé občerstvení .
V obchodě kupujeme nápoje : pivo , víno a džusy .
Když je to den , v kterém musíme pracovat , večírek se začína pozdě - v 7:00 nebo 8:00 hodín , aby všichni mohli přijit po praci .
Když mé narozeniny jsou o víkendu , setkáváme se s kamarády dřiv , napříklád už v 2:00 , a máme tuto schůzku často venku .
Když je hezké počasí , líbí se mi slavít narozeniny v parku nebo na pláži .
Moji kamarády a rodina mi vždycky přinesou hezké dárky .
Všichni se dobře a vesele bavíme při pivě , vině a muzice .
