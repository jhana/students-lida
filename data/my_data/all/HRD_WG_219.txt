Čínský pověst
V číně má jeden tradiční národní festival se jmenuje Duan Wu .
Duan Wu se koná vždy pátý den v pátém měsicí čínského kalendaře .
Duan Wu je festival , který vzpominá jednoho čínského spisovatele a politika .
Jmenuje se Qu Yuan ( Qu Ping ) .
Qu Yuan je hodný politik .
On stará se o obyvatele a země .
Ale k mu císař nevěři .
Císař dělal velkou valku a utrpí porážku .
Qu Yuan je velmi smutný .
Jeho země už není .
Qu Yuan psál zamou poezii a pak padá v řece MiLuo .
Lidi ví , že Qu Yuan si zemřel .
Jsou smutný , veslovají loď na řece Mi Luo a hredají jeho mrtvolu .
Ženy vaří specialní jídlo - Zong Zi , hodí je do řeky MiLuo . uzpominá Qu Yuan .
Kvůli Qu Yuan si zemřel v 5:5 , proto festival se koná v 5.5 .
( Qu Yuan psál hodně poezii .
On jako jeden zakladatel čínské klasické literatury . )
