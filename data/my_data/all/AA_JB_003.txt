Domácí úkol 11 . 5 . 11
Řídí náš život media nebo reklama ?
V naší době je důležitou otázku , jestli reklama nebo media řídí náš život .
Neřikala bych , že řídí život , ale mají velký vliv na společnost .
Svět bez reklam už není možný .
Dnes máme reklamu všude a media hraje větší roli než dříve .
Jsme vždycky obklopováni s televizí , radiem nebo internetem .
Nemůžeme skoro žít bez těch moderních věcí , protože jsme už tak závislé .
Dneska všichni musí být pořád dosažitelné , to je hrozné .
Media a reklamy nám říkají , co musíme dělat , mít , být nebo koupit .
Je trochu smutné , protože jsme ovlivňováni tak moc .
Media má různé formy : intenet , noviny , radio , časopisy a tak dále .
Nenajdemetam jenom informace , ale také zábavu .
Ale media často funguje jako instrument .
Podniky , politici a vlády použivají šanci dostihovat nejvíce lidí .
Musí prezentovat něco nápadného , nic obvyklého .
Existuje trend , že všichni chtějí lidi šokovat .
Proto nějaké nezávislé instituce kontrolujou reklamu a také mediu .
Na jedné stráně to je dobré , protože reklamy můžou být sexistické , urážlivé nebo jinak spatného .
Na druhé straně v naší době je dovolené říct a psát , co chcete .
A to je něco velmi důležitého .
Myslím v budoucnu vliv bude ještě víc a musíme dát pozor , že reklama a media nás nekontrolujou a řídí náš život .
