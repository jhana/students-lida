Proč studovat dánštinu ?
Nevím , proč dánština je popularní studie v Práze .
Zeptala jsem studenty , a každý má svůj důvod .
Jedna říka , že svůj tatínek myslí , že Dánsko je dobry zemi s zdravou ekonomikou a je tam dobře pracovat .
Jedna má v Dánsku přítele a chci mluvít dánsky s ním .
Jedna má radá Hansa Christiana Andersena , dánský autor , nebo Soren Kierkegaard dánský filosof , a chce studovat texty v původním jazyce .
Myslím , že je to náhodnost , že studenty studujou dánštinu a ne norštinu nebo švédštinu .
Danština začíná jenom jednou za dva roky , a často studenty neznají rozdíl mezi Dánskem , Norskem a Švédskem .
