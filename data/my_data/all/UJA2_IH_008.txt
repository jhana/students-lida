Počet nehod se každý rok zvyšuje .
Je to vidět v televizi , novinách , časopisech .
Často ale o tom říkají i v radiu .
Víc a víc na ulici vidíme harovaná auta .
Já si myslím , že nejvíc to ovlivňuje kultura řidičů , kultura jízdy .
Odpovědný člověk nepojede autem , jestliže před tím vypil alkohol , protože ví , že to je moc nebezpečně pro něj , a pro ostatní lidi taky .
Serjozní člověk nebude riskovat i jet šilenou jízdou , protože ví , že odpovídá nejen za svůj vlastní život , ale za své přibuzné , lidi na cestě .
Záleží to na postavě řidiče taky .
Je to dobře , že Ministerstvo dopravy , vláda mají zájem o ten problem .
Je to nutné pořád o tom mluvit , psát , malovat , aby všechný lidi věděli , že vůbec ten problem existuje , že to je nejen problem řidičů , ale chodců , lidí na kolách .
V ramce tohoto programu , myslím , je důležité ukazovat videofilmy s nehodami v autoškolách , aby budoucí řidiči viděli to na vlastní oči .
Já osobně podporuju všechný programy , aktivity , a. t. d. na tento tema .
