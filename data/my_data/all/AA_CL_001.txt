Proč mám ráda Českou republiku
Už žiji v České republice asi čtyři měsíce a myslím že všechno je v pořádku .
Když jsem začínala studovat češtinu , téměř všichni mi kladli stejnou otázku - Proč sis vybrala češtinu a víš moc o České republice ?
Ve skutečnosti je to jinak .
Tehdy jsem musela se rozhodnout mezi dvěma možnostmi : češtinou a rumunštinou po absolvování střední školu .
Myslela jsem , že Praha je romantická jako v pohádkovém světě a pak jsem si vybrala češtinu .
V roce 2009 jsem začínala studovat češtinu a věděla jsem českou historii a literaturu v Číně z kurzu , který se jmenuje Statní Situace České republiky .
Zajímám se zvlaště o české hudebníky , protože mám ráda hrát na piáno .
Říká se , že každý Čech ( Češka ) je umělec ( umělkyně ) , proto se mi moc líbí hudební dílo od Smetany .
Myslím , že všude tady je plno romantiky a umění .
Znala jsem Českou republiku jen z doslechu až do roku 2010 , kdy jsem zúčastnila letní školy v Karlově univerzitě v Praze .
Tehdy jsem měla příležitost vědět více o České republice .
Každý pracovní den jsem jela travají na kurzy gramatiky a converzace v Filoyofické fakultě Karlovy univerzity zatímco jsem jela autobusem na výlet na venkově s kamarády a kamarádkami o víkendu .
Návštivila jsem mnoho historických památek a věděla jsem dobře české dějiny .
Líbila se mi moc česká přírodní krajina .
Už jsem viděla letní Prahu a teď mám příležitost cítit zimní Prahu .
Zimní Praha je pěkná jako v obraze , která mě opojila .
Když mám volný čas , obvykle chodím ven z koleje a prochazím po starém městě , abych cítila Prahu detailně .
Budu zůstávat v praze až do zaří a často mám návštěvu z kamarádů a kamarádek .
Všechno v České republice je v pořadku a myslím , že jsem vynesla moudré rozhodnutí .
