Volný čas
Ve volném čase bylo nejlepší , abychom nic nedělali .
Člověk je takové zvíře , které se tím liší od ostatních , že pořád něco dělá .
Ale zase zní otázka co je to " nic " ?
Je " nic " spánek ?
Možná , že ano , protože v průběhu spání mozek má možnost si trochu oddechnout od práce .
Nejde ( Není možná ) , aby hlava přestala vůbec pracovat .
To by znamenalo , že jsme mrtví .
Ale je to fakt , že když spíme práce mozku se úplně mění .
Nevědomost v průběhu spání je podobná meditací .
Pravě proto je pro mne nejlepší během volného času spát .
Ale to nevypadá to tak , že pořád spím .
To taky nedoporučuji ostatním .
Nejlepé se spí jestli dřive máme možnost sportovat .
Je to nejjednodušší způsob na hezké sny a zatím dobré ráno .
Jestli někdo nemůže spát , tak doporučuji cestování .
Podle Claudio Magrisa - italského spisovatele , ale taky Zygmunta Baumanna - polského sociologa , aby jsme odstranili problemy obyčejného života měli bysme cestovat .
Skutečné cestování musí probíhat nejlépe samotařsky .
Neměl by to být výlet s cestovní kanceláří , protože ustřední je dobrodružství , setkání s lidmi , poznavání cizích zemi .
Můj názor prostě je spojený s potřebou osamělého trávení času .
Volný čas je , když na ničem nezávísíme a nejsme moc zodpovědní ( skutečnou hranici svobody je vždy druhý člověk ) .
Musíme si udělat svůj vlastní časový prostor pro něco co bude na nás dobře působit .
Možná , že pro ostatních bude to něco jiného než spání , běh , četba , ale to vůbec není duležité .
