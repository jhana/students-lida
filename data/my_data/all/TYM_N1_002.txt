Úvod
1 .
Svátky v Uzbekistán :
Státní svátky a tradiční svátky Státní svátky :
2 .
Tradiční svátky :
21 března - Nowruz , hait ramadánu ( Eid-al-Fitr ) , Kurbon hait ( Eid-al-Adha )
3 .
Ostatní svátky
Je pravidlem , každá země a každý národ má své velké Státní a tradiční svátky .
Dnes budu mluvit o prázdniny v Uzbekistánu .
Existuje mnoho svátků v Uzbekistánu , včetně veřejných a tradičních svátků .
Státní svátky jsou následující :
1 . ledna - Nový rok , 2 . 14 leden - Den obránců , 3 . 8 . března - Mezinárodní den žen , 4 . 9 . května - Den paměti a cti , 5 . 1 . září - Den nezávislosti , 6 . 1 . října - Den učitelů , 7 . 8 . prosinec - Den ústavy
Nebudu mluvit v detailech o těchto svátcích , ale měl jsem v úmyslu mluvit o tradiční svátky v Uzbekistánu ( nebo Střední Asie ) .
Uzbecké tradiční svátky patří :
1 . 21 . března - Nowruz 2 .
Hait ramadánu / Eid-al-Fitr ) 3 .
Kurbon hait ( Eid-al-adhá )
Podle mého názoru tyto svátky jsou obecně náboženské svátky .
Poďme do detailů a diskutovat .
Navruz
Navruz ( také volal Noruz , Nowruz a Nawruz ) , jarní prázdniny " Nový rok " , byl oslavován za více než 2500 let , možná tak dlouho , jak 5000 let .
Pocházejí z Persie a dlouhé spojené s dávnými Zoroastrian náboženství , jeho jméno znamená " nový den " , protože se v Perština starověké perské znamenala první den nového roku .
Dnes Navruz se slaví každý rok 21 . března , kdy Slunce vstoupí do znamení Berana na astrologické kalendáře .
Dnes se slaví Navruz velmi pestře a v Íránu , Ázerbájdžánu , Afghánistánu , Uzbekistán , Tádžikistán , Kyrgyzstán , Kazachstán , Turkmenistán a západních provinciích Číny , jakož i Kurdů v Turecku , Sýrii a Iráku a Tataři a Baškirové v jižním Rusku .
V posledních deseti letech , středoasijských republik uznali Navruz jako státní svátek .
Jeho oslava je poznamenán koncerty v parcích a náměstích , veletrhů a národních dostihové závody .
Na rozdíl od západní tradice Nového roku , Navruz je slaven v denních hodinách v rodinném kruhu .
21 . března je hlavní oslavy , ale pro příštích dnech je běžnou praxí , na návštěvu přátel a příbuzných , koupit setí rostlin a ovocných stromů a mají veselé shromáždění v čerstvém jarním vzduchu .
Tradičně je také čas na " uklidit " něčí život .
Lidé , uklízet své domovy , praní koberců a závěsy , ozdobte květinami , a kupovat nové oblečení , které se budou používat za návštěvu .
V den Navruz všechny domácnosti - včetně přípravy jídla , pečlivě úklid domů a uspořádání větví z kvetoucí meruňka , broskev , granátové jablko nebo mandlové stromy - musí být dokončen před povstání hvězda jitřní .
Děti těší na dovolenou , protože si často dary , peníze , stejně jako požehnání od svých starších .
Střední Asie má vlastní Navruz tradice .
Z dávných dob , svátek byl slaven v zemědělské oázy s festivaly , bazary , dostihy , a psí a kohoutí zápasy .
Dnes , Uzbekové stále sloužit tradiční jídlo " sumalyak " , který chutná jako melasa - ochucený krém pšenice a je vyroben z mouky a naklíčených zrn pšenice .
Sumalyak je vařená pomalu ohně , někdy s přídavkem koření .
Naklíčené obilí je symbolem života , tepla , bohatství a zdraví .
Tento svátek se slaví na začátku jara , kdy všechny rostliny a stromy vzkvétat a získat nový pohled na jaře .
V tomto pridverii dovolenou , mnoho rodin se připravují uzbecké národní pokrmy , jako jsou :
Halim , Cook Soames , pilaf a další .
Tyto Uzbek jídla obsahují velké množství vitamínů , které jsou užitečné pro lidské tělo .
V současné době , Nowruz je oslavován každoročně na ploše Ališera Navoi .
Hait ramadanu
Druhý svatek je Hayit , to je také nábožensko-duchovní svátku muslimů .
Tento svátek je známé jako Ruza Hayit , se shoduje s 9 . měsíc ( Hizhry ) na muslimském kalendáři .
Tento svatek zahrnuje obřad jako Eid , která trvá 30 dní a je považována za obřad duchovní a morální očisty .
Podmínky obřadu takto : od východu do západu slunce nesmí jit a pit , jidlo a voda , vedené od zlé myšlenky , předpoklady , se všichni ostatní budou respektovat a pokud možno udělat více dobrého pro ostatní .
Eid ul-Fitr trvá tři dny oslav v muslimských zemích ( den v Uzbekistánu ) a je také známý jako " Menší Eid " .
Obecný pozdrav během tohoto svátku je arabský pozdrav ' id Mubarak ( " Požehnaný Eid " ) , nebo ' id Said ( " Happy Eid " ) .
Navíc , mnohé země mají své vlastní pozdravy na místní jazyk a tradice .
Uzbeckými lidé říkají , ' Hayitingiz bilan ' , ' Hayit muborak ' , ' Ayyom muborak ' .
Typicky , muslimové probudí brzy ráno a mají malou snídani ( jako znamení , že nebyl na rychle se v ten den ) , nejlépe v den ovoce , než navštěvovat speciální Eid modlitby ( Salah ) , která se provádí v mešitách .
Muslimové jsou vyzýváni , aby se oblékli do svých nejlepších šatů ( nový , pokud je to možné ) pro tuto příležitost .
Obřad se skládá z modlitby ( namaz ) , Chutba ( kázání ) a pak prosba ( dua ' ) , která žádá o odpuštění , milosrdenství a pomoc pro všechny živé bytosti v celém světě .
To je pak obvyklé přijmout osobami , jež sedí na obě strany od sebe , zatímco jejich pozdrav .
Po modlitbách , lidé také navštívit své příbuzné , přátele a známé 2 , a někteří lidé také zaplatit návštěvy hřbitovů ( ziyarat al-qubur ) .
V tomto měsíci nejpodivnější věc je Nisholda - bílá tekutina pot .
Vyrobeno z kořene stromu .
Ale v Taškentu ( Uzbekistán kapitálu ) , lidé navštívit rodinu nové nevěsty .
Každá rodina vaří ' Bugirsoq ' .
První den Ramazánské hait " není pracovní den , a všechny ostatní instituce v této slavnostné den .
Třetí je Kurban Hait .
Eid al-Adha ( arabsky :
XXX ) " Festival of Sacrifice " , nebo " Větší Eid " je muslimský svátek slaven muslimy ( včetně drúzové ) na celém světě na památku Ibrahima ochotu obětovat svého syna Ismael jako akt poslušnosti k Bohu .
Náboženský svátek Kurbon Chait je jedním z největších festivalů na světě , který je slaven muslimy .
Zdrojem tohoto svátku jsou starobylé příběhy , které se vztahují k protoka Ibráhíma , kteří ctí svou víru by obětoval " Bůh " svého vlastního syna , ale jeho akce byly zastaveny dobré duchy , a místo toho mu obětovat , jiná zvířata , jako jsou : ovce , velbloudi atd .
Vzhledem k tomu pak , všichni muslimové musí obětovat zvířata v těchto prázdnin .
Svátek trvá tři dny a v těchto dnech všichni muslimové slavit tento svátek s rodinou a přáteli .
Zejména v těchto sváteční dny lidé navštěvují své příbuzné a přátele , stejně jako nemocný a potřebuje pomoc .
První den festivalu " Kurbon Chait " není pracovní den , a všechny ostatní instituce v této slavnostní den .
Eid al-Adha je posledně dvou Eid festivalů slaví muslimové , jejichž základ pochází z Koránu .
Podobně jako Eid al-Fitr , Eid al-Adha začíná s krátkou modlitbou následuje kázání ( khuXXXba ) .
Po modlitbě lid obětoval porážky zvířat .
Ne zbídačené osoba je ponechán bez obětní jídla v těchto dnech .
V některých zemích se rodiny , které nemají vlastní zvířata mohou přispět na charitativní účely , které bude poskytovat maso těm , kteří jsou v nouzi .
Eid al-Adha každoročně připadá na 10 . den měsíce dħul Hijja ( XXX ) z lunárního islámského kalendáře .
Slavnosti trvají tři nebo více dnů v závislosti na zemi .
Eid Al-Adha nastane den poté dirigování hadždž poutníci , každoroční pouť do Mekky v Saúdské Arábii muslimy na celém světě .
Stává se , že je přibližně 70 dnů po skončení měsíce Ramadánu .
Muži , ženy a děti , se očekává , že ve svých nejlepších šatů oblečení pro výkon Eid modlitby v mešitách .
Každý rok více než 5000 občanů Uzbekistánu , slaví tento svátek v Saudské Arábii .
Stejně jako v prázdninových Hayit , lidé si navzájem pogratulovat říkat ' Hayitingiz bilan ' , ' Hayit muborak ' , ' Ayyom muborak ' .
Kromě toho existují některé svátky , které se slaví od národa .
Jedná se o tzv. sezónní dovolené .
Například , Flower dovolené ( Kvetoucí den ) - slaví v pozdním létě ( asi v srpnu ) , Qovun sayli - Meloun show ( zejména oslavila na podzim ) , Sklizeň dovolená ( na podzim , po sklizni ) .
Uzbecky muzy .
45% obyvatel je mužy . 99% lidí umí číst a psát .
Uzbecké muži jsou velmi zručný a pracovitý .
Jsou starostlivý otec , dobré manžel je rodiny poskytovatel se všemi náležitostm a potreb .
Typické Uzbekcke muži mají černé oči a černé vlasy .
Například já .
Obecně mužů , kteří jsou ve vesnicích , práce v jejich hospodářství , řemeslníci , a někteří pracují jako inženýři .
Tyto profese byly z jejich rodinného dědictví předků .
Pracují od 8:00 do 17:00 , někdy i noční směny .
Ve městech většina mužů pracuje na veřejných místech , jako jsou vládní úřady , soukromé firmy .
Máme mnoho zpěváků , herců a sportovců .
Respektují ženy dobře .
Jsou to šéf ve své rodině .
Většinu času se rozhodnout , co dělat v rodině .
Tam je rodinná tradice nejmladší syn bydlí s rodiči , zatímco ostatní vzdálit .
Alespoň jednou týdně členové rodinného domu navštívit své rodiče .
Uzbecky muži jsou hrdí na své děti , spíše než jejich bohatství . v případě , že mají mnoho dětí , zejména chlapci , se cítí jako bohatí .
Existuje komunita nazývá Mahalla .
Tato komunita spravuje ( neoficiální ) bloky města .
Existuje rada Mahalla .
Obyvkle členy představenstva jsou muži .
Pokud existují nějaké problémy spojené s rodinami nebo mládeže , které je v klidu vyřešit způsobem .
Většinu času , diky v této souvislosti Mahalla , existuje jen málo rozvodů a trestné činnosti .
Jedná se o takzvaný self-kontrolního orgánu .
Ve svém volném čase , muži bez ohledu na jejich oblíbenou věc .
Například , že pracují v jejich zahradě , jděte na " Choyxona " , zvláštní místo pro muže sbírat a vařit " pilow " a pít čaj .
Někdy jdou na koncerty nebo divadla .
Mají málo volného času , když tvrdě pracovat .
Obecně uzbecké lidé rádi tvrdě pracovat a vydělávat peníze , jen pro své děti a mají velké svatby .
Máme " Uloq " Hra pro muže během svatby .
V této hře muži mají zachytit a uchopit ovce při jízdě na koni a přivést ji na správné místo .
Tradiční kroje jsou stále často nosí Uzbeks .
Muži nosí doppilar Duppi je naše národní klobouk .
Je to barva je černá a má bílou barevnou tvar .
Jedná se nosí na speciální příležitosti , jako jsou svatby , náboženské svátky ...
Někteří muži nosí tradiční chopan , dlouhý prošívaný plášť původně používaný pastýři .
Většinu času muži nosí západní-styl oblečení nebo oblečení , které spojují západní a tradiční uzbecké styly .
A existuje přísloví pro muže uzbečtina : čtyřicet profese není dost pro muže .
