Jsem tramvajový fanoušek , a proto , dovolenou si nedovedu představit bez tramvají .
Samozřemě modení tramvajové systemy jaké v Praze kde tichá lesklá vozidla plachtí jako labuti podel řeky , přitahují každého skutečného tramvajího fanouška .
A já připustím , že dovolená v těch moderních mestech má přednost před dovolenou kdekoliv tramvaje není !
Ale , moje oblibené tajné fantazie jsou o starých zanelsráních vozidlech , která poskakují a kolebají se v jezdi po uzkých a klikatých ulicích .
Samozřemě na poslední zastavce každé linky každého systemu existuje hospoda ( obvykle vyčerpaná ) dovnitr které zmizí cestující a řídic aby udusilí žízeň pivem , a občas hlad s jidle sporné hodnoty .
Pro pravé tramvajové fanoušky , nejlepší je poslední denní jízdy - obyvykle o půlnoci .
Řídic , posílený z hospody chcí pospíchat do vozovny , aby skončil praci .
Staré motory skučí , kola křičí proti trasách , a vozidlo se kolebá pořad až do přijezdu do vozovny .
Protože to bylo poslední jízda každy fanoušek potřebuje ubytování blízko vozovny a tak vyběr hotelů není široký .
Tramvajoví fanoušci se učí brzo , že neexistují žadné čtvrhvězdové hotely blizko vozoven , i taky žadné tři - , dvě - , jednohvězdové hotely .
Obyčeni majitelé hospod v tramtarii sotva umí co s hosty dělat , a obyčeni zákazníci v baru pokukují s zvědavosti na nesrozumitelné lidi , kteři chtějí zůstat v jasném minushvězdovém statku .
Ale pravný fanoušek je spokenený stím .
Má všechno na pefektní dovolenou - doprava , jidlo a pivo , a ubytování .
Co víc potřebuje na dovoleně ?
Nerozumím jedné věcí - proč jsou většina tramvajových fanoušků svobodni ?
