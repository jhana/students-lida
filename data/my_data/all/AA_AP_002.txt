Jak v Bělorusku slaví Vánoce ?
Musím přiznat , že já o tom mnoho nevím .
Nebo nevím nějaké specifiské běloruské národní tradice , protože vyrostl jsem ve městě , kde oslává Vánoc neni tak rozšiřena .
Ale , možná je to také odpověd .
Kromě toho nelogického kontrasta v hodnocení Vánoc ve více-mené christianském Bělorusku v porovnání s více-mené ateistickém Českom , máme ještě jednu zvláštnost .
Slavime ( přesnejí neslavime , ale nepracujeme ) Vánoce dvakrát - podle pravoslavní a katolické tradici ( v noc s 24 . prosince a 6 . ledna ) .
A zrovna uprostrěd te doby máme svůj hlavní celoroční svátek - Nový rok .
Pro zajímavost mužu dodat , že i " Nový rok " slavime dvakrat 1 . a 14 . ledna , ten pozdějši svátek jmenuje se " Starý Nový rok " ( neco podobného existije i ve Srbsku , Černé Hoře , Makedonsku , a Švajcarsku - něm .
Alter Silvester ) .
Možná , že časová blízkost tech dvouch ( čili čtyřech ) svátků , ovlivnila michání a přehod jejich tradic , a zároven i pokles nenovonovoročných oslav .
Vím , že na věnkově ještě občas oslavuji tzv. Kaljady .
Vědci řikaji , že tato tradice má předchristianský - pohanský původ .
Glavní obsah Kaljad - přivítání slůnca , a začatek nárůstů delki dnu .
Mladež muže výpravit se malymi skupinami po obce , spivaji , přeje lídem nejaké dobré věci , a tym padem získáváji si penize , pečivo , alkohol atd .
Vánoční stromek , dárky , Děd Moroz , resp. Dzed Maroz spojeny více s Novým rokem , než s Vánocemi .
