Káždá hymna prohlásí buď krásu buď moc země .
Podle mého názoru , česká hymna správně mluví o nejvyznámnějsí rys té země : možnost dobře a klidně žít .
Pomocí neuvěřitelně bohatého kulturního dědictví a rozumu se může člověk v Praze vyuzít různé výhody .
Dva z mých nejoblíbenějších přikladů jsou že kultura je přistupná a že doprava je učinná .
Určitě jsou města na světě jejíchž kulturní moznosti jsou stejné a dokonce i větší .
Avšak na rozdíl od Prahy přístup k akcích v těch mistech je často omezen dražšími cenami .
Učinná hromadná doprava taky ovlivňuje život lidí jelikož díky tomu jsou míň aut na ulicích .
Podle posledných udajů , lidé , kteři bydlí v městech bez aut jsou štaštnejší a zdrávější .
I když to může vypádat banální , podle mé skušenosti v jiných zemích tvrdím na to že je to důležité .
Zkrátka , navzdory určitým nevýhodám existující v Čechách , rád bych dělal od Prahy drúhý domov .
