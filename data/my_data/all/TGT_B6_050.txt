MŮJ DENNÍ PROGRAM
Mam ráda když můžu dlouho spát , neráda vstávam brzo rano .
Celý týden vstávam v devět nebo deset hodin .
Rano snídam , myju se , obléču se a jdu do školy .
Přednašky začinam v 12 hodin nebo ve dvě hodiny .
Obědvam když mám hlad - ve tři nebo čtyři hodiny .
Odpoledne pracuju - učim se nebo čtu zajímavé knihy .
Večer , v sedm hodin tančim salse nebo navštivuju moje přitele .
Večer taky vařím nebo divám se na film .
Chodím spát pozdě - o půlnoci , proto jsem často unavená a ospalá .
