NEJLEPŠÍ DEN MÉHO ŽIVOTA
Před dvěma lety jsem snila o psovi .
A dávno jsem chtěla ovčáka .
Moje rodina to věděla a rozhodli udělat překvapení pro mě .
Sestra s přítelem vybrali z mnoha chovných štěňat to nejroztomilejší .
Stalo se to na jaře , v dubnu , když štěněti už bylo dva měsíců a on už mohl žit bez mamínky .
Přinesli ho domu večer a odnesli do mého pokoje .
Když jsem se vratila domu ze školy , šla jsem do pokoje a objevila tam ten nejlepší darek , o kterem jsem toužila už delší dobu a byla jsem z toho v sedmém nebi z radosbi .
On vypadal jako plyšový medvídek s tmavohnědymi očíma .
Byl moc malý , ještě nemohl stáb na nohou , pořad padal a kousal všechno co potkal .
Cela rodina ihned začala diskutovat jake jméno vic sluší našému mazličkovi .
Každému se libilo něco iného a po dlouhem přemyšleni nakonec zvytězilo jmeno Nike .
Asi měsíc Nike bydlel s námi v domě .
Během teto doby on stihl zničit spoustou věcí : nábytek , dveře , tapety a určitě hromadu bot .
V květnu počasí už se zlepšily a zacaly jsme pomalu ho nechavat venku aby časem mohl žit na dvoře .
Po několika dnů Nike pokousal všechno co bylo na dvoře a vytrhal květiny na zahradě .
Během roku Nike pořad něco boural a bylo to moc neopatrně z naše strany zapomět něco venku , probože za okamžik tam uz by nic nebylo .
Teď , na štěstí , Nike už je dospělý a chytrý pes , ale pořad mám z nej velkou radost , je to můj kamarád , osobní ochrance a taky plnohodnotný člen naší rodiny .
Vždycky s usmivem vspominám na to jak ten malý vyvaděl a dodaval spoustu starosti , ale zažili jsme taky spoly mnoha veselych a legračních zažitku , které vím určitě na nás čekaji a i v budoucnu .
