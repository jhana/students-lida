ŽIVOT NA KOLEJI VS. ŽIVOT DOMA
Když člověk složí maturitu a vybere si univerzitu , na které bude studovat , položí si otazku , jestli je lepší bydlet na koleji nebo doma .
Obě mé výhody , ale i nevýhody .
První výhoda bydlení na koleji je , že se tam pozná hodně dalších studentů a studentek , kdežto doma se bydlí jenom s rodinou , které už člověk zná , takže život doma může být nudnější než na koleji .
Na druhé straně život doma je možná jednodušší , protože se nemusí starat o níc :
Rodiče nakupujou , vaří , čistí ( alespoň obvykle ) , ale to taky známená , že je člověk více závislý .
Další hledisko jsou peníze :
Když se žije na koleji , musí se platit kolejné - ač se má eventuelně meně prostor než u rodičů , například když se má dvou - nebo třilůžkovy pokoj .
Našeh pokoj na koleji může být bliže univerzity než domov rodičů , takže není nutné ježdit každy den autobusem , autem nebo vlakem na univerzitu .
Osobně si myslím , že existuje hodně důvodů pro a proti zivotu na koleji , záleží na hodně faktorech , jestli život na koleji nebo doma je lepší .
