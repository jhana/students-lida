Ahoj , Evo !
Děkuju za tvůj dopis .
Jsem ráda že všechno je v pořádku .
Už dlouho jsme se neviděly , a proto chtěla bych tě pozvat na muzikál " Tajemství " .
Hrají to v divadle Kalich každý čtvrtek , pátek a sobotu od 20:00 .
Zavolej mi nebo napiš mi kdy by se ti to hodilo , a já koupím vstupenky .
Taký , mohly bychom nejdřív jít na kávu .
Na Jungmanově , vedle divadla , je výborná kavárna !
Mohly bychom se tam sejít v 19:00 a popovídat si ...
Už se na tebe strašně moc těším !
Měj krásný den ,
Eva
