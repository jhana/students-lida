Město , ve kterém chtěla bych žít .
Jedna z nejdůležtějších podmínek pro místo , ve kterém chtěla bych žít je to , aby má rodina byla vedle mě .
Zase to je těžko bydlet spolu v jedném domě .
A proto chci mít svůj dům , kde bych žila jenom s manželem a dětmi .
Toužím , že když budu mít dobré vzdělání a zajímavou praci , budu cestovat po světě a pak poznám , která země mi vyhovuje .
Ideálně chtěla bych žít v tichém a vyspělém městě někde v Evropě .
Třeba ve Francie nebo Anglie .
Nemůžu také říct , že chci nechat Rusko .
Tam jsem bydlela od začátku svého života , to je můj domov , dokonce když se přistěhuji .
Nebyla jsem ještě nikdy v Americe , ale líbí se mi jejich životní styl .
Oni mají všechno , nezávisle od peněz .
Protože tam je jeden z nejvyšších urovní života lidí .
Zatímco v Rusku je velmi těžko dosáhnout úspěchu a byt přesvědčeným , že tvoje rodina bude mít všecho a nebude nic potřebovat .
Beztak peníze není hlavní .
J. G. Bajron , známý anglický básník , řekl :
" Náš domov je tam , kde nás mají rádi . "
Já s tímto úplně souhlásím .
