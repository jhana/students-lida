Moje ideální dovolená
Jsem velmi klidný člověk , proto pro mě nejlepší je klidná a tichá dovolená .
Můj dům , to je moje pevnost - někdo řekl právě o mně .
Nejraději o dovolené dedím doma .
Většinou nechci nikam jet neboo cestovat .
Naopak chci mít absolutní klid a pohodlnost .
Ale přitom nemám zvyku lenošit .
Mám hodně zálib a neustále je pěstuju .
Můžu číst různorodé knihy , můžu se dívát na umělecké nebo dokumentární filmy .
Můžu něco dělat na počítači , samozřejmě víme , že na něm můžeme udělat mnoho různých zajímavých věcí .
Přitom nemusíme jenom hrát počítačové hry nebo poslouchat nějakou hudbu .
Ale nejraději o dovolené dělám ještě jednu věc .
Myslím , že tato věc se líbí všem lidem .
A konkrétně se mi velmi líbí spát .
Kdy chci spát , odkládám všechny své záliby a hledám postel
Adam
