Milý Adam .
Děkuju ti moc za tvé pozvání .
To je velké překvapení pro mě , protože jsem tě neviděl už skoro rok .
Přeju ti všého nejlepšího .
Buď šťastný a spokojený se svým životem .
Pamatuj taky , že my všichni žijeme jednou , proto udělej všechno , co někdy jsi přál .
Promiň , ale musím se omluvit , že nemůžu přijít , protože jsem nemocná .
Mám anginu a tak musím zůstát doma , ležet v postele a pít teplý čaj .
Ale já bych chtěla tě uvidět , protože mám pro tebe nějaký malý dárek .
Napíšu ti , když budu zdravá , a pak se domluvíme na kterém místě a v jakou dobu se sejdeme .
