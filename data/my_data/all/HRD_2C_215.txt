Skvělé prázdniny .
Hodně často vzpomínám na své skvělé prázdniny z doby studia na střední škole .
Měli jsme výbornou partu dobrých kamarádi , s kterými hodně často podnikali různé dobrodružné cesty .
Každé prázdniny jsme jezdili na brigádu k Černému moři sbírat ovoce .
Cesta vlakem k moři byla dlouhá , jeli jsme skoro dva dny a dvě noci .
Ten čas nám utíkal rychle , protože jsme se nikdy cestou nenudili .
Byly to krásné prázdniny .
Pracovali jsme jenom čtyři hodiny , od pěti do devíti rána .
Potom už slunce silně palilo , a nedalo se v tom horku pracovat .
V deset hodin odpolecne byli jsme již na pláži a užívali jsme slunce a moře .
Bydleli jsme v malých domkech na úpatí hor , uprostřed panenské přírody , kde jsme vychutnávali její klid a krásu .
Za svítání umývali jsme se ledovou vodou z pramene .
Byla nám velká zima , ale nikomu to nevadilo .
Byli jsme šťastný .
Vždy každý páteční večer bylo u nás tradicí ochutnávat víno pod rozlehlým stromem a hrát v karty .
O víkendech šli jsme pokažde vysoko do hor s přenosováním .
Rozdělovali jsme tam oheň , grilovali maso , povídali , poslouchali písničky hrané na kytaru a zpívali .
Tak to jsou moje vzpomínky na skvělé prázdniny .
Jak bych chtěla strávit prázdniny dnes ?
Podobně .
Ráda bych měla všechny své kamarády pohromadě v zapadlé horské chatičce , kde by o nás bylo hezky postaráno .
Přes den bychom s práteli chodívali na pěší túry , jezdili na sjezdovce , kochali a fotili okolní přírodu .
Večer bychom se sešli u krbu při dobré večeři , hrali bychom společenské hry , poslouchali hudbu a probírali čerstvé zážitky prožitého dne .
Domů bychom odjížděli s pocitem krásně prožité dovolené .
Nic bych na tom neměnila , jedině ranní umývání u studánky možná bych vyměnila za teplou sprchu v koupelně .
