Život na koleji vs. život doma
Od té doby jsem přišla do Prahy , bydlím na koleji .
Kolej Na Větrníku má čtyři budovy , ale neni příliš velký .
Ale pokoje jsou malé .
Obsahují 2 postele , 2 skříně , 2 psací stoly a lednici .
Studenti na jednom patru sdílí sprchy , záchody a kuchyni .
Výhada je , že moji spolubidlici je Češka , tak můžeme mluvit česky .
Ve Vídni bydlím ve vlastním bytě , tam mám 38 m2 sáma .
Bydlet na koleji je zajímavá zkušenost , ale přesto přestěhuju na konci dubna do bytu blízko Florence .
Tam mám velký vlastní pokoj a hezkou , moderní kuchyni .
Koupelna v bytě je asi tak velká , jako celý pokoj na koleji .
Ale byt je drahý , tak myslím , to bylo by jediný důvod , zůstat na koleji , ale protože dostanu stipendium , můžu platit vyšší nájem .
Vystěhovala jsem se z mého rodného doma , když jsem byla 18 .
Od té doby bydlím sáma , a to se mi líbí .
Ale občas je to dobré , přijít doma a maminka vaří oběd a tatínek seká trávu .
Ale jeden víkend stačí - potom se těším , že můžu žít ot .
