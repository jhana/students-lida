Moje dětství
Mužu jistotou řeknout , že moje dětství bylo šťastné .
Neměla jsem tatínka , ale maminka a babička udělali všechno , aby moje dětství bylo normalném .
Když jsem se narodila , matka byla nucena pracovat , protože v naší země byl smutný čas ‒ krize .
S narození do 5 let jsem byla vždicky s babičkou .
Matka měla těžkou praci a někdy jsem viděla ji jenom ráno .
Proto jsem někdy zvala babičku matkou .
Pak maminka našla dobrou praci a začaly jsme trávit čas spolu .
Nyní maminka je moje nejlepší kamarádka .
Jenom díky mamince a babičce jsem takové , jako jsem .
Nebudu rozpovídat , kde my jsme byli , kde jsme chodili na procházky , jaké hračky jsem měla , protože to vadilo pak , a nyní rozumím , že štěstí je , že my jsme byly spolu .
