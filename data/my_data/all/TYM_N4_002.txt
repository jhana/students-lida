Svatba a svatební tradici
hledaní nevěsti . ovy rodice hledaji nevěstu svenu synovi .
Když kondidatura nevěsty si zvolena ( vybrana ) , rodici oznamuji o tom synovi , souchlasi li on se seznamit s holkou o kterou oni mu ktera holčinu se chci vzit .
Tehdy on nadízí rodiců m svuj variant , ktery se taky projednáva v rodině .
Zustav na jednom variantě , zasnubovat .
Když obě rodiny souhlasí vdat svich dětí , pak se stanoví den oznamení zásnuby .
Zásnob te udalostí tří synonymuma " kabuldoran " - souhlasen vlastnit ( s persids tradičním uzbekskym jidlem plov ( s tadžitctiny ) a " oklík " ( - bělost , čistota - s uzbečtiny ) . Poslední nazév spojen s tím , že rodice manžela v den vyhlašení zasnoby přinášeji v dům nevěsty bílu látku , jako symbol čistoty a nevinnosti .
Zpevňování zasnoby , to je velké opatření , oproti predchozimu .
V Samarkandu to ma jmeno " non šindiriš " ( rozlomit se chlebu ( s uzbečtiny ) .
Hostiny pro pozvaných hostů delaji v domě nevěsty , . Při tom musi byt dodrzovany následující podrobnosti :
- příbuzní manžela musí prinest v dům nevěsti krasné vyzdovení podnosy ( tácy ) ( " lajli " ) v četním početu ( 4 nebo 6 ) .
Na jednom podnosu musí byt specialni pipecení chleb třech druhu - " gul-non " ( chleb z pšeničný mouky , ( ve středu kteryho raznobarevním vyloupaním prosem vyloužení k ) který ve středu ozdobeny raznobarevním vyloupaním prosem ) " fatií " ( chleb z listkového těsta ) a " kulča " ( maly chleb , upekajeny jenom v Samarkandu , obvykle delaji z kukuřicí mouky .
Chleb " lul-non " slavnostné rozlamuji . Jednu čast nechava v domě nevěsty , druha manžel bere v svuj dům . žehnající snoubencu .
Chleb " fatit " - znamena ze novomanzely budou bohaty .
Chleb " kulča " - obvykle rozdava hostům .
