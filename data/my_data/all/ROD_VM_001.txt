Esej :
" Fotografie , která mě zaujala " z vytavy " Normalizace v obraze "
15.07.2011
Nejzajimavější fotografie ve vystavě " Normalizace v obraze " byla fotografie , na které stoji dva rušií vojaiy u kiosku .
První vojak stoji na stánku a druhý uvnitř .
Česká vlajka visí na stánku a druhý vojak složí ruskou vlajku .
Dva prázdné stoličky stoji před kiosku .
Dvě budovy a dva holé stromy tvoří pozadí , a nic jiného je na fotografii .
Fotografie má popisek " Odcházíme - přátelství zůstává . "
To je napsano nad kiosku .
Líbí se mi fotografie , protože je ironická .
Během této doby , ruští komunističtí vojaci odjeli z Československa a jeli zpatky do Ruska .
To nevadilo českým lidem , že dva vojaci byli tam sám s nikým jim pomoct .
Nikdo byl tam , aby řikali " Nashledanou . "
Cedule nezměnil fakt , že češi nebylí smutné , že ruští vojaci odjeli .
Ale je mi nejlepší ironie , že cedule je psan v ruštině .
Jestli rušti by chtěli , že by pobyli přátelšké s českami , je divné , že nenapsali ceduli v češtině .
