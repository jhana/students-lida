Ahoj , Adame !
Vím , že chceš přijet přiští týden do Prahy a chtěl bych se s Tebou setkat .
Vím také , že neznáš dobře Prahu a myslím , že budeš potřebovat mou pomoc , až si budeš chtít prohlednout Prahu .
Přiští týden mám volno , tak se můžu s Tebou v ponděli setkat .
Sejdeme se v ponděli v jedenáct hodin na Karlově náměstí .
Přejdeme přes Karlův most a půjdeme dál kolem Vltavy .
Až bude horko , tak dlouho chodit nebudeme , a půjdeme do nějaké restaurace a naobědváme se .
Potom půjdeme dál a uvidíme Pražský Hrad a kostel Sv. Víta .
Vratíme se spatky a půjdeme na Vaclavské náměstí .
Ukážu Ti tam známou sochu Sv. Vaclava .
Když budeme unavení , půjdeme do jedné hospody .
Znám ji dobře , připravují tam moc chutné jídlo a dávají výborné pivo .
Myslím , že se procházka Ti bude líbit .
S pozdravem tvůj kamarád Adam .
