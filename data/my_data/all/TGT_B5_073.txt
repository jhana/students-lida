Moje oblíbene ročne obdobi
Mezi všemi ročnými obdobi mám nejvíce ráda jaro , květy se tehdy začinají objevit a všude je jich vůně .
Na jaře ještě není moc horko , ale není taky už moc zima jako v zimě .
Tak mužu se léhko oblekat a moc se mi to líbi .
Na jaře často svíti slunce , tak chodím na procházku nebo plavu v jezeře .
Každy už tehdy mysli na dovolenou nebo prázdniny , a čekaně a planovaní je velmí hezké .
Už se nemůžu dočkat až přijde jaro , protože budu mochla trávit cely den venku .
