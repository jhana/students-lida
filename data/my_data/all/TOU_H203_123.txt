Praha , 14.12 . 09
Milý Karle !
Chtěla bych tě pozvat na vánoční víkend do Prahy .
Mám pocit , že to budou skvělé prázdniny !
Půjdeme spolu na Staroměstské náměstí , budeme se dívat na vánoční stromek .
Pak se projdeme po uličkam kolem centru Prahy .
Moje mamínka taky bude tady , ona pro nás udělá vánoční cukroviky .
Můj tatínek nás možná odvěze do nějakého přiměstě a budeme hrat ve sníhu .
Doufám , že budeš moct přijet k nám !
Všechno nejlepší ,
Tvoje Eva .
