Železniční doprava po Číně
Moc jsem necestovala vlakem po České republice .
Ale vím , že má hustou a rozsáhlou železniční síť .
Dá se říct , že celý systém včetně vlaků je zastaralý .
Je obnovován velmi pomalu .
Ráda bych představila čínskou železniční síť .
Železniční síť vede do všech provincií .
Je efektivní .
Je nejbezpečnější způsob dopravy u nás .
Neexistujou zpateční jízdenky v Číně .
Vždy je jednosměrné .
Dají se koupit pět dny před odjezdem .
Jízdenky si můžeme koupit on-line .
Většinou v centru města má předprodej jízdenek .
Za předprodej se platí poplatek .
Kromě těch způsobů , můžeme koupit přímo u pokladny na nádraží .
Na jízdence je datum jízdy a cílova stanice .
Také nesmí chybět číslo vlaku , vagonu a číslo sedadla nebo lůžka .
Existují nerezervované jízdenky .
To znamená , že není číslo sedadla , ale může nastoupit do vlaku .
Dvakrát kontrolujou jízdenku než nastoupíme do vlaku .
První kontrola je u brány k nastupišti .
Všechny vagony mají čísla a při nastupu kontrolujou podruhé , abychom nastoupili do spravného vagonu .
U nás jezdí několik typů vlaků .
Např. typ Z je nejlepší a nejrychlejší , typ T je expresní vlak , K je rychlík , obyčejný vlak má jen číslo .
Vlaky typů Z , T a K jezdí 100 až 200 Km za hodinu .
Mají tabuli elektronickou na konci vagonu , na které ukazuje teplotu , čas příjezdu do další stanice a rychlost .
Některé expresní vlaky má dvoupatrové vagony .
Jsou čtyři třídy ve vlaku .
Nejlepší jsou měkké lůžkové vozy .
Na nádraží má vlastní čekarnu a ve vlaku je kupé lůžko pro čtyři osoby .
Matrace jsou měkké .
Ještě máme tvrdé lůžkové vozy .
Vagony jsou rozděleny na asi dvacet oddělení , v každém jsou třípatrové lůžka .
Spodní lůžko je nejdražší .
V obou typem lůžkových vozů vymění jízdenku za kovovou cedulku při nástupu .
Pak znovu vymění za jízdenku půl hodiny před koncem cesty .
A kvůli tomu nás vzbudí .
Sedadlové vozy se také rozdělí na měkké a tvrdé .
Měkké sedadlové vozy je víc místa pro nohy .
Jsou pohodlnější sedadla .
Tvrdé sedadlové vozy jsou o polovinu levnější než měkké .
Lavice jsou pro tři nebo pro dvě osoby .
Jídlo je vždycky ve vlaku k dispozici , buď ve krabici nebo v jídelním vozu , který většinou je mezi vagony měkkých lůžek a zbytkem vlaku .
Myslím si , že největší rozdíl mezi českou a čínskou železniční dopravou není v tom , co jsem představila .
Největší rozdíl je , že česká železniční doprava davá méně informace na cestě , např. nikdy neřekne , jak se jmenuje příští stanice , v kolik hodin dojíždí na přiští stanici .
Kromě toho , nejvíc mně překvapila , že některý vlak má jen jeden nebo dva vagony .
U nás minimálně deset vagonů , možná ještě víc , jinak není vlak .
