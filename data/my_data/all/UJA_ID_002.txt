Starý muž a žena chodí na ulici .
Rádi chodí do divadla a proto i dneska jdou tam .
Když sedí v divadle , někdo je volá .
A pak si všímají , kdo to je .
To je jedna holka , která se s nimi potkala na jediné ulici v cizí zemi .
Když ona byla tam , řekla jim , že utratila peníze , a proto měla problém .
A pak Starý muž a žena dali peníze té holce .
Ta holka nikdy nezapomněla na jejich tváře .
Dneska jim řekla " Děkuju vám pekně ! "
