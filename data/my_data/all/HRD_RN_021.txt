Můj nejhorší den v žívotě
Nepamatují moc na špatný den nebo ne´horší den v životě .
Ale bylo mi to nepříjemné , že když jsem poprvé přiletěla do Prahy , bylo to první cesta do ciziny z Japonska .
Byla jsem nervózní .
Domluvíla jsem se dříve s nějakým známým , který byl Čech , aby čekal na mě na letiští .
Tak jsem myslela , že bude to v pořádku .
Ale to nebylo , on nebyl na letišti v Praze .
Hledala jsem ho , ale nevěděla jsem , že ten pán jak vypadá , nikdy jsem ho neviděla .
Nedalo se nic dělat .
Čekala jsem na něho na letišti .
Byla jsem smutná a chtěla jsem rozplakat .
Naštětí jsem našla nějakého japonce , který byl hudebník díky tomu , že měl hudební nástroj .
Zeptala jsem se ho pro jistotu .
Že " jste z Japonska ? "
On byl moc hodný .
Vysvětlíla jsem mu , že mám jakou špatnou situaci .
A za chvílí příjel autem na letiště pro mě neslušný pán .
Poděkovala jsem japoncí srdečně .
Bylo to v pořádku kvůli jeho pomoci .
Byla jsem moc vdečená .
Samozřejmě se omlouval pán , který příjel pozdě .
