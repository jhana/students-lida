Vzdycky jsem chtěla žít a studovat v Evropě , proto bydlím teď v Praze .
Já jsem tady už skoro půl roku .
Všechno mi líbí v České Republice .
Chutná mi moc české jídlo .
V restaurace objednám nejčastěji smažený sýr .
On mi chutná , protože na Ukrajině ho ne máme .
Ale jsem trochu nespokojená s tým , že mluvím česky jenom s profesorkou ve škole .
Ale s českou gramatikou velkých problém ne mám .
Když jsem prijela do České republiky , největším překvapením pro mě bylo metro .
Nikdy jsem ne viděla takého čistého metra .
