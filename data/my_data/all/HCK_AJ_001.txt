Svět není takový jako se zdává být .
Vezměte například popeláře .
Obyčejný živáček si myslí , že každý popelář vyráží a sbírá smětí , protože je to jeho zaměstnání a takovým způsobem vydělává peníze .
Lidé nevědí , že na deset tisíc popelářů připadá jeden pokoutní vynálezce , který konstruuje ze zdánlivě nepotřebných věcí záhadné stroje , které by mohly úplně změnit náš život .
Takový tajuplný vynálezce , pojmenujme ho Adam , může ve svém volném čase konečně vyndat nalezené poklady z pytle .
Doma ho nikdo neuvidí , může se skrýt za zdmi jako za matčinou sukní .
Skončil práci , všechny dnešní odpadky z města leží už na skládce .
Skládka se v noci mění v potkanovo králoství .
Avšak se náš Adam už vrátil domů , velmi pečlivě si umyl ruce a oblekl si do čisté košile .
Váží si moc těch tichých večerů ve své domácí dílně .
Večerní červánky jsou pro něj znamení začátku půvabné práce .
Celý den hledá kably a motory , kov a plast , železo a sklo .
" Hledá " není vlastně vhodné slovo .
Všechno se mu nalezává samo .
Kromě samotného Libora se zbláznilo celé město .
Lidé onemocněli a trpí nějakou podivnou nemoc , která je nutí , aby kupovali a vyhazovali , kupovali a vyhazovali , kupovali a vyhazovali , kupovali a ... do nekonečna .
Pro Adama je to ale neuvěřitelné štěstí .
Pracuje jako popelář a celé bohaství města je pro něj zdarma , samo se přímo nabízí .
Ba !
Kromě toho , Adam dostává pravidelně mzdu od městského úřadu !
Avšak nemá tušení , co by měl s těmi penězi dělat .
Na podzim jsou studenější noci , topí tehdy bankovkami v kamnech .
Bankovky šelestí jemně .
