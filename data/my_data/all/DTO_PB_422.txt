Peníze - potřebujeme je k životu ?
Ve všech obdobích lidi potřebovali něco , co jím daválo radost .
Mohli to být buď materialní statky , nebo nematerialní .
Cím více se rozvíjel svět , tím více stavalo vecí , které lidi potřebovali .
V si už sami nemohli vyrábet , a tak potřebovali dat něco tomu , kdo to zboží výráběl .
Poprvé oni za to zboží davali výrobek , ale potom vymysleli peníze , co bylo evivalentem zboží .
Mělo to ruzné výhody a nevýhody .
Vyhod nest do obchodu pár sklenic vína , aby si za něj koupil chléb , nebo něco ještě , a mohl proste vzit peněženku a jít tam .
Nevyhodou bylo to , ze mohl je lehce ztratit , nebo mohl peníze někdo ukrast .
Teď už si neumíme představit , jak bychom bez peněz mohli existovat , protože potřebujeme je vsude , kde chceme něco koupit .
Představte si , že někdo při hází na poštu z kusem sádla , a chce za něj odeslat dopís .
Jak byypadalo ? !
Peníze pro nás teď už je jako vzduch , a nemůžeme bez něj existovat ani dnes , ani v budoucnosti .
