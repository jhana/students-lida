Když jsem se rozhodla jet do České republiky mí rodiče mi říkali , že čeština je podobná řuštině , ale když jsem přijela do Prahy , ničemu jsem nerozuměla .
V obchodě jsem rozuměla jen slovům " Dobrý den " a " Kolik to stojí ? "
Myslela jsem si , že čeština je moc hezký jazyk a že zvládnu se ho naučit .
Čeština a ruština mají hodně společných věcí .
V ruštině taky máme pády , ale jsou tam i rozdíly .
Například Slovo ´ ´ tramvaj " je v ruštině rodu mužského , a proto jsem měla stím slovem problem .
Při studiu češtiny v porovnání s ruštinou překvapilo mě to , že některá slova jsou stejná , ale mají jiný nebo opačný význam .
Občas když nevím nějaké slovo , snažím se použit ruštinu nebo ukrajinštinu , ale Češi mi nerozumí .
Teď , když umím česky lépe , snažím se ho používat všude .
Se svou kamarádkou , která je z Ruska , bychom chtěli se seznamit s nějakými českými studenty , protože nemáme praxi v češtině .
Tady v Praze máme jenom ruské kamarády , a to je problem , protože nepouživáme češtinu .
Doufám , že moje češtia se bude zlepšovat .
