Roh abundance
" Roh abundance " je teď docela znamý a použivaný vyraz v Rusku .
Lidé říká ho dost často , především staří lidé , ale málo kdo z nich nikdy se zajímal o to , odkud pochází tá fraze .
Existuje mytus .
Řecký mytus říka , že byl surový Bůh , který se jmenoval Kren a nechtěl mít žadné děti , protože se bal , že vezmou mu majetek a moc .
Proto ho žena narodila Zeusa tejemně , aby nikdo nevěděl , a přikazala nymfám , aby se staraly o něm .
Zeus byl vychován mlekem božské kozy , která se jmenovala Amalfeje .
Jednou ta koza se uchytla za nějaké dřevo a zlomila si roh .
Nymfa naplnila ho plodami a dala Zeusovi .
Ale ten rozhodl , že bude darovat je nymfám , které ho vychovovaly a slibil , že v budoucí budou mít všechno , co potřebujou .
Teď ten výraz je symbolem bohatství , dostatku .
Například , jestli někdo má hodně štěstí a všechno je dobře v jeho běžném životě , v nějaké době , lidé mu můžou říct , že to je jako z rohu abundance .
A teď už víme proč .
