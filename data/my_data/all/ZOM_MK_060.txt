Ahoj , Katko !
Pamatuješ , že máme za týden Velikonoce ?
Myslím si , že určitě musím s tebou někam jet .
Ať zařídíme cestu do českých lázní .
Navrhuju jet především do Karlových Varů .
Dnes je to velká atraktivita pro většinu turistů .
Nabízejí tam navštěvníkům léčebné procedury , taky bohatý kulturní program .
Například se konají tam Filmový festival a knižní veletrh .
A taky samozřejmě zkusíme známé lázeňské oplatky .
Potom bychom mohly s tebou navštívit Poděbrady .
To je moc krásné město .
Lázně už tam byly založeny ještě v 17 . století .
Je tam hodně historických a kulturních památek , hezké parky , místa pro odpočinek .
Musíme taky popít lázeňský minerální nápoj - Poděbradku .
Tak doufám , že budeš mít radost se mnou jet .
Čekám na tvůj dopis .
Na shledanou .
