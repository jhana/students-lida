Proč mám ráda ČR ?
Než jsem začala studovat češtinu , nevěděla jsem moc o České republice .
Ačkoliv je sousední země jsem byla tady jenom jednou .
Ale potom jsem strávila hodně času v České republice a musím říct , že mám ráda spoustu českých věc .
Především se mi líbí český jazyk , protože je něco úplně jiného než němcina .
Čeština je těžká , ale to nevadí .
Mimo to mám ráda ČR , protože má velmi zajímavou historii a proto také neuvěřitelně hezké budovy a zámky .
Ráda cestuju a čeká příroda je tak nádherná .
Když jdu na procházku na venkově , ráda se podívám na chaty , které jsou typické v České republice .
Proto jedu také ráda vlakem , tak vídím ještě víc .
Poznala jsem hodně Čechů a myslím , že lidé v ČR jsou skoro pořád milé a napomocné .
Jenom občas jsou nevlídné !
Nenavídím , že Češi mi odpovidají v angličtinu když zkusím mluvit česky .
To je hrozné .
Jsem šťastná , že jsem našla tak dobré české kamarády !
Samozřejmě jako jiné cizinci miluju české jídlo : moje nejoblibenější je guláš s knedlíky .
Šopský salát je také moc chutný a nikdy jsem nejedla tak hodně smažených sýrů !
Jenom svíčkovou nemám ráda , ale nevím proč .
A občas by na talíři byl víc zelenin , to musím kritizovat .
Pivo a kofolu piju ráda , protože v Rakousku kofolu nemáme a piva jsou tak jako tak nejlepší světa .
Je škoda , že voda z vodovodu nechutná tak dobrá .
V celku jsem moc spokojená , jsem v ČR .
