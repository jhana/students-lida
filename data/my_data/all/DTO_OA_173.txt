Maminka - zena , které vděčíme za to nejcennější co máme - za svůj život .
Maminka je to slovo , které v sobě ukrývá něhu , porozumění a lásku .
Maminka - první člověk , který potkává nás v tenhle velkém světě .
Pozorujeme na ní , obdivujeme a milujeme jí .
Milujeme její úsměv , pohled , mimiku a pohyby .
Mám ráda úsměv své maminky .
Když se usmíva , jako usmíva se jako se sv ítí a svití světlem všude kolem .
Ještě se mi líbí její hnědé oči , zaramované černými rasami a obočími .
Líbí se mi její profil , její rovný nos .
Mám ráda každou vrásku a každý pramínek jejích tmavých vlasů .
Vzdycky ráda pozoruju jak maminka postupuje , protože má rychlé , hladké a elegantní pochyby .
Ještě miluju svou maminku , protože má hezkou pováhu .
Je moc trpělivá , starostlivá , odpovědna , pozorná a objětavá .
