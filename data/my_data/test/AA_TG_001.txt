PROSTĚ SKVĚLÉ PRÁZDNINY
Vzpominám si ty prázdniny , které jsem měl , když jsem chodil do základní školy .
Tenkrát čas neběžel tak rychle jako teď .
Každý rok jsme měli den měsíce volno v létě , a pro mě ty dva měsíce byly jako dva roky .
Ani jsem nepoznal učitelku , když jsme začali v nové třídě po prázdninách .
Učitelka se samozřejmě moc , nebo vůbec , nezměnila - vypadla stalé ještě jako starý drak , ale dva měsíce jsou prostě dlouhá doba pro dětskou paměť .
Teď je to úplně naopak :
Dva roky se mi zdají jako dva měsíce .
Prázdniny dnes by měly trvat tak dlouho jako dřív , když jsem byl malý a chodil do školy .
Avšak mají prázdniny pro dospělé i svoje výhody , a nejdůležitější z nich je možnost jít sám do ciziny .
Podle mě jsou nejskvělější prázdniny cesta do jiných zemí .
Můžeš třeba jet na prázdniny se svymi kamarády .
Prázdniny s kluky - s " klukama " - to znamená mejdan .
Takové prázdniny určitě jsou zábavné .
Veliká nevýhoda je jen , že budeš po nich potřebovat ještě prázdniny , abys se uzdravil po všem blbostích se svymi kolégy .
Druhá možnost , když chces travit prázdniny v cizině , je cestovat se svou přítelkyní .
Občas to může být trochu nudné , ale hlavní výhoda s tím je , že dostaneš každý večer sex .
Třetí možnost je cestovat sám .
To může být nudné , a možna nebude tě nikdo vítat .
Ale máš také příležitost seznamit se s novymi lidmi .
Aspoň můžeš dělat , co chceš , právě kdy to chceš - a to jsou prostě skvělé prázdniny .
