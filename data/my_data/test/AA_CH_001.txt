Mám ráda Českou republíku
" Česká republika je malý stát v Evropě " , je to všechno , co jsem znala .
Před dvou lety jsem začínala studovat češtinu a Čechy .
Na Čechy se těším , zatím znám více o České republice , např. její krasné okolí , stará historie , denně tisícoví návštěvníci atd .
Loni na konci listopadu jsme přiletěly do hlavního města ČR - Praha .
Tento den v Praze poprvé sněžilo , a pak začal náš život v ČR .
Bydlíme na koleji Hvězda , která je daleko centra města , však ji mám ráda .
Míním , že všechno na koleji je lepší , ta kuchyně , ta sprcha i ten záchod . . .
S mojí spolubydlící jsem docela spokojená .
Je hezká dívka z Ruska , a mluví česky , anglicky španělsky a samozřejmě rusky .
Každy den dobře spám , dobře jím , a dobře studuji , myslím , že je to první důvod , proč mám ráda ČR .
Česká republika se mi líbí , a především Praha .
V mých očích Praha je jedním z nejkrásnějších a nejhistorických měst v Evropě , a je známá staroměstským náměstím , Karlovým mostem , Vltavou , žitovským hřibitovem atd .
Staroměstské náměstí je středobodem historického centra a nejstarším náměstím v Praze .
Kromě místa Jana Husa a radnice s vysokou věží a se světoznámým orlojem se na náměstí nachází řada zajímavých historických domů a památek .
Často se procházím mezi návštěvníky , a různé občerstvení občas ochutnám .
Kolem náměstí je hodně obchodů se suvenýry a krystaly , které lákají mě k návštěvě .
Přes řeku Vltavu je nejstarší stojící most Karlův most .
Baví mě na tom pohodlně procházet , přitom šum vody poslouchat , a sochy s historií výchutnat .
Čas plyne jako vody , ale nemusíme utíkat a minout hezkou scenerii .
Jednou jsem se zeptala kamaráda v Praze " Máš rád Praha ? "
Odpověděl " Ne , protože má tu hodně aut " .
V porovnání s Pekingem vlastně není tolik aut a dopravní zácep v Praze .
A přijíždějí včas tramvaje , autobusy a metro .
Češi , mládži nebo stařci , radi studují .
I když jezdí trámvají , mnohu lidí čte knihy nebo noviny .
Všude je uplně v akademické atmosféře .
Navíc jiná města v ČR taky mám ráda , jako Plzeň a Kutná hora .
Přestože nemají tolik turistů jako Praha , ale mají své speciality , např , Pivovarské muzeum v Plzní , a Kostnice kostel v Kutné hoře .
Mám ráda ČR , protože má své zvláštní historii , kulturu a scenerii , a je rozdílná s ostatními státy .
