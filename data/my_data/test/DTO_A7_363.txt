Myslím , že každý člověk musí mít nejaký koníček , něco , co jeho zajímá a co davá ho duše klid a spokojení .
A pro mě to je ještě víc zajímavé , když lidé mají rádí něco specifické , třeba moje kamaradka má zajem o esiptologie , já mám ráda mineralogie , astronomie a šperkařství .
Astronomie je dost nový pro mě zájem a velmi naročný .
Není to tak jednoduché pochopit to , co se dejé ve Vesmíru , chovaní hvězd , planet a všech těch síl , které existují kolem nás .
Ale to je neuvěřitelně mimořadně pozorovat v nocí oblohu , plnu hvězd - diamantů a vedět , že můžeš tomu porozumět .
Nejsložitějším pro mě je to , že ještě špatně umím fiziku a matematiku ze školy , ale to jsou nejdůležitejší obory pro pochopení astronomie .
Tak teď musím ( se naučít ) studovat zakladné znalostí zakladných věd .
V tomto oboru mám kumiry :
Steven Hawking a Karl Sagan .
Teď čtu Hawkingovu knihu v češtině a doufám , že někdy budu vysvětlovat to svym dětem .
