Já si myslím , že nejdůležitějším měřítkem životní úrovně lidí jsou peníze , které člověk má pro své základní , kulturní a také luxusní potřeby .
Samozřejmě , poprve stát musí být schopěn poskytovat člověku jídlo , oblečení , byt a lékařskou pomoc , to neznamená , že by to mělo být dáváno zdarma , ale že se stát snažit dát každému možnost pracovat a vydělávat za to ty peníze .
Když budou podmínky pro život splatněny , člověk bude mít schopnost vzdělávat se a zvyšovat svou kulturní úroveň , a to je druhým důležitým měřítkem .
Životní úroveň lidí v mé zemi není stejná pro všech .
Je docela hodně lidí střední úrovně života , ale těch , kdo musí těžce fyzicky pracovat a dostávat za to velmi málo je také dost .
Je tam také i skupina nepoměrně bohatých lidí .
Vnímám životní úroveň lidí v České Republice jako docela dobrou .
Je tady procento chudých lidí , a to je stejně , jako v jakékoliv jiné zemi , a myslím si , že není příliš velký .
Většinu představují lidí se střední úrovní , kteří dostávají tolik peněz , aby mohli žít a nemít s tím těžké problémy .
Možností žměny úrovně života pro Českou republiku je zvyšování bohatství státu , stavba nových zavodů , aby se zvětšil počet pracovních míst a místo Česka na mezinárodním trhu bylo výraznějším .
Můj stát má hodně surovin a ropy a měl by je použivat rozumnějí .
Těžba surovin je velká , ale prodávají to levnější než je možna .
A část peněz z toho by mělo být dáváno všem lidem ve státě , a nejen malé skupině lidí ve vlasti .
