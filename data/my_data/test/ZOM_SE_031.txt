Město ve kterém chci žít .
Na světě je mnoho krásných měst .
Líbí se mi Praha , NY , Otava , Nový Zéland , Sydney aj .
Každé město má své výhody a nedostatky , ale chtěl bych žít v Miami .
Podle mého názoru USA je jedna z nejsilnějších , nejbohatějších zemí .
Za prvé , v Miami je vysoká životní úroveň .
Lidé tam žijou velmi dobře .
Mám kamarádku , která tam bydlí .
Moc se jí tam líbí : dobří lidé , všechno není moc drahé atd .
Za druhé , tam mluví anglicky a také španělsky .
Umím trochu španělštinu a proto bych byl šťastný kdybych měl možnost praktikovat svou španělštinu .
Za třetí , pořád jsem chtěl bydlet v Americe .
Je to mým životním snem .
Pro mě Amerika je moderní život , nové technologie a perspektivy .
Za čtvrté , je tam ocean .
Rád se koupám a plavu .
Je tam taky dobré klíma pro život .
Líbí se mi když je na ulici teplo .
Doufám , že někdy budu mít šanci jet do Spojených Států Amerických .
