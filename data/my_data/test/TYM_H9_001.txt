Můj život a studium v ČR
Já jsem žila v České Republice skoro dva roky .
Studuju ekonomii v Západočeské Univerzitě v Plzni .
Bydlím na kolej s českými studenty .
Můj kolej je vedle hezkého parku , take je blízko hodně zástavek .
Líbí se mi můj kolej , protože každý den můžu prohlížet naturalní i moderní prospekt kolem něj .
Chodím do školy od rana do večeře , ale není celý den .
Nejdříve začínáme v 7:30 a končíme v 20:00 nejpozději .
Každý vyučování trvá jeden a půl hodin .
Tak podle rozvrhu , můžu mít jiné směny .
Jedu do školy tramvají nebo trolejbusem asi 20 minutů z koleje .
Moje fakulta je velká a starobylá budova u centra města .
Mí učiteli jsou výborný a hodný , někdy oni jsou zábavný .
Pomáhá mi trochu víc než češi protože jsme cizinka .
A české kolegy jsou mi přátelívý .
Vždycky myslím si , že mám ji vděčit .
Teď jsem si zvyknula s českým jídlem .
Ale ještě ráda jím vietnamské jídlo .
Chodím do menzy na obědu , a vařím na kolej na večeři .
Mám chuť na knedlíky , to je tradiční jídlo v ČR .
Znám jednu českou babu , a někdy její rodinu návštěvuju ve výkendu .
Učí mě dělat knedlíky i taky nějaký moučník .
Naopak , já vařím rýže a zavítky pro ně .
V asijských potravinách prodavájí hodně zboží z vietnamů , tak není mi těžký když dělám vietnamské jídlo .
Zboží cena není moc drahá v ČR .
Ráději počasí v letě .
Je moc hezké , dlouhý den a krátký noc .
Stromy vykvetá a plodí .
Cestuju větsinou velké město v letě jako Praha , Brno , Ostrava , Liberec .
V zimě padá sníh .
A velká oslava je v tomto období , to je Vánoce .
Cítím vesele na vánoci jako na Tet ve Vietnamu .
Ale nemám rada zimu , protože pořad mám rýmu .
Čeština je mi těžká .
Učím se česky ve škole , ale ještě ho mluvím spatně .
Přece , můj život a studium v ČR je v pořádku .
Žiju a studuju podle statního zákonu .
Dostanu ubytovací stipendium , mám přednost studenta třeba práce na brigadu , účet na bance , číslo na mobilu . . .
A určitě taky mám povinnosti , musim platit peníze na zdravotní a sociální pojíšťenku , musím prodloužit vízum každý rok . . .
Doufám , že zdokolím svůj česky a budu studovat hotovo svůj studijní program tady .
