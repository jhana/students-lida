Výhody a nevýhody studia v ČR
Loni 13 . září jsem přijela do ČR .
O tom jsem promluvila s rodiči , a tak jsem se rozhodla .
Našla jsem informaci o studiu v Česku na internetu a také mi o tom vypravila přitelkyně , která teď studuje už třetí rok .
Proč Česko ?
Ještě ve školě jsem slyšela že teď jsou dobré vysoké školy , a protože jsem vždy chtěla studovat v Evropě , vybrala jsem ji .
Studium v ČR mi otevírá všechny dveře .
Budu mít evropský diplóm a znát o jeden jazyk víc .
Také teď je krásná achitektura , už jsem navštívila kromě Prahy několik měst , a zůstala jsem nádšena .
Nelahozeves zámek , Pražský hrad , Brno , to všechno bych nikdy neuviděla , jestli bych v Česko nepřijela .
Co do problém se studiem teď , tak myslím , že ještě jsem se s nimi nesetkala .
Jestli studuješ a chapeš , vždy najdeš východ z jakekoliv těžké situace , to jsem pochopila už dávno .
Znáš jazyk , tak se domluvíš .
Mi kamarádi už teď přemyslí o tom , co budou studovat , a dál jak se skončí jejich studium a začne skuteční život .
Když sám musíš pracovat a všechno máš ve svých rukou .
Ale není to tak snadné , nikdy nevíš co se stane .
Myslím že co bude , musí se zůstat v budoucnosti , ale přesto můžu říct , že chci dobrou práci , přijemný plat , svůj dům a cestovat .
Ale co bude , to bude .
