V Praze , 10 . března 2011
Drahá Evo ,
Děkuju za tvůj dopis a promiň že jsem tě už dlouho nepsala .
Teď bydlím v Praze , v České republice .
Mám hezký byt a splněný sen .
Letos jsem zjistila že Karlova univerzita v Praze nabízí studijní program na jeden rok , kde studenti studují češtinu aby mohli udělat přijímací zkoušky na české univerzity .
Také na VŠE studuje má přítelkyně , která doporučila mně studium v ČR a protože mým čílem vždy byl studium v Evropě , rozhodla jsem se přijet do Prahy .
Nejdřív bylo těžko , protože jsem se zpozdila a musela jsem udělat moc práce aby udělala zkoušky uspěla na měsičných testech .
Pak už bylo lépe .
První semestr jsme udělali uroveň A2 a měli jsme víc češtiny .
On se skonskončil semestrální zkouškou , kterou jsem udělala na 1 , co v ČR znamená výborně .
Ve druhém semestru studujeme " češtinu pro Ekonomy " , abychom byli schopní udělat zkoušky na univerzity , na ekonomické obory .
Teď máme míň češtiny ale víc matematiky a stejne angličtiny .
Už jsem poslala a zaplatila přihlašky na VŠE , na 2 fakulty : podnikohospodářská a financí a účetnictví .
Je důležité abych uspěla ve přijímacích zkouškách , proto moc se učím .
Co do mých vysledků myslím že jsou docela dobré ale třeba ještě lépe .
Celkem můj život je vzorný ( příkladný ) , především je vyučování a pak zábava , Přesto moc se mi líbí procházky po městu které je velmý staré a má něco kouzelného .
Myslím že bylo by dobře , jestli bys přijela na své magisterské studium do ČR .
Přemyšlí o tom a napiš mně , také chci něco , vědět i o tvém životě v Rumunsku .
Čekám na tvůj dopis .
Všecho nejlepší a moc šťestí ,
Tvá Eva .
