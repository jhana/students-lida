Když mám volny čas , ráda jdu na proházku .
Protože , ja ráda jdu do parku a možna obědvám s kamarádky .
Taky ráda jdu do kavárny a piju kavu s mlekem .
Muj oblibeny kavárny je Luvur cáfe , ktery je blizko stanice Národni třida . ( nebo pěšky od staníce Můstek ) .
Mám ráda klasickou hudbu a když chci relaxovat , poslouhám klasickou hudbu nebo radio .
Ráda čtu romany nebo poezi v Anglicky .
V nedeli nebo když někdo má narozeníny , ráda pečím kolač nebo domaci šusenky .
Taky ráda varim , ale ještě neumím varit dobře .
Ale doufám , že budu umět varít hodně dobře brzo .
( Taky . ráda jím dobře jídlo ! ! )
Ráda jdu do bazén . protože ja potřebuju cvičít .
Tak že plavám v bazem jednou nebo dvakrat za týden .
V letě , chci jedu na hory , když mám čas .
Taky chci jedu do moře na plavat .
Teď jaro je tady , takže čekám na leto už ! !
