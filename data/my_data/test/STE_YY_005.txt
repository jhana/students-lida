Jednácteho listopadu , dva tisíce devět
Čtyři turisty chtěli vystoupit na horu .
Nejdřív oni odšli z hotelu , a přišli pěšky do zastávky .
Čekali deset hodin a potom zastoupili do autobusu .
Po půl hodiny přijeli na cílovou stanici .
Tam byly hory , na kterých oni často chodili .
Všichni nesli jeden batoh , v kterém byl jídlo , voda a kompas pro směr .
Oni utratili hodně času na horách až do večera , takže musili běžet z hor před časem , kdy nebyl moc tmávo .
