Prostě skvělé prázdniny
Jsem měla jedné prostě skvělé prázdniny , když jsem byla poprvé na prázdniny s přítelem .
On je můj první přítel , setkali jsme se před třemi roky a našťestí my jsme spolu od té doby .
Jmenuje se Adam .
Moje rodiče byli překvapené ( a trochu smutné , že jsem už dospělá ) , protože před tím jsem byla na prázdniny pouze s nimi .
Ale měla jsem ráda , že mužů zkoušet něco nového .
My jsme rozhodli , že pojedeme na kole na malou vesnici , jmenuje se Kerekdomb ( to jméno znamená kulatý kopec ) .
Je to asi 50 kilometrů od Ceglédu .
Je tam velmi dobrý kemping - je levný , čistý , a mohli jsme používat koupelnu zdarma .
Byl srpen a velké hořko .
Nebylo lehké vozit stan , batohy a jídla kolem a potili jsme se horkem .
Jeli jsme přes hodně vesnic , museli jsme se zastavovat všude , kde jsme našli studnu , ale trápili jsme se horkem .
Aspoň opalovali jsme se krásně .
My vyjeli ráno a dojeli k vesnici pozdní odpoledne .
Měli jsme svalovou horečku , ale museli jsme stavit stan , vypakovat batohy a vařit nějaké jídlo na večeři .
Potom procházeli jsme po městě .
Našli jsme hospůdku , pili jsme sklenici vína , a hrali jsme kulečník - přítel vyhral samozřejme , protože já řídko hraju kulečník a on je perfektně hraje .
Musím říct několik slov o sousedech , kteří bydlely v kempingu .
Byly hlavně němci .
Oní přesídili celou domácnosti : přivedly televize , zahradní nábytek , malé lámpy , které svítili na zahradě - Oní stavěly miniaturní plot a tak měly svou zahrádku .
Na stromy pověsily lampióny , které taky svítili .
Pamatuju , že v té době bylo světové fotbalové mistrovství a němci každý večer viděly zápas , pily hodně a zpívaly a nikdy nemohli jsme vyspát .
Druhý den byl velmi příjemný , nic jsme nedělali , relaxovali jsme , ležili jsme na pláži , koupali jsme se , opalovali jsme se , a jedli jsme chutné jídla .
Někdy já ráda nedělám nic , jenom relaxovat .
Odpoledne koupili jsme dámské noviny a četli jsem spolu .
Bavila jsem se velmi dobře , protože Adam takově legračně kritizoval články .
Třetí den jsme jeli na vylet kolem do sousední vesnice .
Tam je les , procházeli jsme po lesu a našli jsme rybník .
Tam ďelali jsme piknik a večer se vrátili do kempingu .
Byla jsem smutná , že příští den budeme muset jet doma .
Když jsem jeli na kole doma , taky bylo velmi horko , trápili jsem se moc . . .
Ale jsem se rozhodli , že každé léto pojedeme někam na kole .
Letos taky plánujeme takový vylet , možno že jedeme do Visegrádu ( je to stejné jako Vyšehrad v češtině ) .
