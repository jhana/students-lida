Můj koníček
Mým koníčkem jsou hračky , a přesně panenky .
Když jsem byla malá , neměla jsem jich mnoho .
Žila jsem v Polsku v době komunismu a změn po revolucii v 1980 , a proto jsme neměli dobré hračky .
Když mi bylo jenom několik let , měla jsem jen tři panenky .
Jedná byla hadrová panenka , která patřila mé mamínce .
Druhá byla levná plastická komunistická panenka , která rychle stratila vší vlasy .
Třeti jsem dostala od mého strýce , který pracoval v východním a potom západním Německu - byla to moje první vlastní Barbie !
Ve skutečnosti nebyla to pravdivá Barbie , ale její dobrá německá přítelkyně - Steffie .
Byly to moje tři oblibené hračky a brala jsem je se sebou všude .
Když jsem byla starší a hrozný čas komunismu prošly , samá jsem si začala kupovat panenky .
Celý čas jsem šetřila peníze , abych mohla koupit nějakou drahou panenku , o které jsem snila .
Teď už mám docela velkou sbírku , více než sedmdesát panenek .
Jsou tam jak nove tak staré , to znamená starožitné , panenky , jak hadrové tak plastické , tak keramické .
Některé jsou cenné , jiné jsou levné , ale vší se mi stejně líbí .
Snažím se také mít panenky ze všech časti světa .
Pomahají mí v tom moji přatele , kteří mi často přivozi panenky ze svých exotických cest .
Například mám panenku z Guatemaly , Brazílie a Činy .
Kromě toho jsem přečetla vší knihy , které jsem našla o panenkach v jazicích , kterým rozumím - polsky , anglicky , německy a teď také česky .
Nadto se snazím navštívit Muzea hraček a panenek v městach , do kterých cestuju .
Byla jsem už také dvakrát v Muzeum panenek a medvídku v Praze .
Můj můž nemá moc rád panenky , a tak často jdu samá do takového muzeum .
Ironicky mám syna , který se více zaujíma auty a zvířaty než panenkami .
