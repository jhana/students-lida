Je život bez rodiny těžký ?
Jsem dospělá , žiju bez svých rodičů už několik let .
Zvýkla jsem si na život bez nich .
Umím dělat všechno sama .
Když potřebuju nějakou radu , můžu jim zavolat .
Ale stýska se mi po rodině .
Volání není stejné jako je osobní rozhovor .
Také mezi Prahou a Novosibirskem je velký časový rozdíl .
Proto nemůžu volat domů každý raz , když bych chtěla .
Život bez rodiny není těžký , ale to by byla velká radost , kdyby moji rodiče bydleli ve stejném městě jako já .
Doufám , že jednou to zase bude tak .
