Vybrat si téma eseje mezi " Co mi dal a vzal český jazyk " nebo " Nachází se štěstí v penězích ? " , je pro mě velice těžké .
Proto je s Vaším dovolením spojím .
Co se týče prvního tématu , musím Vám , jako učitelce češtiny pro cizince , říci , že to , co děláte , je jednoduše něco neuvěřitelného .
Vykládat látku tak , jako to děláte vy , to je umění a já jsem opravdu nadšena .
Píšu to nyní , protože to , zda se člověku začne líbit cizí jazyk , závisí na různých faktorech .
Především na lidech , od kterých získáváme informace - od učitelů .
Velice lituji , že jsem se nemohla zúčastnit předchozích kurzů , protože by se moje úroveň velice zlepšila .
Děkuji Vám i celé katedře českého jazyka a literatury .
A pokud bych měla přímo odpovědět na otázku , co pro mě znamená čeština - odpovím " Hodně . "
Je to spojeno také s pochopením kultury , která k jazyku patří .
Dovolím si blíže vysvětlit tuto myšlenku - nést v sobě dvě kultury znamená rozšířit si obzor .
Nemyslím si , že v sobě již druhou kulturu nesu , jenom se mi velice líbí Česká republika a český jazyk , jak ho nazývá ruský spisovatel Bunin - " jazyk ptáků " .
Jazyk , který není podobný žádnému jinému jazyku na světě díky hlásce " ř " .
Když jsem se poprvé dozvěděla o tom , že si musím vybrat další slovanský jazyk ( polský , bulharský , srbochorvatský nebo nějaký balkánský jazyk ) , ohromně se mi zalíbila čeština !
A rozhodla jsem se dobře .
S prvními těžkostmi jsem se ale setkala velmi brzy , např. výslovnost " ř " . Do dnešního dne s výslovností bojuji .
Láska k této zemi u mě vznikla mnohem dříve - již v dětství .
Na Nový rok u nás vždy v televizi dávali Tři oříšky pro Popelku .
Nejspíše díky Libuši Šafránkové , kterou obdivuji do dnešní doby .
Láska k hradům a české hudbě u mě v srdci stále žije .
Co mi dal český jazyk ?
Odpovím - hodně ! .
A na otázku , co mi vzal ? - nevím , co odpovědět , nejspíš nic .
Copak jazyk může člověku něco vzít ?
Peníze a štěstí - jaké filosofické téma a jak dlouho by bylo možné o tom diskutovat !
Pokud bych napsala , že štěstí v penězích neleží , neodpovídalo by to přesně tomu , co si myslím .
Jsem realistka a peníze pro mě znamenají mnoho .
Pamatuji si , jak jsem na začátku svého studia na Kyjevské univerzitě chodila na různé brigády .
Ne proto , že bych musela nebo že by mě nutila špatná finanční situace , ale kvůli tomu , že jsem vždy chtěla být nezávislá a samostatná .
Dají peníze člověku možnost být šťastným ?
Myslím , že ano .
A je tu také možnost někoho šťastným udělat .
Jak je příjemné někomu dát dárek !
Darovat něco , o čem člověk již dlouho sní nebo jen nějakou drobnost .
Jak by tohle šlo bez peněz ?
Blíží se Vánoce a každý může své blízké potěšit .
Dárek lze samozřejmě udělat vlastníma rukama , ale výlohy nás tak lákají !
Štěstí není v penězích , s tím by každý filosof souhlasil .
Teď se nabízí zamyslet se nad tím , co je to štěstí .
Štěstí je v nás samých , je to harmonie všeho , co nás obklopuje , práce , domov , úspěch v práci , který je mimochodem svázán s financemi .
Důležité je dělat vše , co děláme , s láskou .
Pak pocítíme radost a vše bude harmonické a lehké .
Štěstí je v nás samých !
