1 .
Když jsem byla malá , chtěla jsem být spisovatelkou nebo prodaváčkou .
Myslíla jsem , že psát knihy a historky je velmi zajímavé a lehké .
Psála jsem přiběhy o drakovi a princi , o magii a dobrodružství .
Zabývala jsem se kouzelnimi pohádkami , a proto moc jich četla .
Moji kamarádi měli radi magické hístorký , a s nimi jsem se bavila o tajemstvích , o princeznách , o drakách , o cestovaní po báječných .
2 .
Ale když jsem začala chodit do školy , změnila jsem svůj názor .
Pochopíla jsem , že abych se dobře pracovala , musila bych se dobře učit ve školě .
Také jsem musila hodně studovat v umělecké škole . . . Líbilo se mi kreslení a krásné uměni .
Později jsem se rozhodla , že chci pracovát v oblasti umění a mody .
3 .
Nepamatuju , mi to někdo radil , ale myslím , že nikdo mi to neradil .
Vybrala jsem si práce sama .
Jednou jsem šla pro noviny do obchodu a uviděla jsem tam hézký časopis s krasnými modelkami .
Koupila jsem si ho a přečetla .
Líbilo se mi , že lidi tam pišou o fotografii , o umění , a krasě , o modě .
4 .
Teď už chci se stát autorkou v časopisu nebo asistentkou redaktora mody .
5 .
Přesně vím , že chtěla bych pracovat v modním časopisu , kde i , designerách .
Pro mně to nejen koníček , ale umění a věda .
6 .
Vím , že moje práce není lehká , ale naročná .
Ale doufám , že všecho dopadne výborně , protože hodně studuju , mluvíme se staršími , které pracují v časopisu a moc čtu o modě a krasě .
Všechi lidi chtějí žit krasně , a proto moda a umění vždy budou populární .
