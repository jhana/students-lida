Milá Evo !
Jak se máš ?
Dostala jsi můj první email , ktery jsem ti napsal z domu rodičů naší učitelky češtiny v Tokiu ?
Teď ti píšu tento email z sálu " Internet " v univerzitní knihovně :
Konečně jsem přijel do Olomouce .
Vse bylo v pořadku .
Ale , po pravdě řečeno , ve dne příjezdu jsem byl moc unavený .
Ve první noci jsem ubytoval u jejích rodičů .
Oni mě srdečně přivítali .
Její otec jel se mnou nakoupit do Supermerketu a matka celý den vařila pro mne .
Druhý den jsme přisli na koleje .
Okolí kolejí je klidně jako v parku .
Mi dali pokoj v sedmem patře .
Potom jsme přisli do kanceláře k registraci .
Po registraci jsme se rozloučil s nimi .
V 17 hodin začala procházka po Olomouci .
Byli jsme na Horní námestí .
Mě udivilo hodně kašen .
Ale ze všech památniků se mi líbí Velká želva .
Ti napíšu zase brzo .
Mej se hezky !
Adam
