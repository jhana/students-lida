Proč mám rád Českou republiku
Česka republika se mi líbí z mnoha různých důvodů .
Je to krásná země , která leží v srdci Evropy .
Můžeš po ní dlouho cestovat bez toho , aby ses nudil .
Její krásná města a hezké vesnice ukazují dlouhou a zajímavou historii .
Pokud jde o architekturu , hudbu a literaturu , má ČR bohatou chodit na různé výstavy , do muzeí , na koncerty , do opery atd .
Citím se tady zvláště dobře , protože jsem z Bavorska a bavorská a česká kultura se podobají .
Např. krajina a hudba jsou moc podobné .
Stejně tak lidé .
V obou zemích jsou trochu rezervovaní , ale když je poznáš lépe , jsou často zajímaví a originalní .
Samozřejmě nesmíme zapomínat na kuchyni .
Dobré , tradiční jídlo - především maso a omáčky - a přirozeně užasné pivo .
Myslím si , že Česka republika je země , v které strávím ještě hodně hezkých chvil a do ketré se budu často vacet .
