Co bych dělala , kdybych byla milionářka .
Kdybych byla milionářka , nechodila bych do školy , nestudovala bych češtinu .
Měla bych velký dům a mnoho dětí .
Kdybych měla hodně peněz , otevřila bych několik moderních škol , nemocnic , mateřských škol , dětských domů a tak dál .
Zařídila bych závody pro děti a dávala bych jim dárky .
Pomohla bych chudým a bezdomovcům , nemocným .
Koupila bych všechny zámky a starala bych se o nich .
Dělala bych rodícům a kamarádům pomyšlení .
Mohla bych si dovolit koupit něco drahé , jela bych na dovolenou , bydlela bych v krásném hotelu v pokoji s výhledem na moře .
Cestovala bych hodně , objela bych všechna města .
