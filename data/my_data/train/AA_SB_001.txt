Život na kolej / v bytě
Mnoho studentů , kteří studuji ve velkém městě , žijí / bydlí na koleji nebo mají vlastní byt , - záleží to na tom , kolik mají peněz .
Mít vlastní byt ve městě není přímo levně a proto ( myslím si ) bydlí hodně studentů na koleji .
Všechno má své výhody i nevýhody .
Když chceš mít vlastní byt , už začne problem ho najít .
Chceme samozřejmě , aby byl byt hezký , levný a blízko centra , ale toto není lecke/lehce .
Jsme našli " naše vysněné bydlení " , jsme šťastní .
Byt , když je velký , můžeme si dělit s kamarádmi - každy má svůj pokoj i svou soukromnast .
Žiješ na koleji náš obvyklé spolubydlicího neboněkdy mášcelý pokoj pro sebe .
Podle mého názoru mám pocit , že je pokoj moc malý pro dvě osoby .
Na začátku , byla jsem překvapená , ale teď už jsem si zvykla na malý pokoj i na celý kolej a taky , že máme jednu kuchyně se sporákem a spolecenskou koupelnou .
Mrzí mě moc , že někteří nedavají pozor na hygienu , že nečistí sporák .
Když je špinaví a ho už nepotřebují .
Tak preferuju vlastní byt .
Kolej má ale taky své výhody - seznamiš se s novémi lidmi a poznaš nové kultury i jazyky .
Zaporné je , že jsou stěny/zdi moc tenke , že slyšiš všechno : jak soused telefonuje ( skupuje ) , kdy přijde navštěva apod .
Klíd potom nemáš .
Na určitou dobu ( z 3 mesíce ) můžeš bydlet na koleji , ale nechtěla bych to celý studium .
Jsem člověk , který je rád mezi lidmi , ale potřebuju svou soukromnast i svůj klíd .
