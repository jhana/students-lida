Ahoj Tomáši !
Zdravím Tě z Ruska .
Jsem moc šťastná , že přijedu do České republiky .
Tak dlouho jsem tě neviděla .
Chtěla bych navštívit Staré Město , které je centrem Prahy .
Četla jsem , že tam je hodně památek , muzeí , barokních a gotických kostelů , krásných náměstí .
Zvláště , bych chtěla navstívit Staroměstské náměstí , které je centrem Starého Města .
Na Týnský chrám , Kostel svatého Mikuláše , a další pozoruhodné stavby .
Vím , že uprostřed Staroměstského náměstí stojí pomník Jana Husa .
Chtěla bych se fotografovat vedle pomníku .
Také bych chtěla navštívit Pražský hrad a podívat se na Katedrálu svatého Víta , o které jsi mi už hodně povidal .
Kromě toho bych chtěla ochutnat smažený sýr a svíčkovou .
Myslím , že česká kuchyně bude mi chutnat .
Těším se na Tvou odpověď .
S pozdravem Tvoje Pavla .
