Ahoj Petře !
Moc jsem ráda , že chceš přijet do Prahy , už mám pro nás zajímavý program .
Sejdeme se v 10 hodin ráno na staroměstském náměstí vedle Orloje .
Pak půjdeme pěšky přes Karlův most do Malostranského náměstí a uvidíme tam několik pěkných kostelů .
Tam můžeme vyjít do kostela svatého Vita .
Ten kostel je opravdu kouzelný a moc velký , vedle něho se citíš jako malé ditě .
To je znamé místo v Praze a uvidíme tam hodně turistů .
Pak sejdeme zpátky dolů a pojedeme lanovkou ne Petřin .
Tam je hodně parků a taky pěkný pohled na Prahu .
Hlávní město je jako na dláni .
Po Petřinu můžeme jet do dobré restaurace a dat si něco k obědu .
Myslím , že česneková polévka a hovězí gulaš budou ti moc chutnat .
Pak půjdeme na Vácslavské náměstí a podíváme se na Národní Muzeum .
Budova muzea je stará a hezká .
Pak se vrátíme na Staroměstské náměstí , je to moje nejoblibenější místo v Praze !
Ráda tam chodím a dívám se na staré hodiny .
Zdá se mi , že se tam čas zustavil .
Už se těším na tvůj přijezd !
Měj se hezky .
Eva
