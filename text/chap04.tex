\chapter{Our work}
\label{chap:4}

\section{Data}
\label{sec:data}

\subsection{Czech Language}
The Czech language is a member of the West Slavic family of
languages.\footnote{Together with, for example, Slovak and Polish.} It is spoken
by over 10 million people, mainly in the Czech Republic.

Regarding morphology, Czech is highly inflected (words are modified to express
different grammatical categories) and fusional (one morpheme form can
simultaneously express several different meanings).

\subsection{Corpus}
\label{sec:data.corpus}
We use a subset of the publicly available Czech as a Second Language with
Spelling, Grammar and Tags (CzeSL-SGT, \cite{czesl-cgt}) corpus, which was
developed as an extension of a part of the CzeSL-plain (``plain" meaning not
annotated) corpus, adding texts collected in 2013. CzeSL-plain consists of three
parts, \verb+ciz+ (a subset of CzeSL-SGT), \verb+kval+ (academic texts obtained
from non-native speakers of Czech studying at Czech universities in masters or
doctoral programmes) and \verb+rom+ (transcripts of texts written at school by
pupils and students with Romani background in communities endangered by social
exclusion). 
\begin{figure}[hbt]
  \includegraphics[width=\textwidth]{img/corp-stats/L1_counts}
  \caption{Distribution of 20 most frequent languages in CzeSL-SGT}
  \label{fig:L1_counts}
\end{figure}

The corpus contains transcripts of essays of non-native speakers of Czech written
in 2009-2013. It originally contains 8,617 texts by 1,965 authors with 54
different first languages, Figure \ref{fig:L1_counts} shows their distribution.

The data cover several language levels of the Common
European Framework of Reference for Languages (CEFR),\footnote{\url{http://www.cambridgeenglish.org/cefr/}} ranging from beginner (A1)
to upper intermediate (B2) level and some higher proficiency.
There are 862 different topics in CzeSL-SGT, such as \textit{Moje rodina},
\textit{Dopis kamarádce/kamarádovi},
\textit{Nejhorší den mého života} or
\textit{Co se stane, až dojde ropa?}.\footnote{\textit{My family};
  \textit{A letter to a friend};
  \textit{The worst day of my life};
  \textit{What will happen when we run out of oil? }}
749 essays do not include a topic specification.

Metadata information consists of 30 attributes, most of which are assigned to
each text. 15 of these attributes contain information about the texts and 15
contain information about the authors. A list of these attributes can be found
in Appendix \ref{app:meta}. 

\subsubsection{Annotation}
Each token is labelled by its original and corrected (\texttt{word1}) word
form, lemma and morphological tags of both forms and the automatically
identified error type determined by comparing the original and corrected
forms. For example, one of the sentences from the corpus
(\textit{Je tam hrób Franze Kafki.} -- \textit{Franz Kafka's grave is there.}) is
annotated as follows:
\footnotesize
\begin{verbatim}
<s id="84">
    <word lemma="být" tag="VB-S---3P-AA---" word1="Je"
          lemma1="být" tag1="VB-S---3P-AA---"
          gs="" err="">Je</word>
    <word lemma="tam" tag="Db-------------" word1="tam"
          lemma1="tam" tag1="Db-------------"
          gs="" err="">tam</word>
    <word lemma="hrób" tag="X@-------------" word1="hrob"
          lemma1="hrob" tag1="NNIS1-----A----"
          gs="S" err="Quant1">hrób</word>
    <word lemma="Franze" tag="NNMS1-----A----" word1="Franze"
          lemma1="Franz" tag1="NNMS2-----A----"
          gs="" err="">Franze</word>
    <word lemma="Kafki" tag="X@-------------" word1="Kafky"
          lemma1="Kafka" tag1="NNMS2-----A----"
          gs="S" err="Y0">Kafki</word>
    <word lemma="." tag="Z:-------------" word1="."
          lemma1="." tag1="Z:-------------"
          gs="" err="">.</word>
</s>
\end{verbatim}
\normalsize

Morphological tags of the Prague positional tagset (\cite{hajic2004}) are
represented as a string of 15 symbols, each position corresponding to one
morphological category. Non-applicable values are denoted by a single hyphen
(\texttt{-}). The 15 categories are described in Appendix \ref{app:POS}.

\subsection{Our data}
We excluded texts with unknown speaker IDs, unknown language group of first
language and texts where Czech was given as first language (possibly an error in
annotation). We also randomly selected 10\% of all texts written by authors with
Russian as their first language, to acquire a better partitioning of languages
and language groups throughout the data.

Due to these operations, the number of texts we use is narrowed down to 3{,}715,
with the distribution of authors' first language groups as depicted in Table
\ref{tab:s_L1.counts}. The five most frequent languages are Chinese, Russian, Ukrainian, Korean and English.

\begin{table}[htb]
  \begin{center}
    \resizebox{0.7\textwidth}{!}{
    \begin{tabular}{|l r r|}
      \hline
      Language group & Absolute count & Relative count \\
      \hline
      non-Indo-European & 1{,}747 & 47\% \\ 
      Slavic & 1{,}070 & 29\% \\
      Indo-European & 898 & 24\% \\
      \textbf{All} & \textbf{3{,}715} & \textbf{100\%}\\
      \hline
    \end{tabular}
    }
  \end{center}
  \caption{Text counts by language groups}
  \label{tab:s_L1.counts}
\end{table}

\begin{table}[htb]
  \begin{center}
    \resizebox{0.7\textwidth}{!}{
    \begin{tabular}{|l r r|}
      \hline
      & Absolute count & Relative count \\
      \hline
      \textsc{TRAIN}    & 1{,}489 & 40\% \\
      \textsc{DEV-TEST} & 1{,}115 & 30\% \\
      \textsc{TEST}     & 1{,}111 & 30\% \\
      \textbf{All} & \textbf{3{,}715} & \textbf{100\%}\\
      \hline
    \end{tabular}
    }
  \end{center}
  \caption{Text counts by dataset split}
  \label{tab:datasplit}
\end{table}

A great field of work in \acl{NLI} (for English) has been carried out on the
\acs{TOEFL11} corpus (\cite{TOEFL11}). It differs from CzeSL-SGT and the subset
we use in several ways.
First, \acs{TOEFL11} consists of 12{,}100 essays by native speakers of 11
languages (French, Italian, Spanish, German, Hindi, Japanese, Korean, Turkish,
Chinese, Arabic and Telugu). The essays are equally distributed between the
languages. In comparison, our subset of CzeSL-SGT contains 3{,}715 Czech essays
by native speakers of 52 languages, but the five most frequent languages form
about 50\% of the dataset.
Second, on average, 322 word tokens (at a range from two to 876) are contained
in \acs{TOEFL11} essays. This is almost three times more than texts in our
CzeSL-SGT subset which contain 110 word tokens on average, at a range from 5 to
1{,}536.
Third, CzeSL-SGT is annotated for errors, which allows another whole area
of NLI research.
Finally, only one essay per student is present in \acs{TOEFL11}. In our data,
each student contributes by 2.9 essay on average and the number of essays per
student range from one to 22. 

\section{Tools}
All of the scripts which extract features from text, filter features and prepare
data for classification are written in the Python programming language, version
2.7.\footnote{\url{https://www.python.org/}} Some of the figures are generated
using the R language.\footnote{\url{https://www.r-project.org/}}
We use the SVM\textsuperscript{\textit{light}} implementation of
\aclp{SVM} for classification.\footnote{\url{http://svmlight.joachims.org/}}

\section{Features}
\label{sec:our.feats}

We employ six different feature types which are based on the distribution of 
certain patterns in text: four n-gram types, function words and error types. Two
other features are average sentence and word length. We test four different
values for n-gram, function word and error feature types:
\begin{itemize}
\item[--] \textit{Raw} frequencies are simply the number of occurences of a
  pattern in a document.
\item[--] \textit{Relative} frequencies are raw frequencies of a pattern divided
  by the length of the document.
\item[--] \textit{Log-normalized} frequencies are computed as in
  \cite{aharodnik2013}:
  \begin{align}
    {S_{ij} = round \bigg( 10 \times \frac{1+\log(tf_{ij})}{1+\log(l_j)}}\bigg)
%    \label{eq:logfreq}
  \end{align}
\item[--] \textit{Binary} values denote only the fact that the pattern is present.
\end{itemize}
To distinguish between these value types, we use the following abbreviations:
\texttt{raw}, \texttt{rel}, \texttt{log} and \texttt{bin}.

\subsection{\textit{n}-grams}
\textit{Character n-grams} are extracted from individual
words\footnote{Here, we consider sequences of alphanumeric characters as words.}
for $n = 1,2,3$. Before extracting the n-grams, a word is converted to lowercase
and \textit{padded} from both right and left with spaces, that is, a
sequence of $n-1$ spaces is appended to the beginning and end of the word. This
allows us to distinguish between a character sequence that typically occurs in
the middle of a word and the same sequence that occurs more frequently at the
end. Considering the word \textit{Je} from the previously introduced sentence and
$n=2$, we would extract bigrams (\_, \textit{j}), (\textit{j}, \textit{e}),
(\textit{e},\_).

\subsection{Function words}
For our experiments, we extracted 300 most frequent function words from the
Prague Dependency Treebank (\cite{pdt2.5}). We considered conjunctions,
prepositions, particles and pronouns. When extracting feature values, we
consider only function words occurring at least twice in data.

\subsection{Average length of word and sentence}
We compute the average sentence length $l_s$ of document $d$ as
$$l_{s}(d)= \frac{\sum_{i=1}^{n} |s_{i}|}{n}\quad,$$
and the average word length $l_{w}(d)$ as
$$l_{w}(d)=\frac{\sum_{i=1}^{n} |w_{i}|}{n}\quad,$$
where $n$ is the number of words and $m$ is the number of sentences in $d$,
considering all sequences of alphabetical characters and digits as words.

\subsection{Errors}
Thanks to the annotation of errors in CzeSL-SGT, we are also able to conduct an
analysis based on the distribution of various error types in the
essays. An overview of all error types together with
examples can be found in Appendix \ref{app:errors}.

\section{Experiments on development data}
At this point, we have chosen a set of feature types and a set of feature
values. Our task is now to explore the space of models that can be learned from
our data and choose such parameters and features, that the final model will have
a good ability of distinguishing texts written by Slavic and non-Slavic native
speakers.
In this section, we give an overview of these initial experiments on development
data. In the next section, we give and analyze results on test data.
Abbreviations used to specify different types of kernel functions and feature
types are described in Tables \ref{tab:kertypes} and \ref{tab:feattypes}.
\begin{table}[htb]
  \begin{center}
  \begin{tabular}{|l|l|}
    \hline
    \textbf{Abbreviation} & \textbf{Description} \\
    \hline
    \texttt{lin}   & linear kernel function \\
    \hline
    \texttt{poly-1}& polynomial kernel function of degree 1\\
    \hline
    \texttt{poly-2}& polynomial kernel function of degree 2\\
    \hline
    \texttt{poly-3}& polynomial kernel function of degree 3\\
    \hline
  \end{tabular}
  \end{center}
  \caption{Kernel functions}
  \label{tab:kertypes}
\end{table}
\begin{table}
  \begin{center}
  \begin{tabular}{|l|l|}
    \hline
    \textbf{Abbreviation} & \textbf{Description} \\
    \hline
    CG[1-3] & Character $n$-grams, $n=1,2,3$ \\
    WG[1-3] & Word $n$-grams, $n=1,2,3$ \\
    PG[1-3] & \ac{POS} $n$-grams, $n=1,2,3$ \\
    OG[1-3] & Open-class-\ac{POS} $n$-grams, $n=1,2,3$ \\
    FW & Function words \\
    ER & Error types \\
    WL & Word length \\
    SL & Sentence length \\      
    \hline
  \end{tabular}
  \end{center}
  \captionof{table}{Feature types}
  \label{tab:feattypes}
\end{table}

For basic evaluation of a model's performance, we use the \textit{F-score} as a
leading measure. The F-score is defined as the harmonic mean of precision and
recall:
$$F = 2 \cdot \frac{precision \cdot recall}{precision + recall}$$

We now proceed in three steps, or runs:

\subsection{Run 1}
The aims of the first run were to choose a value of the cost parameter $C$,
which influences the size of the hyperplane margin when classifying with
\acp{SVM},\footnote{See Section \ref{sec:nonsep} for figures and details on how
  the cost parameter affects classification.} and the types of kernel
functions, each of which gives a different similarity measure.\footnote{See
  Equation \ref{eq:kernelfunctions} for a definition of the linear,
  polynomial and radial-basis kernel function.}

\begin{table}[ht]
  \begin{center}
    \resizebox{\textwidth}{!}{
    \begin{tabular}{|l|l|}
      \hline
      Data           & \textsc{DEV-TEST} \\
      Feature types  & CG[1-3], WG[1-3], PG[1-3], OG[1-3], ER, FW, WL, SL\\
      Feature values & \texttt{bin}, \texttt{raw}, \texttt{rel}, \texttt{log}\\
      Kernels        & \texttt{lin}, \texttt{poly-1}, \texttt{poly-2},
      \texttt{poly-3}\\ 
      $C$ parameter  & 0.001; 0.01; 0.1; 1.0; 10.0; 100.0; 1{,}000.0;
      10{,}000.0\\
      Information gain threshold & 0 \\
      \hline
    \end{tabular}
    }
  \end{center}
  \caption{Settings for run 1 of experiments.}
  \label{tab:run1}
\end{table}

Table \ref{tab:run1} shows the particular settings of run 1. We only tested
individual feature types (mainly because of time-saving reasons). We
experimented with all previously described feature values and various values of the
cost parameter. As kernel functions, we used both linear and polynomial
functions, which have been popular in previous work. We did not use the radial
basis function, as it did not prove useful in our preliminary experiments.
\begin{table}[ht]
  \begin{center}
    \resizebox{\textwidth}{!}{
    \begin{tabular}{|l|r|r|r|}
      \hline
      \textbf{Feature type} &
      \specialcell{\textbf{All features} \\ \textbf{(count)}}
      & \specialcell{\textbf{Selected features} \\ \textbf{(count)}}
      & \specialcell{\textbf{Discarded features} \\ \textbf{(percentage)}}\\
      \hline
      CG[1-3] & 9{,}710 & 9{,}003 & 7\% \\
      WG[1-3] & 28{,}394 & 27{,}493 & 3\% \\
      PG[1-3] & 6{,}260 &  5{,}862 & 6\% \\
      OG[1-3] & 18{,}953 & 18{,}140 & 4\% \\
      ER & 42 & 39 & 7\% \\
      FW & 12 & 12 & 0\%\\
      \hline
      All &   63{,}371 & 60{,}549 & 4\%\\
      \hline
    \end{tabular}
    }
  \end{center}
  \caption{Feature selection -- run 1}
  \label{tab:ig1}
\end{table}

\begin{table}[htb]
\begin{center}
\resizebox{!}{.25\paperheight}{%
\begin{tabular}{lrrrrrr}
\textbf{Kernel} & \textbf{C} & \textbf{Acc. Mean} & \textbf{Acc. SD}  & \textbf{F-score Mean} & \textbf{F-score SD} & \textbf{Frequency} \\
\texttt{lin}      & 0.001   & 60.46  & 12.26 & 45.53  & 8.36  & 32   \\
\texttt{poly-1}   & 0.001   & 60.46  & 12.26 & 45.52  & 8.35  & 32   \\
\texttt{lin}      & 0.1     & 60.71  & 11.93 & 43.82  & 5.08  & 30   \\
\texttt{poly-1}   & 0.1     & 60.72  & 11.93 & 43.81  & 5.08  & 30   \\
\texttt{poly-1}   & 1.0     & 60.78  & 11.83 & 43.36  & 5.96  & 30   \\
\texttt{lin}      & 1.0     & 60.79  & 11.82 & 43.36  & 5.95  & 30   \\
\texttt{poly-1}   & 0.01    & 62.03  & 10.25 & 43.35  & 9.82  & 31   \\
\texttt{lin}      & 0.01    & 62.03  & 10.24 & 43.34  & 9.83  & 31   \\
\texttt{lin}      & 10.0    & 60.41  & 12.08 & 43.32  & 5.77  & 30   \\
\texttt{lin}      & 100.0   & 59.41  & 12.88 & 42.78  & 6.02  & 30   \\
\texttt{lin}      & 1{,}000.0  & 59.54  & 12.47 & 42.49  & 5.95  & 30   \\
\texttt{poly-1}   & 1{,}000.0  & 60.84  & 12.24 & 42.17  & 6.14  & 30   \\
\texttt{poly-2}   & 0.001   & 58.48  & 16.39 & 41.91  & 8.23  & 30   \\
\texttt{poly-1}   & 10{,}000.0 & 58.77  & 13.45 & 41.79  & 6.41  & 30   \\
\texttt{poly-1}   & 10.0    & 59.65  & 12.85 & 41.16  & 7.67  & 30   \\
\texttt{poly-2}   & 0.01    & 59.2   & 15.85 & 41.14  & 8.35  & 30   \\
\texttt{lin}      & 10{,}000.0 & 58.47  & 13.78 & 40.93  & 7.32  & 30   \\
\texttt{poly-1}   & 100.0   & 58.01  & 13.51 & 39.82  & 8.48  & 30   \\
\texttt{poly-2}   & 0.1     & 59.81  & 16.41 & 39.03  & 8.84  & 30   \\
\texttt{poly-2}    & 100.0         & 58.74  & 17.48 & 38.82  & 8.97  & 30   \\
\texttt{poly-2}    & 1{,}000.0        & 58.66  & 17.68 & 37.91  & 9.45  & 29   \\
\texttt{poly-2}    & 10{,}000.0       & 63.09  & 14.74 & 37.32  & 9.54  & 28   \\
\texttt{poly-2}      & 10.0          & 59.48  & 17.52 & 37.21  & 10.12 & 30   \\
\texttt{poly-2}      & 1.0           & 60.23  & 16.75 & 37.02  & 10.14 & 30   \\
\texttt{poly-3}    & 1{,}000.0        & 54.47  & 19.21 & 35.32  & 11.21 & 31   \\
\texttt{poly-3}      & 0.01          & 54.69  & 18.44 & 35.15  & 11.68 & 30   \\
\texttt{poly-3}      & 0.1           & 54.43  & 18.62 & 34.85  & 10.51 & 29   \\
\texttt{poly-3}    & 10{,}000.0       & 53.96  & 19.37 & 34.77  & 11.5  & 30   \\
\texttt{poly-3}      & 0.001         & 58.77  & 17.35 & 34.15  & 12.31 & 30   \\
\texttt{poly-3}      & 100.0         & 54.79  & 19.24 & 33.44  & 12.66 & 30   \\
\texttt{poly-3}      & 1.0           & 56.73  & 18.07 & 33.27  & 11.8  & 29   \\
\texttt{poly-3}      & 10.0          & 56.71  & 18.58 & 33.1   & 12.11 & 28   \\
\end{tabular}
}
\end{center}
\caption{Summary statistics of experiments with single-type features}
\label{tab:pars}
\end{table}

Table \ref{tab:pars} %in Appendix \ref{app:pars}
shows summary statistics of
the experiments with common parameters (type of kernel function and $C$
parameter). It is sorted by the F-score mean. We observed that the
\texttt{poly-3} kernel function performs significantly worse in both F-score and
accuracy, compared with \texttt{lin}, \texttt{poly-1} and \texttt{poly-2}. We
thus made the choice not to include \texttt{poly-3} in the next steps.

When considering the cost parameter $C$ values, it is apparent (from Table~\ref{tab:pars}) that there is no
clear lead. We fixed the value to 0.001 for all following experiments, but we
understand that more investigation would be needed for a robust choice.
%\footnote{The standard deviations show a statistically significant difference
%of the means as can be tested using the standard T test}

Table \ref{tab:ig1} shows how many features were discarded by selecting only
features with information gain larger than 0. The counts vary for the four
possible feature values, so the table contains the average of these. For example,
7{,}750 character \mbox{$n$-grams} counted with relative frequencies were selected from
all character \mbox{$n$-grams}, in contrast to 9{,}566 character $n$-grams with
log-normalized values. 

%\clearpage
\subsection{Run 2}
We decided to perform further experiments, now employing
combinations of feature types. The aim was now to obtain a rough notion of the
difference in performance of individual and combined feature types. We used
settings as shown in Table \ref{tab:run2}, where all $n$-gram features were set
to $n=1,2,3$. Again, we simplified the task by considering only some of the
$2^8$ possible combinations. These combinations were motivated purely by
intuition and similar previous work.
\begin{table}[htb]
  \begin{center}
    \resizebox{\textwidth}{!}{
    \begin{tabular}{|l|l|}
      \hline
      Data           & \textsc{DEV-TEST} \\
      Feature types  &
      \specialcell{(CG, PG, OG), (CG, PG, WG, OG), (CG, PG, WG, OG, ER, FW),\\
        (ER, FW), (PG, OG), (SL, WL), (SL, WL, PG, ER, FW), \\
        (SL, WL, PG, FW), ALL} \\
      Feature values & \texttt{bin}, \texttt{raw}, \texttt{rel}, \texttt{log}\\
      Kernels        & \texttt{lin}, \texttt{poly-1}, \texttt{poly-2}\\
      C parameter    & $0.001$\\
      Information gain threshold & $0.001$ \\
      \hline
    \end{tabular}
    }
  \end{center}
  \caption{Settings for run 2 of experiments}
  \label{tab:run2}
\end{table}

Comparing some of the selected results in
Appendices \ref{app:run1} and \ref{app:run2} indicates that combining feature
types is not always for the best. For example, both \ac{POS}
$n$-grams and Open-Class \ac{POS} $n$-grams perform over 70\% accuracy on the
average in run 1, but combining them in run 2 leads to none or insignificant
improvement.

\begin{table}[htb]
  \begin{center}
    \resizebox{\textwidth}{!}{
    \begin{tabular}{|l|r|r|r|}
      \hline
      \textbf{Feature type} &
      \specialcell{\textbf{All features} \\ \textbf{(count)}}
      & \specialcell{\textbf{Selected features} \\ \textbf{(count)}}
      & \specialcell{\textbf{Discarded features} \\ \textbf{(percentage)}}\\
      \hline
      CG, PG, OG             & 34{,}923  & 12{,}352 & 65\%\\
      CG, PG, OG, WG         & 63{,}317  & 19{,}737 & 69\%\\
      CG, PG, WG, OG, ER, FW & 63{,}371  & 19{,}772 & 69\%\\
      ER, FW                 & 54        & 35       & 35\%\\
      PG, OG                 & 25{,}213  & 2{,}338  & 91\%\\
      SL, WL, PG, FW, ER     & 6{,}316   & 2{,}340  & 63\%\\
      SL, WL, PG, FW         & 6{,}274   & 2{,}340  & 63\%\\
      \hline
    \end{tabular}
    }
  \end{center}
  \caption{Feature selection -- run 2}
  \label{tab:ig2}
\end{table}

\begin{table}[htb]
  \centering
  \begin{minipage}{0.49\textwidth}
  \includegraphics[width=\linewidth]{img/ig/CG-log}
  \captionof{figure}{Character $n$-grams, \texttt{log}}
    \label{fig:ig1}
  \end{minipage}
  \begin{minipage}{0.49\textwidth}
  \includegraphics[width=\linewidth]{img/ig/WG-log}
  \captionof{figure}{Word $n$-grams, \texttt{log}}
    \label{fig:ig2}
  \end{minipage}
\end{table}

%\clearpage
\subsection{Run 3}
\begin{table}[htb]
  \begin{center}
    \resizebox{\textwidth}{!}{
    \begin{tabular}{|l|l|}
      \hline
      Data           & \textsc{DEV-TEST} \\
      Feature types  &
      \specialcell{CG[1-3], WG[1-3], PG[1-3], OG[1-3], ER, FW, WL, SL,\\
        (CG, PG, OG), (CG, PG, WG, OG), (CG, PG, WG, OG, ER, FW),\\
        (ER, FW), (PG, OG), (SL, WL), (SL, WL, PG, ER, FW), \\
        (SL, WL, PG, FW), all} \\
      Feature values & \texttt{bin}, \texttt{raw}, \texttt{rel}, \texttt{log}\\
      Kernels        & \texttt{lin}, \texttt{poly-1}, \texttt{poly-2}\\
      C parameter    & $0.001$\\
      Information gain threshold & $0.002$ \\
      \hline
    \end{tabular}
    }
  \end{center}
  \caption{Settings for run 3 of experiments}
  \label{tab:run3}
\end{table}
Regarding the plots of features with respect to their information
gain (see Figures \ref{fig:ig1}, \ref{fig:ig2} and Appendix \ref{app:IG}), we
applied more strict feature selection, selecting features with information gain
larger than $0.002$. 

This was thus the third run of experiments, with settings as in run 2, but
combining feature types of both previous runs. Table \ref{tab:res3} shows
selected results for each feature group.
As is obvious from Table \ref{tab:ig3}, the number of features dropped
significantly by applying a strict information gain threshold. A drop, though
less notable, also occurred in nearly all results (measured by accuracy and
F-score), compared to both previous runs.

\begin{table}[htb]
  \begin{center}
    \resizebox{\textwidth}{!}{
    \begin{tabular}{|l|r|r|r|}
      \hline
      \textbf{Feature type} &
      \specialcell{\textbf{All features} \\ \textbf{(count)}}
      & \specialcell{\textbf{Selected features} \\ \textbf{(count)}}
      & \specialcell{\textbf{Discarded features} \\ \textbf{(percentage)}}\\
      \hline
      CG[1-3] & 9{,}710 & 2{,}210 & 77\% \\
      WG[1-3] & 28{,}394 & 1{,}987 & 93\% \\
      PG[1-3] & 6{,}260 &  1{,}068 & 83\% \\
      OG[1-3] & 18{,}953 & 1{,}978 & 90\% \\
      ER & 42 & 17 & 60\% \\
      FW & 12 & 8& 33\% \\
      \hline
      All & 63{,}371 & 7{,}243 & 89\%\\
      \hline
    \end{tabular}
    }
  \end{center}
  \caption{Feature selection -- run 3}
  \label{tab:ig3}
\end{table}

%\clearpage
\section{Results}
For the final run and evaluation, we continued in employing the linear and
polynomial kernel functions and we also preserved the value of $C$, 0.001. Based
on development experiments, we made the choice to apply ``liberal'' feature
selection and only discarded features with zero information gain.

We decided to gain more details about the performance of $n$-gram features and 
apart from feature groups as used in run 3, we also tested individual uni-, bi-
and tri-gram groups.

Each feature group was tested for different feature values and kernel
functions, and out of approximately 350 results that we obtained alltogether,
210 (60\%) performed at accuracy above the majority baseline of 67\%.

For an overview of selected results, see Table \ref{tab:test} and Figure
\ref{fig:test}. Several observations can be made:
\begin{itemize}
  \item[--] Considering $n$-gram features (character, word, \ac{POS} and
    Open-Class \ac{POS} $n$-grams), a combination of all three $n$-gram types
    ($n=1,2,3$) never outperformed the best result of the individual groups.
  \item[--] Binary and log-normalized values of features seem to dominate -- 23
    out of 28 best results consist of binary or log-normalized values. This
    is in line with most of the previous work.
  \item[--] $n$-gram features in general are useful and generate the highest
    accuracies both on their own (character trigrams) and combined (character
    and OC-\ac{POS} $n$-grams).
  \item[--] As expected, a closer look at word unigrams, which seem to produce
    satisfying results, shows that topic bias is the main cause. When sorted by
    information gain, top ten features in word unigrams include words connected
    to the Russian language or country, like \textit{rusku} or
    \textit{ruska}. Similarly, top ten character trigrams include
    \textit{r}\quad\textit{u}\quad\textit{s}. Appendix \ref{app:IGexamples}
    contains both of these top ten lists and some more.
  \item[--] Error types did not perform highly (at 60\% accuracy). This is a
    surprising result, compared to e.g. \cite{aharodnik2013} or
    \cite{kochmar2011}, who also conducted binary classification experiments and
    used errors as features.
  \item[--] Tables \ref{tab:conf1} and \ref{tab:conf2} show two confusion
    matrices of the best results. We can say, that accurately identifying texts
    from authors with a Slavic language background using $n$-gram 
    features proved to be more  difficult than identifying non-Slavic
    texts. This can be derived from a high number of 
    mis-classified Slavic texts (false negatives), compared to a relatively low
    number of mis-classified non-Slavic texts (false positives).
\end{itemize}

\begin{table}[hb]
  \begin{center}
    \resizebox{0.4\textwidth}{!}{
    \begin{tabular}{|l|r|r|}
      \hline
      \textbf{Feature} & \textbf{Accuracy} & \textbf{F-score} \\
      \hline
      \textsc{CG}\textsubscript{bin}  & 72\% & 60 \\
      \textsc{CG1}\textsubscript{rel} & 65\% & 50 \\
      \textsc{CG2}\textsubscript{log} & 72\% & 57 \\
      \textbf{CG3\textsubscript{log}} & \textbf{77\%} & \textbf{54} \\
      \hline
      \textsc{WG}\textsubscript{bin}  & 74\% & 55 \\
      \textbf{WG1\textsubscript{bin}} & \textbf{78\%} & \textbf{48} \\
      \textsc{WG2}\textsubscript{raw} & 71\% & 47 \\
      \textsc{WG3}\textsubscript{log} & 72\% & 5 \\
      \hline
      \textsc{PG}\textsubscript{log}  & 71\% & 55 \\ 
      \textsc{PG1}\textsubscript{log} & 66\% & 50 \\
      \textbf{PG2\textsubscript{bin}} & \textbf{74\%} & \textbf{49} \\
      \textsc{PG3}\textsubscript{raw} & 74\% & 47 \\
      \hline
      \textsc{OG}\textsubscript{bin}  & 72\% & 54 \\
      \textsc{OG1}\textsubscript{log} & 69\% & 54 \\
      \textbf{OG2\textsubscript{bin}} & \textbf{75\%} & \textbf{46} \\
      \textsc{OG3}\textsubscript{raw} & 73\% & 48 \\
      \hline
    \end{tabular}
    }
    \quad
    \resizebox{0.55\textwidth}{!}{
    \begin{tabular}{|l|r|r|}
      \hline
      \textbf{Feature} & \textbf{Accuracy} & \textbf{F-score} \\
      \hline
      \textbf{CG, OG\textsubscript{bin}} & \textbf{78\%} & \textbf{55} \\
      \textsc{CG, PG}\textsubscript{bin} & 75\% & 62 \\
      \textsc{PG, OG}\textsubscript{bin} & 74\% & 21 \\
      \textbf{CG, PG, OG\textsubscript{bin}} & \textbf{78\%} & \textbf{53} \\
      \textbf{[CPOW]G\textsubscript{bin}} & \textbf{77\%} & \textbf{61}\\
      \textsc{ER}\textsubscript{log} & 60\% & 47 \\
      \textsc{FW}\textsubscript{log} & 58\% & 44 \\
      \textsc{ER, FW}\textsubscript{raw} & 64\% & 49 \\
      \textsc{[CPOW]G, ER, FW}\textsubscript{bin} & 76\% & 61 \\
      \textsc{SL} & 50\% & 40 \\
      \textsc{WL} & 54\% & 40 \\
      \textsc{SL, WL} & 53\% & 40 \\
      \textsc{SL, WL, PG, FW}\textsubscript{log} & 73\% & 50 \\
      \textsc{[SW]L, PG, FW, ER}\textsubscript{bin} & 72\% & 41 \\
      \hline
      \textsc{ALL}\textsubscript{bin} & 76\% & 61\\
      \hline
    \end{tabular}
    }
  \end{center}
    \caption{Selected results -- \textsc{TEST} run}
    \label{tab:test}
\end{table}

\begin{table}[hbt]
  \begin{center}
    \begin{tabular}{r|r|r|r}
      \multicolumn{1}{r}{} &  \multicolumn{2}{c}{Predicted class}\\
      \multicolumn{1}{r}{} &
      \multicolumn{1}{c}{\textbf{Slavic}} &
      \multicolumn{1}{c}{\textbf{non-Slavic}}\\
      \cline{2-3}
      \parbox[t]{15mm}{ \multirow{2}{*}{\rotatebox[origin=c]{90}{True class} } }
      \textbf{Slavic} & 150 & 158 & 308\\
      \cline{2-3}
      \textbf{non-Slavic} & 86 & 717 & 803\\
      \cline{2-3}
      \multicolumn{1}{r}{Sum} & \multicolumn{1}{r}{236} &
      \multicolumn{1}{r}{875} & \multicolumn{1}{r}{\textbf{1111}}\\
    \end{tabular}
  \end{center}
  \caption{Confusion matrix -- \textsc{CG, OG\textsubscript{bin}}. Precision
    64\%, recall 49\%.}
  \label{tab:conf1}
\end{table}

\begin{table}[hbt]
  \begin{center}
    \begin{tabular}{r|r|r|r}
      \multicolumn{1}{r}{} &  \multicolumn{2}{c}{Predicted class} &
      \multicolumn{1}{r}{} \\
      \multicolumn{1}{r}{} &
      \multicolumn{1}{c}{\textbf{Slavic}} &
      \multicolumn{1}{c}{\textbf{non-Slavic}} & \multicolumn{1}{r}{Sum}\\
      \cline{2-3}
      \parbox[t]{15mm}{ \multirow{2}{*}{\rotatebox[origin=c]{90}{True class} } }
      \textbf{Slavic} & 138 & 170 & 308\\
      \cline{2-3}
      \textbf{non-Slavic} & 78  & 725 & 803\\
      \cline{2-3}
      \multicolumn{1}{r}{Sum} & \multicolumn{1}{r}{216} &
      \multicolumn{1}{r}{895} & \multicolumn{1}{r}{\textbf{1111}}\\
    \end{tabular}
  \end{center}
  \caption{Confusion matrix -- \textsc{CG, PG, OG\textsubscript{bin}}. Precision
  64\%, recall 45\%.}
  \label{tab:conf2}
\end{table}

\begin{figure}[hbt]
  \includegraphics[width=\textwidth]{img/testdata}
  \caption{Scatterplot of selected results -- \textsc{TEST} run}
  \label{fig:test}
\end{figure}
