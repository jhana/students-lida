\chapter{Native Language Identification Background}
\label{chap:3}

%In this chapter, we will first briefly introduce authorship attribution and
%profiling (inferring characteristics of an author from the characteristics of
%documents written by that author\footnote{As defined in \cite{juola2006}})
%according to \cite{koppel2009}. Then we will proceed to an overview of previous
%work in the area of \acf{NLI}, considering it to be an authorship profiling task.
The general task of examining text in order to determine or verify
characteristics of the text's author is commonly referred to as
\textit{authorship attribution}. The task can be broadly defined on any piece of
linguistic data (\cite{juola2006}), but we will further on assume written text.
\cite{koppel2009} provide a detailed overview of previous work in the area of
statistical authorship attribution. In one of the recognized scenarios, in the
\textit{profiling} problem, the aim is to provide as much demographic or
psychological information about the author as possible. This information might
include gender (\cite{koppel2002}), age (\cite{schler2006}) or even personality
traits (\cite{pennebaker2003}).
  
We consider the task of \acl{NLI} to be an authorship attribution problem of the
profiling scenario.
In recent years, serious achievements in \ac{NLI} have been accomplished by
treating the task as a machine learning problem. Existing approaches differ in
several ways:

First, most use English as the target language. But in the last
few years, like us, some (e.g. \cite{aharodnik2013}, \cite{malmasi2014arabic},
\cite{malmasi2014chinese}) have concentrated on other languages as
well. Second, even when working with one language, different corpora are being
used, resulting in limited comparability.\footnote{\cite{tetreault2012} suggest
  that proficiency reporting would help in comparing results across different
  corpora.} Finally, a great variety of features are explored and
implemented. We keep these differences in mind throughout this section, which
provides an overview of previous work in the area of \ac{NLI}. 

\section{English NLI}
  
The first work focusing on identifying native language from text was done by
\cite{koppel2005}, who used data from the \ac{ICLE} (\cite{icle2002}). The corpus
consists of essays written by university students of the same English
proficiency level. The authors classified a sample of essays into 5 classes by
the students' native language: Czech, Bulgarian, Russian, French and Spanish.
They achieved an accuracy of 80.2\% with the 20\% baseline using function words,
character n-grams, error types and rare (less frequent) POS bigrams as
features. Feature values are computed as frequencies relative to document
length. Orthographic errors were found in texts by the MS Word spell checker and
then assigned a type by a separate tool. Focusing on these, the authors explore
and find some distinctive patterns useful for identifying native speakers of
particular languages -- for example, native speakers of Spanish tended to
confuse \textit{m} and \textit{n} (\textit{confortable}) or \textit{q} and
\textit{c} (\textit{cuantity}, \textit{cuality}).  

The \ac{ICLE} has been a popular choice for many others. See Table
\ref{tab:algs} for a categorization by corpora and classification algorithms. We
will now distinguish previous experiments by feature types used.

\subsection{Feature Types}

\begin{itemize}
\item[--] \textit{Syntactic features}: \cite{wong2009} replicate the work of
  \cite{koppel2005} on the second version of the \ac{ICLE}
  (\cite{icle2009}, \ac{ICLE}v2) choosing the same five languages and adding
  Chinese and Japanese. They explore the role of three syntactic error
  types as features. The errors (subject-verb disagreement, noun-number
  disagreement and determiner disuse) are detected by a grammar checker,
  \textit{Queequeg}.\footnote{\url{http://queequeg.sourceforge.net/index-e.html}}
  Classification with these three features (represented as relative frequencies)
  alone gives an accuracy of about 24\%. However, combining the features used by
  \cite{koppel2005} with the three syntactic errors types does not lead to any
  improvement in accuracy, sometimes even causing accuracy decrease.

  Syntactic features are further evaluated on the same data set (7 languages
  from the second version of the \ac{ICLE}) by \cite{wong2011}. They introduce
  sets of \ac{CFG} production rules as binary features.
  The rules are extracted using the Stanford parser (\cite{klein2003}) and the
  Charniak and Johnson parser (\cite{charniak2005}) and tested in two settings, 
  \textit{lexicalised}\footnote{A non-terminal is annotated with its lexical
    head (a single word). For example, the rule VP$\,\to\,$VBD NP PP could
    be replaced with a rule such as VP(dumped) $\,\to\,$ VBD(dumped) NP(sacks)
    PP(into) (example from \cite{martin2000}).}
  with function words and punctuation and \textit{non-lexicalised}.  

  \cite{bykh2014} follow this approach by also concentrating on \ac{CFG}
  rule features for the task of \ac{NLI}. They consider and systematically
  explore both non-lexicalized and lexicalized \ac{CFG} features, experimenting
  with different feature representations (binary values, normalized
  frequencies). They define three feature types: phrasal \ac{CFG} production rules
  excluding all terminals (e.g. S $\rightarrow$ NP), lexicalized \ac{CFG}
  production rules of the type preterminal $\rightarrow$ terminal (e.g. JJ
  $\rightarrow$ nice) and the union of these two. The obtained results vary
  greatly when comparing single-corpus (best reported results of 78.8\%) and
  cross-corpus (best reported results of 38.8\%) settings, confirming the
  challenge of achieving high cross-corpus results in the task of \ac{NLI}.

\item[--] Apart from syntactic features, the significance of using character,
  word or \ac{POS} \textit{n-grams} when dealing with the task of \ac{NLI} has
  been addressed by several authors:

  \cite{tsur2007} also follow \cite{koppel2005} by choosing the same five
  languages\footnote{Bulgarian, Czech, French, Russian, Spanish} from the
  \ac{ICLE}. Forming a hypothesis that the choice of words people make when
  writing in a second language is influenced by the phonology of their native
  language, they focus on \textit{character n-grams} with an emphasis on
  bigrams. By selecting 200 most frequent bigrams in the whole corpus, an
  accuracy of 65.6\% is achieved. Repeating the experiment with 200 most
  frequent trigrams yields an accuracy of 59.7\%.
  
\begin{table}[ht]
  \begin{center}
    \resizebox{0.9\textwidth}{!}{
      \begin{tabular}{|r|c|c|}
        \hline
        & $n=1$ & $n=2$ \\
        \hline
        \textbf{Word-based n-grams} & (\textit{analyzing}), (\textit{attended}) &
        (\textit{aspect of}), (\textit{could only}) \\
        \textbf{\acs{POS}-based n-grams} & (\textit{NNP}), (\textit{MD}) &
        (\textit{NNS MD}), (\textit{NN RBS}) \\
        \textbf{Open-Class-POS-based n-grams} & (\textit{far}), (\textit{VBZ}) &
        (\textit{NN whenever}), (\textit{JJ well}) \\
        \hline
      \end{tabular}
    }
  \end{center}
  \caption{Examples of features used by \cite{bykh2012}.}
  \label{tab:ngramsbykh}
\end{table}
  
  \cite{bykh2012} introduce classes of recurring n-grams (n-grams that
  occur in at least two different essays of the training set) of various lengths
  as features in their experiment. Three feature classes are described:
  \textit{word-based n-grams} (the surface forms), \textit{POS-based n-grams}
  (all words are converted to the corresponding \ac{POS} tags) and
  \textit{Open-Class-POS-based n-grams} (n-grams, where nouns, verbs, adjectives
  and cardinal numbers are replaced by corresponding \ac{POS} tags). See Table
  \ref{tab:ngramsbykh} for examples of these feature classes.
  Essays are represented as binary feature vectors. Experiments included both
  single $n$ (unigrams, bigrams etc.) and [1-$n$] n-gram (uni-grams, uni- and
  bigrams, uni-, bi- and trigrams etc.) settings. Without discarding any features,
  \cite{bykh2012} confirm satisfying results for word [1-2]-gram features with
  accuracy nearly 90\%, and for Open-Class-POS-based [1-3]-grams (80.6\%).

\item[--] \textit{Function words}: Further replicating the work of
  \cite{koppel2005}, \cite{tsur2007} test the performance of function word based
  features. Relative frequencies of 460 English function words give 66.7\%
  accuracy. Function words are also employed by \cite{brooke2011},
  \cite{brooke2012}, \cite{tetreault2012} and others.

\end{itemize}

\cite{kochmar2011} uses a subset of the
\ac{CLC}\footnote{\url{http://www.cambridge.org/us/cambridgeenglish/about-cambridge-english/cambridge-english-corpus}}
and investigates binary classification of related Indo-European language pairs
(e.g. Spanish-Catalan, Danish-Swedish). This work presents a systematic study of
various feature groups and their contribution to overall classification results.
Employed features include \ac{POS} n-grams, character n-grams and phrase
structure rules. Unlike most of other studies, which use the Penn
Treebank tagset, \cite{kochmar2011} uses the CLAWS
tagset.\footnote{\url{http://ucrel.lancs.ac.uk/claws/}} Apart from the mentioned
features, the author also concentrates on an \textit{error-based} analysis, examining
error type rates (normalized by text length), error type distribution
(normalized by number of error types in text) and error content (error codes are
associated with the incorrect word forms).

\begin{table}[ht]
  \begin{center}
    \resizebox{\textwidth}{!}{
      \begin{tabular}{|l|l|l|}
        \hline
        \textbf{Author(s)} & \textbf{Data} & \textbf{Algorithm} \\
        \hline
        \cite{koppel2005} & \ac{ICLE}   & \ac{SVM} \\
        \cite{tsur2007}   & \ac{ICLE}   & \ac{SVM} \\
        \cite{wong2009}   & \ac{ICLE}v2 & \ac{SVM} \\
        \cite{wong2011}   & \ac{ICLE}v2 & MaxEnt \\
        \cite{brooke2011} & \ac{ICLE}v2, Lang-8 & \ac{SVM} \\
        \cite{kochmar2011}& \acs{CLC} & \ac{SVM} \\
        \cite{brooke2012} & \ac{ICLE}v2, Lang-8, CLC & MaxEnt, \ac{SVM} \\
        \cite{tetreault2012} & \acs{ICLE-NLI}, \acs{TOEFL7}, \acs{TOEFL11},
        \acs{TOEFL11}-Big & Logistic regression \\
        \cite{bykh2012}   & \ac{ICLE}v2 & \ac{SVM} \\
        \cite{bykh2014}   & \acs{TOEFL11}, \acs{NT11} & Logistic regression \\
        \hline
      \end{tabular}
      }
  \end{center}
  \caption{Summary of previous work -- corpora and algorithms}
  \label{tab:algs}
\end{table}

\subsection{Cross-corpus evaluation}

Some researchers test generalizability of their results. For example,
\cite{bykh2012} conducted a second set of experiments, using \ac{ICLE} data for
training and a set of other corpora (Non-Native Corpus of
English,\footnote{essays written by Spanish native speakers} Uppsala Student
English Corpus\footnote{essays written by Swedish native speakers} and Hong Kong
University of Science and Technology English Examination Corpus\footnote{essays
  written by Chinese native speakers}) for testing. The results obtained in a
cross-corpus evaluation vary from 86.2\% (Open-Class-\ac{POS} n-grams) to 87.6\%
(surface-based word n-grams), suggesting, that the features introduced by using
the \ac{ICLE} generalize well to other corpora.

A previous study though, by \cite{brooke2011}, states quite the opposite and
criticizes the usage of the \ac{ICLE} for the task of \acl{NLI}. The authors
test their claim that topic bias plays a major role during classification using
\ac{ICLE}: 

They infer this from an experiment which compares classification performance on
randomly split and topic-based split data. For example, when using character
n-grams, randomized split performance is more than 80\%, whereas only 50\% is
achieved with a topic based split. \cite{brooke2011} introduce Lang-8, an
alternative web-scraped corpus. Data is derived from the Lang-8
website,\footnote{\url{http://lang-8.com}} which contains journal entries by
language learners, which are corrected by native speakers.

\cite{brooke2012} further explore cross-corpus evaluation using Lang-8 as the
training corpus and the \ac{ICLE} and a sample of the \ac{CLC} for
testing. Three experiments are evaluated: distinguishing between 7
languages\footnote{Polish, Russian, French, Spanish, Italian, Chinese,
  Japanese}, between 5 European languages and between Chinese and Japanese.

\subsection{Native Language Identification Shared Task 2013}
In 2013, a \ac{NLI} shared task\footnote{See \url{https://sites.google.com/site/nlisharedtask2013/home} for more details}
was organised, addressing goals of \ac{NLI} community unification and field
progression. The 29 participating teams gained access to the \acs{TOEFL11}
corpus (\cite{TOEFL11}), which consists of 1100 essays per language, covering 11
languages. The essays were collected through the college entrance \ac{TOEFL}
test delivery system of the \ac{ETS}. 
\cite{tetreault2013report} provide a comprehensive overview of the results of
the shared task. The shared task consisted of three sub-tasks, differing in the
training data used. The main subtask restricted training data to
\acs{TOEFL11-TRAIN}, a specified subset of \acs{TOEFL11}.
Following prior work (\cite{koppel2005}, \cite{wong2009} etc.), a majority of
the teams used \acfp{SVM}.

\begin{table}[h]
  \begin{center}
    \resizebox{\textwidth}{!}{
    \begin{tabular}{|l|r|l|l|}
      \hline
      \specialcell{\textbf{Team Name}\\\textbf{Abbreviation}} &
      \specialcell{\textbf{Overall}\\\textbf{Accuracy}} &
      %\specialcell{\textbf{Learning}\\\textbf{Method}} &
      \textbf{Learning Method} &
      \textbf{Feature Types} \\ 
      \hline
      JAR & 84\% & \ac{SVM} & word, \ac{POS} and character n-grams \\
      OSL & 83\% & & word and character n-grams \\
      BUC & 83\% & Kernel Ridge Regression  &  character-based \\
      CAR & 83\% & Ensemble & word, \ac{POS} and character n-grams
      \\
      TUE & 82\% & \ac{SVM} & word and \ac{POS} n-grams, syntactic features
      \\ 
      NRC & 82\% & \ac{SVM} & word, \ac{POS} and character n-grams,
      syntactic features \\
      HAI & 82\% & Logistic Regression & \ac{POS} and character n-grams,
      spelling features \\ 
      CN  & 81\% & \ac{SVM} & word, \ac{POS} and character n-grams,
      spelling features \\
      NAI & 81\% & & word, \ac{POS} and character n-grams,
      syntactic features \\
      UTD & 81\% & & \\
      \hline
    \end{tabular}}
  \end{center}
  \caption{An overview of features and learning methods used by the top 10 teams
    in the \ac{NLI} Shared Task. Based on Tables 3, 7 and 8 from
    \cite{tetreault2013report}}  
  \label{tab:sharedtask}
\end{table}

The most common features were word, character and \ac{POS} n-grams (see
Table \ref{tab:sharedtask}), typically ranging from unigrams to
trigrams. However for example \cite{jarvis2013} tested usage of character
n-grams with $n$ as high as 9 and reports levels of accuracy nearly as high when
using a model based on character n-grams as the winning model involving lexical
and \acl{POS} n-grams.

\cite{popescu2013} submitted a model based solely on character-level features,
treating texts as sequences of symbols. Their system made use of string kernels
and a biology-inspired kernel.

\cite{hladka2013} distinguish between n-grams of words and n-grams of lemmas
(base forms of words) and also introduce two types of \textit{skipgrams} of
words: 
\begin{itemize}
  \item[--] First, for a sequence of words $w_{i-3}, w_{i-2}, w_{i-1}, w_{i}$,
    bigram $(w_{i-2}, w_{i})$ and trigrams $(w_{i-3}, w_{i-1}, w_{i})$,
    $(w_{i-3}, w_{i-2}, w_{i})$ are extracted (Type 1).
  \item[--] Second, for a sequence of words
    $w_{i-4}, w_{i-3}, w_{i-2}, w_{i-1}, w_{i}$, bigrams $(w_{i-3}, w_{i})$,
    $(w_{i-4},w_{i})$ and trigrams $(w_{i-4}, w_{i-3}, w_{i})$,
    $(w_{i-4}, w_{i-2}, w_{i})$ and $(w_{i-4}, w_{i-1}, w_{i})$ are
    extracted (Type 2).
\end{itemize}

See the following example sentence:\footnote{Example sentence from the Cze-SLT
  corpus (\cite{czesl-cgt}).}      
\begin{table}[h]
  \begin{center}
    \begin{tabular}{*{5}{c}}
      $w_{i-4}$ & $w_{i-3}$ & $w_{i-2}$ & $w_{i-1}$ & $w_{i}$ \\
      \textit{Ja} & \textit{bych} & \textit{koupila} & \textit{sobě} &
      \textit{auto}\\
      I & would & buy & myself & a car \\
    \end{tabular}
  \end{center}
\end{table}

\begin{table}[h]
  \begin{center}
     \resizebox{\textwidth}{!}{
       \begin{tabular}{|l|ll|}
         \hline
         & $n=2$ & $n=3$ \\
         \hline
         Type 1& (\textit{koupila}, \textit{auto})& (\textit{bych}, \textit{sobě},
         \textit{auto}), (\textit{bych}, \textit{koupila}, \textit{auto}) \\
         Type 2& (\textit{bych}, \textit{auto}), (\textit{Ja}, \textit{auto})&
         (\textit{Ja}, \textit{bych}, \textit{auto}), (\textit{Ja}, \textit{koupila},
         \textit{auto}), (\textit{Ja}, \textit{sobě}, \textit{auto}) \\
         \hline
       \end{tabular}
     }
  \end{center}
  \caption{Skipgrams of example sentence}
\end{table}

These skipgrams are in line with the definition in \cite{guthrie2006}, with the
difference, that 0-skip n-grams are not considered, as they are already
represented in the feature class of word n-grams.

\cite{malmasi2013} propose \textit{function word n-grams} as a novel
feature. Function word n-grams are defined as a type of word n-grams, where
content words are skipped. The example that the authors present is the
following: the sentence \textit{We should all start taking the bus} would be
first stripped of content words and reduced to \textit{we should all the}, from
which n-grams would be extracted. 

Syntactic features (previously evaluated by \cite{wong2009} and \cite{wong2011})
were used by six teams, though all of them combined these with word, character
or \ac{POS} n-grams and it is thus hard to say how big the role the syntactic
features played. For example, \cite{malmasi2013} implemented \ac{TSG} fragments
and Stanford dependencies\footnote{Counts of basic dependencies extracted using
  the Stanford Parser
  (\url{http://nlp.stanford.edu/software/stanford-dependencies.shtml}).
  An example from  \cite{demarneffe2008}: considering the sentence
  \textit{Sam ate 3 sheep}, one of the extracted grammatical relations would be
  a \textit{numeric modifier} (any number phrase that serves to modify the
  meaning of the noun with a quantity), represented as \textit{num}(sheep,
  3).} 
(following \cite{tetreault2012}) as features. 
  
\section{non-English NLI}
The majority of experiments have been carried out using texts written in
English, with various native language (L1)
background of the authors. Recently, like we do, some
researchers have also focused on non-English second languages: Czech, Chinese,
Arabic, Finnish and Norwegian. We provide a summary of their work. One has to
take into account that these results are even less comparable, but serve well
when validating language independent methods. For a brief overview, see
Table \ref{tab:nonenglish}.
\begin{table}[h]
  \begin{center}
    \resizebox{\textwidth}{!}{
      \begin{tabular}{|l|l|l|l|}
        \hline
        \textbf{Author(s)}  & \textbf{Language} &
        \specialcell{\textbf{Learning}\\ \textbf{Method}} &
        \textbf{Feature Types} \\
        \hline
        \cite{aharodnik2013} & Czech & \ac{SVM} & \acs{POS} n-grams, error types
        \\
        %\hline
        \cite{malmasi2014chinese} & Chinese & \ac{SVM} & \acs{POS} n-grams,
        function words, \acs{CFG} production rules \\
        %\hline
        \cite{wang2016} & Chinese & \ac{SVM} &  \specialcell{\acs{POS},
          character and word n-grams, function words,\\\acs{CFG} production
          rules, skip-grams} \\ 
        %\hline
        \cite{malmasi2014arabic}  & Arabic & \ac{SVM} & \acs{POS} n-grams,
        function words, \acs{CFG} production rules\\
        \cite{malmasi2014finnish} & Finnish & \ac{SVM} & \ac{POS} and character
        n-grams, function words\\ 
        \cite{malmasi2015norwegian} & Norwegian & \ac{SVM} &
        \specialcell{\ac{POS} n-grams, mixed \ac{POS}-function word n-grams,
          \\function words}\\ 
        \hline
      \end{tabular}
    }
  \end{center}
  \caption{Summary of previous work -- non-English data}
  \label{tab:nonenglish}
\end{table}

\subsection{Czech}

To our knowledge the only previous work in NLI which focused on Czech as the
target language was conducted by \cite{aharodnik2013}, who work
with data which formed the basis for the CzeSL-SGT corpus used by
us.\footnote{See Section \ref{sec:data.corpus}}
Using about 1400 essays from the Czech as a Second Language (CzeSL) corpus
(\cite{hana2010}), the authors classify Czech texts into two classes,
determining whether the L1 of the author belongs to the Indo-European or
non-Indo-European language group. This work is the closest related to our
experiment.

As their primary goal, they focus on using only non-content based features to
avoid corpus specific dependency. These consist of a set of 264
\ac{POS} bi-grams and 305 tri-grams (with 3 or more occurrences in the data) and
35 extracted error types. The authors use a \ac{SVM} classifier and represent
feature values as term weights $S$, which are computed as a rounded logarithmic
ratio of the token $i$ frequency in document $j$ to the total amount of tokens in
the document (\cite[p.580]{manning1999}):
\begin{align}
  {S_{ij} = round \bigg( 10 \times \frac{1+\log(tf_{ij})}{1+\log(l_j)}}\bigg)
  \label{eq:logfreq}
\end{align}

A combination of all three types of features (errors and \ac{POS} n-grams)
yields a promising performance of 85\% precision and 89\% recall. Errors alone
also perform well at 84\% precision and recall. Results distinguishing
performance on different levels of author proficiency are provided.

\subsection{Chinese}

The first to develop a \ac{NLI} system for Chinese, \cite{malmasi2014chinese}
combine sentences from the same L1 to manually form documents for
classification, as full texts are not available in the Chinese Learner Corpus
(\cite{wang2012clc}). 3.75~million tokens of text are extracted. The authors use
11 classes, some (Korean, Spanish, Japanese) overlapping with the languages of
the \acs{TOEFL11} corpus, which was previously used in the \ac{NLI} Shared
Task. Due to the fact that the Chinese Learner Corpus is not topic-balanced, the
authors avoid topic-dependent lexical features such as character or word
n-grams. Experiments are run with both binary and normalized features and
results indicate normalized features to be more informative (in contrast,
\cite{brooke2012} make an opposite finding for English \ac{NLI}). By combining
all features, that is \ac{POS} n-grams (n = 1,2,3), function words and \ac{CFG}
production rules, the highest achieved accuracy is 70.6\%.

\cite{wang2016} use the Jinan Chinese Learner Corpus (\cite{wang2015jclc}),
which mainly consists of essays written by speakers of languages of Asia (most
frequently Indonesian: 39\% of essays, Thai: 15\%  and Vietnamese: 9\%). Adopted
features include character, word and \ac{POS} n-grams, function words, \ac{CFG}
production rules and 1-skip bigrams (based on \cite{guthrie2006}): for a
sequence of words $$w_{i-4}, w_{i-3}, w_{i-2}, w_{i-1}, w_{i}$$
bigrams $(w_{n-1}, w_{n}), (w_{n-2}, w_{n})$ are extracted, here we would obtain
7 bigrams and $n$ would range from $i$ to $i-3$.
For example, from the sentence \textit{Ja bych koupila sobě auto}, bigrams
(\textit{Ja}, \textit{bych}), 
(\textit{bych}, \textit{koupila}), (\textit{koupila}, \textit{sobě}),
(\textit{sobě}, \textit{auto}), (\textit{Ja}, \textit{koupila}), (\textit{bych},
\textit{sobě}) and (\textit{koupila}, \textit{auto}) would be extracted. 

\begin{figure}[h]
  \centering
  \includegraphics[page=1, width=.6\linewidth]{img/wang2016}
  \caption{}
  \label{fig:wang2016}
\end{figure}
\cite{wang2016} achieve an average accuracy of 75.3\%, with highest scores for
essays written by speakers of Thai (84.5\%) and lowest for speakers of
Mongolian (33.6\%). The authors consider low performance to be a consequence of
insufficient size of training data. The relationship between training data size
and classification accuracy is illustrated in Figure
\ref{fig:wang2016}.

\subsection{Arabic}

\cite{malmasi2014arabic} work with a subset of the second version of the
\ac{ALC},\footnote{\url{http://www.arabiclearnercorpus.com/}} which contains
texts by Arabic learners studying in Saudi Arabia. The subset used for
experiments consists of 329 essays of speakers with 7 different native language
backgrounds. Given the topic imbalance of the \ac{ALC}, the authors choose to
avoid lexical features and concentrate on three syntactic feature 
types: \ac{CFG} production rules, Arabic function words and \ac{POS} n-grams.
For single feature type settings, \ac{POS} bigrams performed best. All features
combined provided an accuracy of 41\%. 

\subsection{Finnish}

\cite{malmasi2014finnish} use a subset of 204 documents from the Corpus of
Advanced Learner Finnish (\cite{ivaska2014}). Function words, \ac{POS} n-grams of
size 1-3 and character n-grams up to n=4 are used as features. The authors use a
majority baseline of 19.6\% and report an accuracy of 69.5\% using a combination
of all features. A second experiment is conducted for distinguishing non-native
writing, achieving 97\% accuracy using a 50\% chance baseline. Here all
features score 88\% or higher.

\subsection{Norwegian}

Following \cite{malmasi2014chinese}, \cite{malmasi2015norwegian} extract 750k
tokens of text in the form of individual sentences from the
\textit{Andrespråkskorpus} (\cite{tenfjord2006}) consisting of essays written by
learners of Norwegian. The selected sentences are combined to create a set of
2\,158 documents of similar length written by authors of 10 native languages.
Three types of features are used: Norwegian function words and function word
bigrams (following \cite{malmasi2013}, a sentence is first stripped from
content words and then n-grams are extracted), \ac{POS} n-grams and mixed
\acl{POS}/function word n-grams (\ac{POS} n-grams where the surface form of
function words is retained).
Using the majority baseline of 13\%, all features combined perform at 78.6\%
accuracy. \ac{POS} n-gram models prove to be useful, unigrams achieveing
61.2\%, bigrams 66.5\% and trigrams 62.7\% accuracy. 


