\chapter{Machine Learning Background}
\label{chap:2}
\acresetall

\section{Support Vector Machine Classification}
In a general $n$-ary \textit{text classification} task, we are given a document
represented by a vector $\bm{x} \in \mathbb{X}$ and a set of $n$ classes
$\mathbb{Y} = \{y_{1},y_{2},\dots,y_{n}\}$. Then using a learning method, we want to
learn a \textit{decision function $f$} that maps documents to classes:
$$f:\mathbb{X}\to\mathbb{Y}.$$ This decision function will (in an ideal case)
allow us to map new unseen examples. This process is commonly referred to as
\textit{supervised learning}, in contrast to \textit{unsupervised learning},
where no explicit labels are assigned to data and the learning method only works
with observed patterns and extracted statistical structure.

We will further on generally assume a binary classification task. Documents are
represented as \textit{feature vectors} $\bm{x_{i}} \subseteq \mathbb{R}^n$ and
we work with a set of \textit{training data} $$\{\bm{x_{i}},y_{i}\}_{i=1}^d$$
from which the decision function $f:\mathbb{R}^n\to\mathbb{Y}$ is learned. Here
$d$ is the number of training examples and $y_{i} \in \{-1,+1\}$ denote the
class labels. 

In the following three subsections, we will first concentrate on the simplest
task -- classifying linearly separable data with \acp{SVM}, next we explain how
the method is applied to general unseparable data containing outliers
(observations with extreme values) or noisy data (corrupted observations) and in
the third section we focus on how \acp{SVM} deal with data that does not allow
linear separation at all. 

\subsection{Large/Hard Margin Classification: Linearly Separable Data}

\begin{table}[ht]
  \centering
  \begin{minipage}{0.49\textwidth}
    \includegraphics[page=1,width=\linewidth]{img/figs}
    \captionof{figure}{}
    \label{fig:1}
  \end{minipage}
  \begin{minipage}{0.49\textwidth}
    \includegraphics[page=10,width=\linewidth]{img/figs}
    \captionof{figure}{}
    \label{fig:10}
  \end{minipage}
\end{table}

Consider the datasets in Figures \ref{fig:1} and
\ref{fig:10}, both of which consist of two classes. Clearly both of the datasets
are somehow separable. In \ref{fig:1}, we can separate the two classes perfectly
by drawing a line between them. In \ref{fig:10}, we can separate the two classes
by a circle, but no straight line can be drawn between them. We will call
datasets like the one in Figure \ref{fig:1} \textit{linearly separable} and we
will concentrate on these in this section.

\begin{table}[ht]
  \centering
  \begin{minipage}{0.49\textwidth}
    \includegraphics[page=2,width=\linewidth]{img/figs}
    \captionof{figure}{}
    \label{fig:2}
  \end{minipage}
  \begin{minipage}[h]{0.49\textwidth}
    \includegraphics[page=11,width=\linewidth]{img/figs}
    \captionof{figure}{}
    \label{fig:11}
  \end{minipage}
\end{table}


Linearly separable datasets can be in fact separated by an infinite number of
lines (Figure \ref{fig:2}). These lines are \textit{hyperplanes} of a two
dimensional space. To generalize, 
a hyperplane of an $n$-dimensional space is a subspace with dimension $n-1$, a
set of points satisfying the equation $$\bm{w}^T\bm{x}+b=0,$$ where $\bm{w}$, the
\textit{parameter vector} or \textit{weight vector}, is normal (orthogonal to
any vector lying on the hyperplane), $\bm{x}$ is the vector representation of the
document and $b \in \mathbb{R}$ moves the hyperplane in the direction of
$\bm{w}$ (See Figure \ref{fig:11}).
The form of the decision function for document $\bm{x}$ can now be defined as
$$f(\bm{x}) = sgn(\bm{w}^T\bm{x}+b),$$ a value of $-1$ indicating one class and
$+1$ indicating the other class. 

Given a data set and a particular hyperplane, the
\textit{functional margin $\phi_{i}$} of an example $\bm{x}_i$ is defined as
$y_{i}(\bm{w}^T\bm{x}_i +b)$ (\dots) and the \textit{geometric margin $\gamma_{i}$}
$$\gamma_{i} = \frac{\phi_{i}}{\|\bm{w}\|} = \frac{|f(\bm{x_{i}})|}{\|\bm{w}\|}$$
gives us the Euclidean distance between $\bm{x}_{i}$ and the
hyperplane.

A \acf{SVM} (\cite{vapnik1979},\cite{vapnik1982}) is a hyperplane based
classifier that in addition to finding a separating hyperplane defines it to be
as far away from the nearest data instances (the \textit{support vectors}) as
possible. That is it maximizes the margin of the classifier
$\gamma = \frac{2}{\|\bm{w}\|}$, which is the width of the band drawn between
the data instances closest to the hyperplane (Figure \ref{fig:3}).

\begin{figure}[h]
  \centering
  \includegraphics[page=3, width=.6\linewidth]{img/figs}
  \caption{}
  \label{fig:3}
\end{figure}

To find the best separating hyperplane, the formulation is in the form of a
minimization problem (as maximizing $\gamma$ is the same as minimizing
$\frac{1}{\gamma}$):\footnote{Recall $\|\bm{w}\| = \sqrt{\bm{w}^T\bm{w}}$}
\begin{align*}
  \text{Minimize } \frac{1}{2} \bm{w}^T\bm{w} \\
  \text{Subject to } y_i(\bm{w}^T\bm{x}_i + b) \geq 1\\
  i = 1 \dots n.
\end{align*}

To solve this problem using the \textit{method of Lagrange multipliers}, we
introduce a Lagrange multiplier $\alpha_i$ for each training 
example $(\bm{x}_i,y_i)$. Given $\bm{\alpha} = (\alpha_i\dots \alpha_n)$, the
primal Lagrangian function is given by
\begin{align}
  \label{eq:lagrange}
  L(\bm{w},b,\bm{\alpha}) &= \frac{1}{2}\bm{w}^T\bm{w} - \sum_i \alpha_i(y_{i}
  (\bm{w}^T \bm{x}_i + b)-1) \\
  &=  \frac{1}{2}\bm{w}^T\bm{w} - \sum_i \alpha_iy_{i} (\bm{w}^T \bm{x}_i + b) +
  \sum_i \alpha_i
\end{align}
We minimize $L$ with respect to $\bm{w}$ and $b$:
\begin{align}
  \frac{\partial L}{\partial \bm{w}} &= \bm{w} - \sum_i \alpha_i y_i \bm{x}_i = 0\\
  \frac{\partial L}{\partial b} &= \sum_i \alpha_i y_i = 0
\end{align}
and substitute $\bm{w} = \sum_i \alpha_i y_i \bm{x}_i$ into the primal form
(\ref{eq:lagrange}):
\begin{align*}
  L = \sum_i \alpha_i - \frac{1}{2}\sum_{i}\sum_{j}\alpha_i\alpha_j y_i y_j
  \bm{x}_i^T\bm{x}_j 
\end{align*}

In the obtained solution
\begin{align*}
  \bm{w} &= \sum \alpha_{i}y_{i}\bm{x}_{i} \\
  b &= y_k - \bm{w}^T\bm{x}_k \quad \text{for $k : \alpha_{k} \neq 0$}
\end{align*}
an $\alpha_i \neq 0$ indicates that the corresponding $\bm{x}_i$ is a support
vector. The decision function $f$ can then be expressed as
\begin{align}
   \label{eq:decisionfunct}
  f(\bm{x}) = sgn(\sum_{i=1}^{n} \alpha_{i} y_{i} \bm{x}_{i}^T \bm{x} +b).
\end{align}


\subsection{Soft Margin Classification: Non-separable Data}
\label{sec:nonsep}
In the context of real-world tasks, data are seldom perfectly (and linearly)
separable. A \textit{soft margin} \ac{SVM} allows outliers to exist within the
margin, but pays a cost for each of them. \textit{Slack variables} $\xi_{i}$ are
introduced for each data instance to prevent the outliers from affecting the
decision function:
\begin{align}
  \xi_{i} =
  \begin{cases}
    0, & \text{if $\bm{x}_i$ is correctly classified}, \\
    \leq \frac{1}{\|\bm{w}\|}, & \text{if $\bm{x}_i$ violates the margin rule}, \\
    > \frac{1}{\|\bm{w}\|} & \text{if $\bm{x}_i$ is misclassified}. 
  \end{cases}
\end{align}

In Figure \ref{fig:slack}, document $\bm{x}_1$ is misclassified and document
$\bm{x}_2$ violates the margin rule:
%$\xi_1 \leq \frac{1}{\|\bm{w}\|}$ and $\xi_2 > \frac{1}{\|\bm{w}\|}$:
\begin{figure}[h]
  \centering
  \includegraphics[page=4,width=.6\linewidth]{img/figs}
  \caption{Slack variables}
  \label{fig:slack}
\end{figure}

The formulation of the SVM optimization problem with slack variables is now:
\begin{align*}
  \text{Minimize } \frac{1}{2} \bm{w}^T\bm{w} + C \cdot \sum_{i=1}^n \xi_{i} \\
  \text{Subject to } y_i(\bm{w}^T\bm{x}_i + b) \geq 1 - \xi_i \\
  i = 1 \dots n,
\end{align*}
where the \textit{cost parameter} $C \geq 0$ provides a way to control
\textit{overfitting} of data. This occurs when the learning process provides a
very accurate fit to the training data, but cannot generalize on unseen testing
data: a small value of $C$ results in a large margin while a large $C$ results
in a  narrow margin, classifying more training examples correctly (the
soft-margin \ac{SVM} then behaves as the hard-margin \ac{SVM}).

\begin{table}[h]
  \centering
  \begin{minipage}{0.49\textwidth}
    \includegraphics[page=5,width=\linewidth]{img/figs}
    \captionof{figure}{Small $C$}
    \label{fig:5}
  \end{minipage}
  \begin{minipage}[h]{0.49\textwidth}
    \includegraphics[page=6,width=\linewidth]{img/figs}
    \captionof{figure}{Large $C$}
    \label{fig:6}
  \end{minipage}
\end{table}

The solution of the minimization problem with slack variables is
\begin{align*}
  \bm{w} &= \sum \alpha_{i}y_{i}\bm{x}_{i} \\
  b &= y_k(1-\xi_{k}) - \bm{w}^T\bm{x}_k \quad \text{for $k = \argmax_k \alpha_{k}$}
\end{align*}

and the decision function follows \ref{eq:decisionfunct}.

\subsection{Non-linear Classification: Kernels}
Consider now the data set in Figure \ref{fig:dim1}, which contains data
instances of one dimension: 

\begin{figure}[h]
  \centering
  \includegraphics[page=7, width=.6\linewidth]{img/figs}
  \caption{}
  \label{fig:dim1}
\end{figure}

Clearly we are unable to separate the data by a linear
classifier.\footnote{Recall also Figure \ref{fig:10}} But by
projecting the data into a space of higher dimension, we can make it
linearly separable (Figure \ref{fig:separable}).

\begin{figure}[h]
  \centering
  \includegraphics[page=8, width=.6\linewidth]{img/figs}
  \caption{$\phi(x) = (x,x^2)$}
  \label{fig:separable}
\end{figure}

As finding the mapping $\phi$ can turn out to be expensive (due to it's high
dimension), \acp{SVM} provide an efficient method, commonly reffered to as the
\textit{kernel trick}: we do not need to explicitly define the mapping $\phi$,
but instead we define a \textit{kernel function}
\begin{align}
  K : \mathbb{R}^n \times \mathbb{R}^n &\to \mathbb{R} \\
  K(\bm{x}_{i},\bm{x}_j) &= \phi(\bm{x}_i^T)\phi(\bm{x}_j)
\end{align}
and replace the dot
product $\bm{x}_i\bm{x}_j$ :
\begin{align}
  L(\alpha) &= \sum \alpha_{i} - \frac{1}{2}\sum \alpha_i \alpha_j y_i y_j
  K(\bm{x}_i, \bm{x}_j) \\
  f(\bm{x}) &= sgn(\sum_{i=1}^{n} \alpha_{i} y_{i} K(\bm{x}_{i},\bm{x}) + b).
\end{align}

%A kernel function is valid, when it satisfies \textit{Mercer's condition}, that
%$\int g(\bm{x})^2$ is finite:
%\begin{align}
%  \int\int K(\bm{x},\bm{z})g(\bm{x})g(\bm{z})d\bm{x} d\bm{z} \geq 0,
%\end{align}
%for every square integrable function $g(\bm{x})$. 
Common kernel functions include:
\begin{align}
   K(\bm{x}_{i},\bm{x}_j) =
  \begin{cases}
    \bm{x}_i^T\bm{x}_j & \text{(linear)}, \\
    (s\cdot\bm{x}_i^T\bm{x}_j +r)^d & \text{(polynomial)}, \\
    e^{-\gamma\cdot\|\bm{x}_i-\bm{x}_j\|^2}& \text{(\acf{RBF})}, \\
  \end{cases}
  \label{eq:kernelfunctions}
\end{align}
where $r,s,\gamma > 0$ are user-defined parameters.

\section{Feature Selection}
In machine learning experiments, commonly a subset of all available features is 
chosen, dealing with two potential issues: First, irrelevant features induce
greater computational cost and, second, irrelevant features may lead to
overfitting. The process of selecting a feature subset is reffered to as
\textit{feature selection}. A multitude of feature selection techniques
exist. When applying \textit{filter} methods of selection, the features are
first ranked based on a relevance to class measure, then a subset is selected
and this subset is given to the classifier. Popular rankings include
\textit{Pearson's correlation coefficient}, \textit{F-score} or
\textit{mutual information}. 

\textit{Wrapper} methods of selection employ a classifier: first a subset of
features is chosen, then the subset is evaluated by a classifier, a change to
the subset is made and the new subset is evaluated. This approach is generally
very expensive in computation, so heuristic search methods are applied to find
the optimal sets of features.

In our experiments, we choose to filter features using the ranking of
\textit{mutual information}, sometimes called \textit{information gain}.

\subsection{Information gain}
\subsubsection{Entropy of a random variable}
Let $p(x)$ be the probability function of a random variable $x$ over the event
space $X : p(x) = P(X==x)$. The \textit{entropy} $H(p) = H(X) \geq 0$ is the
average uncertainty of a single variable: 
\begin{align*}
  H(X) &= \sum_{x \in X}p(x)\cdot \log_{2}\frac{1}{p(x)} \\
  &= -\sum_{x \in X}p(x)\cdot \log_{2}p(x).
\end{align*}
Entropy measures the amount of information in a random variable, sometimes
described as the average number of $0/1$ questions needed to describe an outcome
of $p(x)$, \footnote{\url{https://en.wikipedia.org/wiki/Twenty_Questions}}
or the average number of \textit{bits} you necessarily need to encode a value of
the given random variable. To describe the behavior of the entropy 
function, consider the following example from \cite{manning1999}:

Simplified Polynesian appears to be just a random sequence of letters, with the
following letter frequencies:
\begin{table}[ht]
  \begin{center}
    \begin{tabular}{*{6}{c}}
      p & t & k & a & i & u \\
      $\frac{1}{8}$ & $\frac{1}{4}$ & $\frac{1}{8}$ & $\frac{1}{4}$ &
      $\frac{1}{8}$ & $\frac{1}{8}$
    \end{tabular}
  \end{center}
\end{table}
Then the per-letter entropy is:
\begin{align*}
  H(\text{Polynesian}) &= -\sum_{i \in \{p,t,k,a,i,u\}} p(i)\cdot\log_{2}p(i) \\
  &= -(4\cdot\frac{1}{8}\cdot\log_{2}\frac{1}{8} +
  2\cdot\frac{1}{4}\cdot\log_{2}\frac{1}{4}) \\ 
  &= 2\frac{1}{2} \quad\text{bits.}
\end{align*}
Following the previous interpretation of entropy, we can now design a code that
takes on average $2\frac{1}{2}$ bits to encode a letter:
\begin{center}
  \begin{tabular}{*{6}{c}}
    p & t & k & a & i & u \\
    100 & 00 & 101 & 01 & 110 & 111
  \end{tabular}
\end{center}

The definition of entropy extends to joint distributions as the amount of
information needed to specify both of their values:
$$H(X,Y) = -\sum_{y \in Y}\sum_{x \in X} p(x,y)\cdot \log_{2}p(x,y)$$
and the \textit{conditional entropy} of a discrete random variable $y$ given $x$
expresses how much extra information one still needs to supply on average to
communicate $y$ given that the other side knows $x$:
\begin{align*}
  H(Y|X) &= -\sum_{x \in X} p(x)\cdot H(Y|X==x) \\
  &= -\sum_{y \in Y}\sum_{x \in X} p(x,y)\cdot \log_{2}p(y|x).
\end{align*}
The \textit{chain rule} of entropy follows from the definition of conditional
entropy:
\begin{align*}
  H(Y|X) &= -\sum_{y \in Y}\sum_{x \in X} p(x,y)\cdot \log_{2}p(y|x) \\
  &= -\sum_{y \in Y}\sum_{x \in X} p(x,y)\cdot \log_{2}\frac{p(x,y)}{p(x)} \\
  &= \sum_{y \in Y}\sum_{x \in X} p(x,y)\cdot \log_{2}\frac{p(x)}{p(x,y)} \\
  &= -\sum_{y \in Y}\sum_{x \in X} p(x,y)\cdot \log_{2}p(x,y) -\sum_{y \in
    Y}\sum_{x \in X} p(x,y)\cdot \log_{2}p(x) \\ 
  &= H(X,Y) + \sum_{x \in X}p(x)\cdot \log_{2}p(x) \\
  &= H(X,Y) - H(X).
\end{align*}

\subsubsection{Information gain}
The difference $H(X)-H(X|Y) = H(Y)-H(Y|X)$ is called the
\textit{mutual information} between $X$ an $Y$, or the \textit{information
  gain}. It reflects the amount of information about $X$ provided by $Y$ and
vice versa. Features in a text classification task correspond to random
variables, and thus following \cite{hladka2013}, we can speak about
\textit{feature entropy}, the conditional entropy of a feature given another
feature and the mutual information of two features. Given a class $C$ and a
feature $A$ containing values $\{a_i\}$, we can compute the
\textit{information gain} of $C$ and $A$, which measures the amount of shared
information between class $C$ and feature $A$:  
\begin{align*}
  IG(C,A) &= H(C) - H(C|A) \\
  &= H(C) - \sum_{a_i \in A} p(a_i)\cdot H(C|a_i)
\end{align*}

Ranking the features according to information gain gives a measure for
comparing how features contribute to the knowledge about the target class: the
higher information gain $IG(C,A)$, the better chance that $A$ is a useful
feature.

\section{Feature Types}
In this section we will describe some of the types of features that have been
used in previous experiments.

\subsection{\textit{n}-grams}
In text processing, an \textit{$n$-gram} in general can be defined as any
continuous sequence of co-occuring tokens (e.g. words or characters) in
text. Given a sequence of tokens $t_1\dots t_n$, then \textit{bigrams} are
described as $\{(t_i,t_{i+1})\}_{i=1}^{n-1}$,
\textit{trigrams} as $\{(t_i,t_{i+1},t_{i+2})\}_{i=1}^{n-2}$ and so on.
Consider the following sentence:\footnote{The sentence is from
  the CzeSL-SGT corpus and contains mild errors in diacritics.}

\begin{table}
  \begin{center}
    \begin{tabular}{*{5}{c}}
      Česká & gramatika & neni & těžka & . \\
      \textit{Czech} & \textit{grammar} & \textit{is not} & \textit{difficult} &
      \textit{.} 
    \end{tabular}
  \end{center}
\end{table}

\begin{table}
  \begin{center}
    \begin{tabular}{|c|c|}
      \hline
      $\bm{n}$ & $\bm{n}$\textbf{-grams} \\
      \hline
      1 & (\textit{Česká}), (\textit{gramatika}), (\textit{neni}),
      (\textit{těžka}), (\textit{.})\\ 
      2 &  (\textit{Česká gramatika}), (\textit{gramatika neni}), (\textit{neni
        těžka}), (\textit{těžka .})\\
      3 &  (\textit{Česká gramatika neni}), (\textit{gramatika neni těžka}),
      (\textit{neni těžka .})\\
      \hline
    \end{tabular}
  \end{center}
  \caption{Word uni-, bi- and tri-grams}
  \label{tab:wordgrams}
\end{table}

\begin{table}[ht]
  \begin{center}
    \begin{tabular}{|c|c|}
      \hline
      $\bm{n}$ & $\bm{n}$\textbf{-grams} \\
      \hline
      1 & (\textit{g}), (\textit{r}), (\textit{a}), (\textit{m}), (\textit{a}),
      (\textit{t}), (\textit{i}), (\textit{k}), (\textit{a}) \\ 
      2 &  (\textit{gr}), (\textit{ra}), (\textit{am}), (\textit{ma}),
      (\textit{at}), (\textit{ti}), (\textit{ik}), (\textit{ka}) \\
      3 &  (\textit{gra}), (\textit{ram}), (\textit{ama}), (\textit{mat}),
      (\textit{ati}), (\textit{tik}), (\textit{ika}) \\
      \hline
    \end{tabular}
  \end{center}
  \caption{Character uni-, bi- and tri-grams}
  \label{tab:chargrams}
\end{table}

\begin{table}
  \begin{center}
    \begin{tabular}{|c|c|}
      \hline
      $\bm{n}$ & $\bm{n}$\textbf{-grams} \\
       \hline
      1 & (\textit{AA}), (\textit{NN}), (\textit{VB}),
      (\textit{AA}), (\textit{Z:})\\ 
      2 &  (\textit{AA NN}), (\textit{NN VB}), (\textit{VB AA}),
      (\textit{AA Z:})\\ 
      3 &  (\textit{AA NN VB}), (\textit{NN VB AA}), (\textit{VB AA Z:})\\
      \hline
    \end{tabular}
  \end{center}
  \caption{\ac{POS} uni-, bi- and tri-grams of corrected
    word forms (AA = Adjective, NN = Noun, VB = Verb in present or future form,
    Z: = Punctuation)}
  \label{tab:posgrams}
\end{table}

For $n=1\dots3$, Table \ref{tab:wordgrams} shows which \textit{word} $n$-grams
would be retrieved from the sentence, Table \ref{tab:chargrams} shows which
\textit{character} $n$-grams would be retrieved from the word \textit{gramatika}
and Table \ref{tab:posgrams} shows which \textit{part-of-speech} $n$-grams would
be retrieved from the sentence.

\subsection{Function words}
Words can generally be divided into two groups of \textit{function words} and
\textit{content words}. Function words carry little lexical meaning, and
typically define sentence structure and grammatical relationships. The class of
function words is sometimes called \textit{closed}, because new function words
are rarely added to a language. Function words include prepositions, determiners,
conjunctions, pronouns, auxiliary verbs (e.g. the verb \textit{do} in the
English sentence \textit{Do you understand?}) and some adverbs (e.g. adverbs
that refer to time: \textit{then}, \textit{now}). On the other hand, content 
words mainly serve as carriers of lexical meaning and include nouns, adjectives
and most verbs and adverbs. New content words are often added to languages, so
the class of content words is sometimes called \textit{open}.

\subsection{Context-free Grammar Production Rules}
The syntactic structure of a sentence can be expressed in multiple ways. An
intuitive notation is a phrase structure \textit{tree}. In the tree, internal
nodes are called \textit{nonterminal} and leaves are called \textit{terminal}
nodes, or simply \textit{terminals}.

\begin{table}[ht]
  \centering
  \begin{minipage}{0.6\textwidth}
    \includegraphics[page=1, width=.69\linewidth]{img/tree}
    \captionof{figure}{Syntactic tree}
    \label{fig:tree}
  \end{minipage}
  \begin{minipage}{0.3\textwidth}
    \begin{tabular}{l}
       S $\to$ NP VP \\
      VP $\to$ VBD NP \\
      NP $\to$ NNS \\
      NP $\to$ NNP \\
      NNP $\to$ \textit{Edward} \\
      VBD $\to$ \textit{hated} \\
      NNS $\to$ \textit{birthdays}
    \end{tabular}
    \caption{Production rules}
    \label{tab:rules}
  \end{minipage}
\end{table}

A structure like the one in Figure \ref{fig:tree} can also be represented as a
set of \textit{production rules} (or \textit{rewrite rules}) of the form
$X \to Y_1Y_2\dots Y_n$, where $X$ is a terminal symbol and $Y_1Y_2\dots Y_n$ is
a sequence of terminals and
nonterminals:\footnote{NNP = Proper noun, singular; NNS = Noun, plural; VBD =
  Verb, past tense}  

A \textit{context-free grammar} $G=(T,N,S,R)$ consists of a set of terminals
$T$, a set of nonterminals $N$, a start symbol $S$ (which is a nonterminal) and
a set of production rules $R$ of the form as shown in \ref{tab:rules}.

\subsection{Errors}
Apart from different patterns distributed in texts, corpora tagged
for errors provide extra information. The motivation for using
errors (typically represented by a form of an error tag) comes from the assumption
that errors that a learner of a second language makes are related to his native
language.
Tagged errors may include for example syntactic errors (i.e. subject-verb
disagreement) or errors in morphology (i.e. inflectional ending).
