#!/usr/bin/python
#Contains several minor methods often used during feature extraction
import collections, unicodedata, math

#Different ways how to compute ngrams
"""def my_ngrams(inlist,n):
    len_inlist = len(inlist)
    outlist = [tuple(inlist[i:i+n]) for i in xrange(len_inlist-n+1)]
    return outlist"""

def my_ngrams(inlist,n):
    outlist = list()
    for i in xrange(len(inlist)-n+1):
        outlist.append(tuple(inlist[i:i+n]))
    return(outlist)

"""def my_ngrams(inlist,n):
    return izip(*[islice(seq, i, len(inlist))
                  for i, seq in enumerate(tee(inlist, n))])"""

###
def cleantext(line):
    """`line` is a unicode string"""
    newline = u''.join([ c if unicodedata.category(c)[0] in 'LZ'
                         else u' ' for c in line])
    return newline.lower()

def _byvalue(x,y):
    if y[1] > x[1]:
        return 1
    elif y[1] < x[1]:
        return -1
    else:
        return 0
        
def wordlist(line):
    return line.split()

class Counter(collections.Counter):
    def printcounter(self,statsfile):
        ngramlist = self.items()
        ngramlist.sort(cmp=_byvalue)
        for ngram, count in ngramlist:
            statsfile.write((u'%s\t\t%d\n' % (ngram, count)).encode('utf-8'))
