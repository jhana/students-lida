#!/usr/bin/python
# These methods are used when computing different values of extracted features.

import math

def log_normalize(term_frequency, text_length):
    freq_log = 1 + math.log(term_frequency)
    length_log = 1 + math.log(text_length)
    return(round(10*(freq_log/length_log)))

def relative(term_frequency, text_length):
    return(float(term_frequency)/text_length)

def transform(featureset, mode, doclength):
    if(mode=='bin'):
        return({k:1 for k in featureset})
    elif(mode=='raw'):
        return featureset
    elif(mode=='rel'):
        return({k:relative(v,doclength) for k,v in featureset.items()})
    elif(mode=='log'):
        return({k:log_normalize(v,doclength) for k,v in featureset.items()})
        
